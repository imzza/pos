-- MySQL dump 10.13  Distrib 5.7.24, for Linux (x86_64)
--
-- Host: localhost    Database: stepinns_sispos
-- ------------------------------------------------------
-- Server version	5.7.24-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ospos_app_config`
--

DROP TABLE IF EXISTS `ospos_app_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_app_config` (
  `key` varchar(50) NOT NULL,
  `value` varchar(500) NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_app_config`
--

LOCK TABLES `ospos_app_config` WRITE;
/*!40000 ALTER TABLE `ospos_app_config` DISABLE KEYS */;
INSERT INTO `ospos_app_config` VALUES ('address','P2 33 Jeff Hights Main Bullevard Road Gulberg 3 Lhore'),('allow_duplicate_barcodes','1'),('barcode_content','id'),('barcode_first_row','category'),('barcode_font','0'),('barcode_font_size','10'),('barcode_formats','null'),('barcode_generate_if_empty','0'),('barcode_height','50'),('barcode_num_in_row','2'),('barcode_page_cellspacing','20'),('barcode_page_width','100'),('barcode_second_row','item_code'),('barcode_third_row','unit_price'),('barcode_type','Code39'),('barcode_width','250'),('cash_decimals','2'),('cash_rounding_code','1'),('company','StepInnSolution'),('company_logo',''),('country_codes','PKR'),('currency_decimals','2'),('currency_symbol','RS'),('custom10_name',''),('custom1_name',''),('custom2_name',''),('custom3_name',''),('custom4_name',''),('custom5_name',''),('custom6_name',''),('custom7_name',''),('custom8_name',''),('custom9_name',''),('customer_reward_enable','0'),('customer_sales_tax_support','0'),('dateformat','d.m.Y'),('date_or_time_format','date_or_time_format'),('default_origin_tax_code',''),('default_register_mode','sale'),('default_sales_discount','0'),('default_tax_1_name',''),('default_tax_1_rate',''),('default_tax_2_name',''),('default_tax_2_rate',''),('default_tax_category','Standard'),('default_tax_rate','8'),('derive_sale_quantity','0'),('dinner_table_enable','0'),('email','info@stepinnsolution.com'),('email_receipt_check_behaviour','last'),('enforce_privacy',''),('fax',''),('financial_year','1'),('gcaptcha_enable','0'),('gcaptcha_secret_key','6Lc5EoEUAAAAALyNWeSlv32O-_qtmLSwDtA-TAOO'),('gcaptcha_site_key','6Lc5EoEUAAAAAK0aq2oarWh8voQlWrAchbSewRSd'),('giftcard_number','series'),('invoice_default_comments','This is a default comment'),('invoice_email_message','Dear {CU}, In attachment the receipt for sale {ISEQ}'),('invoice_enable','1'),('language','english'),('language_code','en-US'),('last_used_invoice_number','0'),('last_used_quote_number','0'),('last_used_work_order_number','0'),('lines_per_page','25'),('line_sequence','0'),('mailpath','/usr/sbin/sendmail'),('msg_msg',''),('msg_pwd',''),('msg_src',''),('msg_uid',''),('notify_horizontal_position','center'),('notify_vertical_position','bottom'),('number_locale','en_US'),('payment_options_order','cashdebitcredit'),('phone','+92 3056120188'),('print_bottom_margin',''),('print_delay_autoreturn','0'),('print_footer','0'),('print_header','0'),('print_left_margin',''),('print_receipt_check_behaviour','always'),('print_right_margin',''),('print_silently','0'),('print_top_margin',''),('protocol','mail'),('quantity_decimals','0'),('quote_default_comments','This is a default quote comment'),('receipt_font_size','11'),('receipt_show_company_name','1'),('receipt_show_description','1'),('receipt_show_serialnumber','1'),('receipt_show_taxes','0'),('receipt_show_total_discount','1'),('receipt_template','receipt_default'),('receiving_calculate_average_price','0'),('recv_invoice_format','{CO}'),('return_policy','Return Policy SHould be Placed Here.'),('sales_invoice_format','{CO}'),('sales_quote_format','Q%y{QSEQ:6}'),('smtp_crypto','ssl'),('smtp_host',''),('smtp_pass',''),('smtp_port','465'),('smtp_timeout','5'),('smtp_user',''),('suggestions_first_column','name'),('suggestions_second_column',''),('suggestions_third_column',''),('tax_decimals','2'),('tax_included','0'),('theme','cerulean'),('thousands_separator',''),('timeformat','h:i:s A'),('timezone','Asia/Tashkent'),('website','http://stepinnsolution.com/pos/'),('work_order_enable','0'),('work_order_format','W%y{WSEQ:6}');
/*!40000 ALTER TABLE `ospos_app_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_customers`
--

DROP TABLE IF EXISTS `ospos_customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_customers` (
  `person_id` int(10) NOT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `account_number` varchar(255) DEFAULT NULL,
  `taxable` int(1) NOT NULL DEFAULT '1',
  `sales_tax_code` varchar(32) NOT NULL DEFAULT '1',
  `discount_percent` decimal(15,2) NOT NULL DEFAULT '0.00',
  `package_id` int(11) DEFAULT NULL,
  `points` int(11) DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `employee_id` int(10) NOT NULL,
  `consent` int(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `account_number` (`account_number`),
  KEY `person_id` (`person_id`),
  KEY `package_id` (`package_id`),
  CONSTRAINT `ospos_customers_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `ospos_people` (`person_id`),
  CONSTRAINT `ospos_customers_ibfk_2` FOREIGN KEY (`package_id`) REFERENCES `ospos_customers_packages` (`package_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_customers`
--

LOCK TABLES `ospos_customers` WRITE;
/*!40000 ALTER TABLE `ospos_customers` DISABLE KEYS */;
INSERT INTO `ospos_customers` VALUES (2,NULL,NULL,1,'',5.00,NULL,NULL,0,'2018-07-19 13:59:11',1,1),(3,NULL,NULL,1,'',0.00,NULL,NULL,1,'2018-07-20 02:38:34',1,1),(4,'kjh','kjh',1,'',0.00,NULL,NULL,1,'2018-07-20 02:45:44',1,1),(6,NULL,NULL,1,'',0.00,NULL,NULL,0,'2018-07-20 08:35:47',1,1),(10,NULL,NULL,0,'',0.00,NULL,NULL,0,'2018-08-09 09:19:53',1,1);
/*!40000 ALTER TABLE `ospos_customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_customers_packages`
--

DROP TABLE IF EXISTS `ospos_customers_packages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_customers_packages` (
  `package_id` int(11) NOT NULL AUTO_INCREMENT,
  `package_name` varchar(255) DEFAULT NULL,
  `points_percent` float NOT NULL DEFAULT '0',
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`package_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_customers_packages`
--

LOCK TABLES `ospos_customers_packages` WRITE;
/*!40000 ALTER TABLE `ospos_customers_packages` DISABLE KEYS */;
INSERT INTO `ospos_customers_packages` VALUES (1,'Default',0,0),(2,'Bronze',10,0),(3,'Silver',20,0),(4,'Gold',30,0),(5,'Premium',50,0);
/*!40000 ALTER TABLE `ospos_customers_packages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_customers_points`
--

DROP TABLE IF EXISTS `ospos_customers_points`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_customers_points` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `sale_id` int(11) NOT NULL,
  `points_earned` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `person_id` (`person_id`),
  KEY `package_id` (`package_id`),
  KEY `sale_id` (`sale_id`),
  CONSTRAINT `ospos_customers_points_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `ospos_customers` (`person_id`),
  CONSTRAINT `ospos_customers_points_ibfk_2` FOREIGN KEY (`package_id`) REFERENCES `ospos_customers_packages` (`package_id`),
  CONSTRAINT `ospos_customers_points_ibfk_3` FOREIGN KEY (`sale_id`) REFERENCES `ospos_sales` (`sale_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_customers_points`
--

LOCK TABLES `ospos_customers_points` WRITE;
/*!40000 ALTER TABLE `ospos_customers_points` DISABLE KEYS */;
/*!40000 ALTER TABLE `ospos_customers_points` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_dinner_tables`
--

DROP TABLE IF EXISTS `ospos_dinner_tables`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_dinner_tables` (
  `dinner_table_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`dinner_table_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_dinner_tables`
--

LOCK TABLES `ospos_dinner_tables` WRITE;
/*!40000 ALTER TABLE `ospos_dinner_tables` DISABLE KEYS */;
INSERT INTO `ospos_dinner_tables` VALUES (1,'Delivery',0,0),(2,'Take Away',0,0);
/*!40000 ALTER TABLE `ospos_dinner_tables` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_employees`
--

DROP TABLE IF EXISTS `ospos_employees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_employees` (
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `person_id` int(10) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `hash_version` int(1) NOT NULL DEFAULT '2',
  `language` varchar(48) DEFAULT NULL,
  `language_code` varchar(8) DEFAULT NULL,
  UNIQUE KEY `username` (`username`),
  KEY `person_id` (`person_id`),
  CONSTRAINT `ospos_employees_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `ospos_people` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_employees`
--

LOCK TABLES `ospos_employees` WRITE;
/*!40000 ALTER TABLE `ospos_employees` DISABLE KEYS */;
INSERT INTO `ospos_employees` VALUES ('admin','$2y$10$xS3Z3.0DoOPUUUY1w2AeCO0tCfKnoDNN0vKahVowiVkqseMijXmyO',1,0,2,'',''),('AHSAN','$2y$10$DyBPV05S./XeuU8EPLdl/eq4L0vYjST2LbX94D.ukTGMgIycZmmUe',12,0,2,'',''),('david','$2y$10$MoJ0FVzkgwoZYUeSGerK5.XvICX.g1OSRwwQXuv72HbBrGeG.ygJ.',8,1,2,'',''),('SHAHROZE','$2y$10$ARiL5FBzSsW3JFFYBH0IkOebvW7ce0xGQIdsTm.gZTOGm00n8p0gu',7,0,2,'','');
/*!40000 ALTER TABLE `ospos_employees` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_expense_categories`
--

DROP TABLE IF EXISTS `ospos_expense_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_expense_categories` (
  `expense_category_id` int(10) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) DEFAULT NULL,
  `category_description` varchar(255) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`expense_category_id`),
  UNIQUE KEY `category_name` (`category_name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_expense_categories`
--

LOCK TABLES `ospos_expense_categories` WRITE;
/*!40000 ALTER TABLE `ospos_expense_categories` DISABLE KEYS */;
INSERT INTO `ospos_expense_categories` VALUES (1,'Utility','Unitilies',0),(2,'Supplier','',0);
/*!40000 ALTER TABLE `ospos_expense_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_expenses`
--

DROP TABLE IF EXISTS `ospos_expenses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_expenses` (
  `expense_id` int(10) NOT NULL AUTO_INCREMENT,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `amount` decimal(15,2) NOT NULL,
  `payment_type` varchar(40) NOT NULL,
  `expense_category_id` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `employee_id` int(10) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `supplier_name` varchar(255) DEFAULT NULL,
  `supplier_tax_code` varchar(255) DEFAULT NULL,
  `tax_amount` decimal(15,2) DEFAULT NULL,
  PRIMARY KEY (`expense_id`),
  KEY `expense_category_id` (`expense_category_id`),
  KEY `employee_id` (`employee_id`),
  CONSTRAINT `ospos_expenses_ibfk_1` FOREIGN KEY (`expense_category_id`) REFERENCES `ospos_expense_categories` (`expense_category_id`),
  CONSTRAINT `ospos_expenses_ibfk_2` FOREIGN KEY (`employee_id`) REFERENCES `ospos_employees` (`person_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_expenses`
--

LOCK TABLES `ospos_expenses` WRITE;
/*!40000 ALTER TABLE `ospos_expenses` DISABLE KEYS */;
INSERT INTO `ospos_expenses` VALUES (4,'2018-08-08 08:04:16',10.00,'Cash',1,'',1,0,'River John','12',1.00),(5,'2018-12-18 11:18:22',13.00,'Cash',1,'Cock Chips Biscuts.',1,1,'','',13.00),(6,'2018-12-24 07:20:57',100.00,'Cash',1,'',1,1,'abc','',100.00),(7,'2018-12-24 07:58:41',100.00,'Cash',1,'',1,0,'John Sons','',0.00),(8,'2018-12-28 10:00:18',1233.00,'Cash',2,'',1,0,'Johnsons','',1.00),(9,'2019-01-05 08:39:52',1200.00,'Cash',2,'',1,0,'John Sons','',0.00);
/*!40000 ALTER TABLE `ospos_expenses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_giftcards`
--

DROP TABLE IF EXISTS `ospos_giftcards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_giftcards` (
  `record_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `giftcard_id` int(11) NOT NULL AUTO_INCREMENT,
  `giftcard_number` varchar(255) DEFAULT NULL,
  `value` decimal(15,2) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `person_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`giftcard_id`),
  UNIQUE KEY `giftcard_number` (`giftcard_number`),
  KEY `person_id` (`person_id`),
  CONSTRAINT `ospos_giftcards_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `ospos_people` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_giftcards`
--

LOCK TABLES `ospos_giftcards` WRITE;
/*!40000 ALTER TABLE `ospos_giftcards` DISABLE KEYS */;
/*!40000 ALTER TABLE `ospos_giftcards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_grants`
--

DROP TABLE IF EXISTS `ospos_grants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_grants` (
  `permission_id` varchar(255) NOT NULL,
  `person_id` int(10) NOT NULL,
  `menu_group` varchar(32) DEFAULT 'home',
  PRIMARY KEY (`permission_id`,`person_id`),
  KEY `ospos_grants_ibfk_2` (`person_id`),
  CONSTRAINT `ospos_grants_ibfk_1` FOREIGN KEY (`permission_id`) REFERENCES `ospos_permissions` (`permission_id`) ON DELETE CASCADE,
  CONSTRAINT `ospos_grants_ibfk_2` FOREIGN KEY (`person_id`) REFERENCES `ospos_employees` (`person_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_grants`
--

LOCK TABLES `ospos_grants` WRITE;
/*!40000 ALTER TABLE `ospos_grants` DISABLE KEYS */;
INSERT INTO `ospos_grants` VALUES ('config',1,'office'),('customers',1,'home'),('customers',7,'home'),('debcreds',1,'home'),('employees',1,'office'),('expenses',1,'home'),('expenses_categories',1,'home'),('home',1,'office'),('items',1,'home'),('items',7,'home'),('items_stock',1,'--'),('items_stock',7,'--'),('item_kits',1,'home'),('office',1,'home'),('receivings',7,'home'),('receivings_stock',7,'--'),('reports',1,'home'),('reports',7,'home'),('reports_categories',7,'--'),('reports_customers',7,'--'),('reports_discounts',7,'--'),('reports_employees',1,'--'),('reports_employees',7,'--'),('reports_expenses_categories',7,'--'),('reports_inventory',1,'--'),('reports_inventory',7,'--'),('reports_items',7,'--'),('reports_payments',7,'--'),('reports_sales',7,'--'),('sales',7,'home'),('sales',12,'home'),('sales_delete',7,'--'),('sales_delete',12,'--'),('sales_stock',7,'--'),('sales_stock',12,'--'),('suppliers',1,'home'),('taxes',1,'office');
/*!40000 ALTER TABLE `ospos_grants` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_inventory`
--

DROP TABLE IF EXISTS `ospos_inventory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_inventory` (
  `trans_id` int(11) NOT NULL AUTO_INCREMENT,
  `trans_items` int(11) NOT NULL DEFAULT '0',
  `trans_user` int(11) NOT NULL DEFAULT '0',
  `trans_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `trans_comment` text NOT NULL,
  `trans_location` int(11) NOT NULL,
  `trans_inventory` decimal(15,3) NOT NULL DEFAULT '0.000',
  PRIMARY KEY (`trans_id`),
  KEY `trans_items` (`trans_items`),
  KEY `trans_user` (`trans_user`),
  KEY `trans_location` (`trans_location`),
  CONSTRAINT `ospos_inventory_ibfk_1` FOREIGN KEY (`trans_items`) REFERENCES `ospos_items` (`item_id`),
  CONSTRAINT `ospos_inventory_ibfk_2` FOREIGN KEY (`trans_user`) REFERENCES `ospos_employees` (`person_id`),
  CONSTRAINT `ospos_inventory_ibfk_3` FOREIGN KEY (`trans_location`) REFERENCES `ospos_stock_locations` (`location_id`)
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_inventory`
--

LOCK TABLES `ospos_inventory` WRITE;
/*!40000 ALTER TABLE `ospos_inventory` DISABLE KEYS */;
INSERT INTO `ospos_inventory` VALUES (1,1,1,'2018-07-19 13:58:01','Manual Edit of Quantity',1,20.000),(2,2,1,'2018-07-20 04:31:52','Manual Edit of Quantity',1,4.000),(3,1,1,'2018-07-20 04:32:19','',1,12.000),(4,3,1,'2018-07-20 08:35:12','Manual Edit of Quantity',1,0.000),(5,3,1,'2018-07-20 08:36:06','POS 1',1,-1.000),(6,1,1,'2018-08-08 06:40:32','POS 2',1,-1.000),(7,1,1,'2018-08-08 10:51:03','Deleted',1,-31.000),(8,2,1,'2018-08-08 10:51:03','Deleted',1,-4.000),(9,4,1,'2018-08-08 06:59:27','Manual Edit of Quantity',1,43.000),(10,5,1,'2018-08-08 07:08:15','Manual Edit of Quantity',1,50.000),(11,4,1,'2018-08-08 07:33:45','',1,-20.000),(12,4,1,'2018-08-08 07:44:35','RECV 1',1,29.000),(13,5,1,'2018-08-08 07:44:35','RECV 1',1,32.000),(14,4,1,'2018-08-08 07:54:28','POS 4',1,-4.000),(15,5,1,'2018-08-08 07:56:00','POS 5',1,-1.000),(16,4,1,'2018-08-08 07:59:10','RECV 2',1,-10.000),(17,4,7,'2018-08-08 08:08:28','POS 7',1,-1.000),(18,4,1,'2018-08-08 08:12:34','POS 8',1,-4.000),(19,4,7,'2018-08-08 08:20:03','POS 9',1,-1.000),(20,6,1,'2018-08-09 09:19:24','Manual Edit of Quantity',1,43.000),(21,6,1,'2018-08-09 09:20:54','POS 10',1,-1.000),(22,4,1,'2018-08-09 09:21:47','POS 11',1,-1.000),(23,4,1,'2018-08-09 14:18:46','POS 12',1,-21.000),(28,7,1,'2018-08-10 14:57:56','Manual Edit of Quantity',1,50.000),(29,7,1,'2018-08-10 15:01:46','POS 15',1,-1.000),(30,8,1,'2018-12-17 00:05:59','Manual Edit of Quantity',1,0.000),(31,9,1,'2018-12-18 11:01:06','Manual Edit of Quantity',1,2.000),(32,5,1,'2018-12-18 11:06:45','POS 16',1,-1.000),(33,9,1,'2018-12-18 11:10:51','Deleted',1,-2.000),(34,10,1,'2018-12-19 09:28:36','Manual Edit of Quantity',1,12.000),(35,11,1,'2018-12-19 10:41:58','Manual Edit of Quantity',1,12.000),(36,12,1,'2018-12-19 11:01:50','Manual Edit of Quantity',1,12.000),(37,12,1,'2018-12-19 11:14:54','Manual Edit of Quantity',1,11.000),(38,13,1,'2018-12-19 12:56:49','Manual Edit of Quantity',1,11.000),(39,13,1,'2018-12-19 13:01:38','POS 17',1,-1.000),(40,14,1,'2018-12-19 14:33:40','Manual Edit of Quantity',1,12.000),(41,15,1,'2018-12-20 04:59:26','Manual Edit of Quantity',1,12.000),(42,16,1,'2018-12-20 05:03:35','Manual Edit of Quantity',1,21.000),(43,16,7,'2018-12-20 06:25:27','POS 18',1,-1.000),(44,16,7,'2018-12-20 09:50:07','POS 19',1,-1.000),(45,16,7,'2018-12-20 11:17:16','POS 20',1,-10.000),(46,16,7,'2018-12-20 11:18:48','POS 21',1,-4.000),(47,16,1,'2018-12-21 07:12:03','Manual Edit of Quantity',1,10.000),(48,16,7,'2018-12-21 10:14:10','POS 22',1,-1.000),(49,16,7,'2018-12-21 10:49:20','POS 23',1,-1.000),(50,12,7,'2018-12-21 10:51:46','POS 24',1,-1.000),(51,16,7,'2018-12-21 10:51:46','POS 24',1,-1.000),(52,14,7,'2018-12-21 10:51:47','POS 24',1,-1.000),(53,16,7,'2018-12-21 13:55:49','POS 25',1,-1.000),(54,17,1,'2018-12-24 09:06:34','Manual Edit of Quantity',1,12.000),(55,18,1,'2018-12-24 10:12:00','Manual Edit of Quantity',1,21.000),(56,19,1,'2018-12-24 10:41:37','Manual Edit of Quantity',1,20.000),(57,19,1,'2018-12-24 10:55:50','Manual Edit of Quantity',1,10.000),(58,20,1,'2018-12-27 10:38:34','Manual Edit of Quantity',1,0.000),(59,21,1,'2018-12-27 11:15:55','Manual Edit of Quantity',1,12.000),(60,22,1,'2018-12-27 12:49:18','Manual Edit of Quantity',1,12.000),(61,23,7,'2019-01-01 05:42:43','Manual Edit of Quantity',1,20.000),(62,24,7,'2019-01-01 06:09:16','Manual Edit of Quantity',1,20.000),(63,25,7,'2019-01-01 06:18:17','Manual Edit of Quantity',1,20.000),(64,25,7,'2019-01-01 06:27:25','',1,40.000),(65,25,7,'2019-01-01 06:58:49','POS 27',1,-20.000),(66,25,7,'2019-01-01 07:06:52','POS 28',1,-15.000),(67,25,7,'2019-01-01 07:19:28','RECV 3',1,480.000),(68,26,1,'2019-01-01 09:16:07','Manual Edit of Quantity',1,50.000),(69,26,1,'2019-01-01 09:18:57','',1,60.000),(70,25,7,'2019-01-01 09:36:44','POS 29',1,-2.000),(71,16,7,'2019-01-01 09:38:45','POS 30',1,-1.000),(72,16,7,'2019-01-01 12:46:12','POS 31',1,-1.000),(73,25,7,'2019-01-01 12:47:33','POS 32',1,-1.000),(74,25,7,'2019-01-01 12:49:33','POS 33',1,-1.000),(75,25,7,'2019-01-01 12:53:35','POS 34',1,-1.000),(76,25,7,'2019-01-01 12:54:52','POS 35',1,-1.000),(77,25,7,'2019-01-01 12:58:12','POS 36',1,-1.000),(78,14,7,'2019-01-02 06:18:14','POS 37',1,-1.000),(79,27,1,'2019-01-02 07:42:38','Manual Edit of Quantity',1,0.000),(80,28,1,'2019-01-02 07:43:50','Manual Edit of Quantity',1,0.000),(81,29,1,'2019-01-02 07:44:32','Manual Edit of Quantity',1,0.000),(82,30,1,'2019-01-02 07:47:56','Manual Edit of Quantity',1,0.000),(83,31,1,'2019-01-02 07:48:36','Manual Edit of Quantity',1,0.000),(84,32,1,'2019-01-02 07:48:38','Manual Edit of Quantity',1,0.000),(85,13,1,'2019-01-02 08:23:53','',1,-3.000),(86,12,1,'2019-01-02 08:24:12','',1,-10.000),(87,12,1,'2019-01-02 08:24:43','',1,100.000),(88,14,7,'2019-01-02 13:48:09','POS 38',1,-1.000),(89,25,7,'2019-01-02 13:48:58','POS 39',1,-1.000),(90,25,7,'2019-01-02 13:49:36','POS 40',1,-1.000),(91,33,1,'2019-01-03 09:29:50','Manual Edit of Quantity',1,1500.000),(92,33,1,'2019-01-03 10:06:43','',1,1000.000),(93,33,1,'2019-01-03 10:08:01','',1,-2000.000),(94,33,1,'2019-01-03 10:12:44','',1,100.000),(95,33,1,'2019-01-03 10:21:41','',1,10.000),(96,20,1,'2019-01-03 10:24:16','',1,12.000),(97,20,1,'2019-01-03 10:25:20','Deleted',1,-12.000),(98,21,1,'2019-01-03 10:25:20','Deleted',1,-12.000),(99,22,1,'2019-01-03 10:25:20','Deleted',1,-12.000),(100,23,1,'2019-01-03 10:25:20','Deleted',1,-20.000),(101,27,1,'2019-01-03 10:27:00','Manual Edit of Quantity',1,150.000),(102,26,1,'2019-01-03 11:11:57','',1,12.000),(103,26,1,'2019-01-03 11:12:45','',1,1.000),(104,26,1,'2019-01-03 11:17:38','Manual Edit of Quantity',1,27.000),(105,33,1,'2019-01-07 10:55:09','',1,10.000),(106,18,1,'2019-01-08 05:45:52','',1,211.000);
/*!40000 ALTER TABLE `ospos_inventory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_item_kit_items`
--

DROP TABLE IF EXISTS `ospos_item_kit_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_item_kit_items` (
  `item_kit_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `quantity` decimal(15,3) NOT NULL,
  `kit_sequence` int(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`item_kit_id`,`item_id`,`quantity`),
  KEY `ospos_item_kit_items_ibfk_2` (`item_id`),
  CONSTRAINT `ospos_item_kit_items_ibfk_1` FOREIGN KEY (`item_kit_id`) REFERENCES `ospos_item_kits` (`item_kit_id`) ON DELETE CASCADE,
  CONSTRAINT `ospos_item_kit_items_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `ospos_items` (`item_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_item_kit_items`
--

LOCK TABLES `ospos_item_kit_items` WRITE;
/*!40000 ALTER TABLE `ospos_item_kit_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `ospos_item_kit_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_item_kits`
--

DROP TABLE IF EXISTS `ospos_item_kits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_item_kits` (
  `item_kit_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `item_id` int(10) NOT NULL DEFAULT '0',
  `kit_discount_percent` decimal(15,2) NOT NULL DEFAULT '0.00',
  `price_option` tinyint(2) NOT NULL DEFAULT '0',
  `print_option` tinyint(2) NOT NULL DEFAULT '0',
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`item_kit_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_item_kits`
--

LOCK TABLES `ospos_item_kits` WRITE;
/*!40000 ALTER TABLE `ospos_item_kits` DISABLE KEYS */;
INSERT INTO `ospos_item_kits` VALUES (1,'dummy',0,1.00,2,0,'Dum description'),(2,'General',0,12.00,0,0,'');
/*!40000 ALTER TABLE `ospos_item_kits` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_item_quantities`
--

DROP TABLE IF EXISTS `ospos_item_quantities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_item_quantities` (
  `item_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `quantity` decimal(15,3) NOT NULL DEFAULT '0.000',
  PRIMARY KEY (`item_id`,`location_id`),
  KEY `item_id` (`item_id`),
  KEY `location_id` (`location_id`),
  CONSTRAINT `ospos_item_quantities_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `ospos_items` (`item_id`),
  CONSTRAINT `ospos_item_quantities_ibfk_2` FOREIGN KEY (`location_id`) REFERENCES `ospos_stock_locations` (`location_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_item_quantities`
--

LOCK TABLES `ospos_item_quantities` WRITE;
/*!40000 ALTER TABLE `ospos_item_quantities` DISABLE KEYS */;
INSERT INTO `ospos_item_quantities` VALUES (12,1,112.000),(13,1,7.000),(14,1,9.000),(15,1,12.000),(16,1,9.000),(17,1,12.000),(18,1,232.000),(19,1,30.000),(20,1,0.000),(21,1,0.000),(22,1,0.000),(23,1,0.000),(24,1,20.000),(25,1,496.000),(26,1,150.000),(27,1,150.000),(28,1,0.000),(29,1,0.000),(30,1,0.000),(31,1,0.000),(32,1,0.000),(33,1,620.000);
/*!40000 ALTER TABLE `ospos_item_quantities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_items`
--

DROP TABLE IF EXISTS `ospos_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_items` (
  `name` varchar(255) NOT NULL,
  `category` varchar(255) NOT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `item_number` varchar(255) DEFAULT NULL,
  `description` varchar(255) NOT NULL,
  `cost_price` decimal(15,2) NOT NULL,
  `unit_price` decimal(15,2) NOT NULL,
  `reorder_level` decimal(15,3) NOT NULL DEFAULT '0.000',
  `receiving_quantity` decimal(15,3) NOT NULL DEFAULT '1.000',
  `item_id` int(10) NOT NULL AUTO_INCREMENT,
  `pic_filename` varchar(255) DEFAULT NULL,
  `allow_alt_description` tinyint(1) NOT NULL,
  `is_serialized` tinyint(1) NOT NULL,
  `stock_type` tinyint(2) NOT NULL DEFAULT '0',
  `item_type` tinyint(2) NOT NULL DEFAULT '0',
  `tax_category_id` int(10) NOT NULL DEFAULT '1',
  `deleted` int(1) NOT NULL DEFAULT '0',
  `custom1` varchar(255) DEFAULT NULL,
  `custom2` varchar(255) DEFAULT NULL,
  `custom3` varchar(255) DEFAULT NULL,
  `custom4` varchar(255) DEFAULT NULL,
  `custom5` varchar(255) DEFAULT NULL,
  `custom6` varchar(255) DEFAULT NULL,
  `custom7` varchar(255) DEFAULT NULL,
  `custom8` varchar(255) DEFAULT NULL,
  `custom9` varchar(255) DEFAULT NULL,
  `custom10` varchar(255) DEFAULT NULL,
  `employee_id` int(11) NOT NULL,
  `assign_employees` varchar(255) NOT NULL,
  `assign_employees_qty` varchar(255) NOT NULL,
  PRIMARY KEY (`item_id`),
  KEY `item_number` (`item_number`),
  KEY `supplier_id` (`supplier_id`),
  CONSTRAINT `ospos_items_ibfk_1` FOREIGN KEY (`supplier_id`) REFERENCES `ospos_suppliers` (`person_id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_items`
--

LOCK TABLES `ospos_items` WRITE;
/*!40000 ALTER TABLE `ospos_items` DISABLE KEYS */;
INSERT INTO `ospos_items` VALUES ('To be Delete','Laptop',11,NULL,'',50.00,60.00,1.000,1.000,12,NULL,0,0,0,0,0,0,'','','','','','','','','','',1,'8,7','13,4'),('Sampel Product','Sample',11,NULL,'',21.00,12.00,1.000,1.000,13,NULL,0,0,0,0,0,0,'','','','','','','','','','',7,'8,7','1,3'),('IRFAN ULLAH','asas',11,NULL,'',90.00,100.00,1.000,1.000,14,NULL,0,0,0,0,0,0,'','','','','','','','','','',7,'8,7','6,-1'),('Chrismiss Special','choclate',5,NULL,'',0.00,0.00,1.000,1.000,15,NULL,0,0,0,0,0,0,'','','','','','','','','','',1,'8','12'),('Only ME','saryia',5,NULL,'',44.00,32.00,1.000,1.000,16,NULL,0,0,0,0,0,0,'','','','','','','','','','',1,'7','-1'),('Hello','Laptop',NULL,NULL,'',32.00,40.00,6.000,1.000,17,NULL,0,0,0,0,0,0,'','','','','','','','','','',1,'',''),('Supplier Test','what so ever',9,NULL,'',1200.00,1200.00,1.000,12.000,18,NULL,0,0,0,0,0,0,'','','','','','','','','','',1,'8','12'),('Debet and Credet','debet',11,NULL,'',40.00,40.00,1.000,1.000,19,NULL,0,0,0,0,0,0,'','','','','','','','','','',1,'8','12'),('','asas',NULL,NULL,'',0.00,0.00,1.000,1.000,20,NULL,0,0,0,0,0,1,'','','','','','','','','','',1,'',''),('Mr. admin','Add new Category',11,NULL,'',12.00,32.00,1.000,1.000,21,NULL,0,0,0,0,0,1,'','','','','','','','','','',1,'8,7','1,2'),('ghjghjgjhghjg','choclate',11,NULL,'',561.00,600.00,1.000,1.000,22,NULL,0,0,0,0,0,1,'','','','','','','','','','',1,'8,7','10,2'),('fgfgfgf','Biscuite',11,NULL,'',0.00,0.00,12.000,24.000,23,NULL,0,0,0,0,0,1,'','','','','','','','','','',7,'',''),('abc','Biscuite',9,'870572009516','',0.00,0.00,1.000,24.000,24,NULL,0,0,0,0,0,0,'','','','','','','','','','',7,'',''),('sooper','Biscuite',9,'8964000681107','',88.00,90.00,1.000,24.000,25,NULL,0,0,0,0,0,0,'','','','','','','','','','',7,'12,7','20,-4'),('sooper','Biscuite',15,'8964000681107','',130.00,150.00,1.000,0.000,26,NULL,0,0,0,0,0,0,'','','','','','','','','','',1,'',''),('tisshue','asas',5,'8961006040044','',100.00,110.00,1.000,0.000,27,NULL,0,0,0,0,0,0,'','','','','','','','','','',1,'',''),('kl','Add new Category',11,'8961006040044','',0.00,0.00,1.000,1.000,28,NULL,0,0,0,0,0,1,'','','','','','','','','','',1,'',''),('Mr. admin','asas',5,NULL,'',0.00,0.00,1.000,1.000,29,NULL,0,0,0,0,0,1,'','','','','','','','','','',1,'',''),('8961006040044','Add new Category',11,NULL,'',0.00,0.00,1.000,1.000,30,NULL,0,0,0,0,0,1,'','','','','','','','','','',1,'',''),('8961006040044','Add new Category',11,NULL,'8961006040044\r\n',8961006040044.00,0.00,1.000,1.000,31,NULL,0,0,0,0,0,1,'','','','','','','','','','',1,'',''),('8961006040044','Add new Category',11,NULL,'8961006040044\r\n',8961006040044.00,0.00,1.000,1.000,32,NULL,0,0,0,0,0,1,'','','','','','','','','','',1,'',''),('Gulab Jamun','Mithai',15,NULL,'',1113.00,1122.00,12.000,0.000,33,NULL,0,0,0,0,0,0,'','','','','','','','','','',1,'12,7','100,100');
/*!40000 ALTER TABLE `ospos_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_items_taxes`
--

DROP TABLE IF EXISTS `ospos_items_taxes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_items_taxes` (
  `item_id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `percent` decimal(15,3) NOT NULL,
  PRIMARY KEY (`item_id`,`name`,`percent`),
  CONSTRAINT `ospos_items_taxes_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `ospos_items` (`item_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_items_taxes`
--

LOCK TABLES `ospos_items_taxes` WRITE;
/*!40000 ALTER TABLE `ospos_items_taxes` DISABLE KEYS */;
INSERT INTO `ospos_items_taxes` VALUES (2,'General',7.000),(2,'Sales',12.000),(4,'Incom',10.000),(4,'Sales',5.000),(5,'Incom',11.000),(5,'sales',5.000);
/*!40000 ALTER TABLE `ospos_items_taxes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_migrations`
--

DROP TABLE IF EXISTS `ospos_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_migrations` (
  `version` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_migrations`
--

LOCK TABLES `ospos_migrations` WRITE;
/*!40000 ALTER TABLE `ospos_migrations` DISABLE KEYS */;
INSERT INTO `ospos_migrations` VALUES (20180501100000);
/*!40000 ALTER TABLE `ospos_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_modules`
--

DROP TABLE IF EXISTS `ospos_modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_modules` (
  `name_lang_key` varchar(255) NOT NULL,
  `desc_lang_key` varchar(255) NOT NULL,
  `sort` int(10) NOT NULL,
  `module_id` varchar(255) NOT NULL,
  PRIMARY KEY (`module_id`),
  UNIQUE KEY `desc_lang_key` (`desc_lang_key`),
  UNIQUE KEY `name_lang_key` (`name_lang_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_modules`
--

LOCK TABLES `ospos_modules` WRITE;
/*!40000 ALTER TABLE `ospos_modules` DISABLE KEYS */;
INSERT INTO `ospos_modules` VALUES ('module_config','module_config_desc',110,'config'),('module_customers','module_customers_desc',10,'customers'),('module_debcreds\r\n','module_debcreds_desc\r\n',700,'debcreds'),('module_employees','module_employees_desc',80,'employees'),('module_expenses','module_expenses_desc',108,'expenses'),('module_expenses_categories','module_expenses_categories_desc',109,'expenses_categories'),('module_giftcards','module_giftcards_desc',90,'giftcards'),('module_home','module_home_desc',1,'home'),('module_items','module_items_desc',20,'items'),('module_item_kits','module_item_kits_desc',30,'item_kits'),('module_messages','module_messages_desc',98,'messages'),('module_office','module_office_desc',999,'office'),('module_receivings','module_receivings_desc',60,'receivings'),('module_reports','module_reports_desc',50,'reports'),('module_sales','module_sales_desc',70,'sales'),('module_suppliers','module_suppliers_desc',40,'suppliers'),('module_taxes','module_taxes_desc',105,'taxes');
/*!40000 ALTER TABLE `ospos_modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_people`
--

DROP TABLE IF EXISTS `ospos_people`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_people` (
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `gender` int(1) DEFAULT NULL,
  `phone_number` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `address_1` varchar(255) NOT NULL,
  `address_2` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `zip` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `comments` text NOT NULL,
  `person_id` int(10) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`person_id`),
  KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_people`
--

LOCK TABLES `ospos_people` WRITE;
/*!40000 ALTER TABLE `ospos_people` DISABLE KEYS */;
INSERT INTO `ospos_people` VALUES ('System','Administrator',NULL,'555-555-5555','stepinnsolution@gmail.com','Address 1','','','','','','',1),('Ashiq ','Khaki',1,'323232323','ashiq@sis.com','abc address','address 2','lahore','punjab','54000','pakistan','',2),('Hafiz','Adil',1,'03343938883','hafizadil431@gmail.com','Green Town Lahore','Green Town Lhr','','','','','',3),('Jhkijhkjhk','Jh',NULL,'h','kj','jh','kjh','kj','k','jh','kjh','kj',4),('John','River',1,'121-211-1212','irfan_lifeu@hotmail.com','74 2 B 2 Township Lahore','','sdadsa','Alabama','54000','Pakisatn','Sample Comments',5),('As','Asd',1,'as','as','sas','','','','','','',6),('Irfan','Ullah',1,'03056120188','irfan_lifeu@hotmail.com','adsda','','sdadsa','Alabama','54000','Pakistan','Comments',7),('David','Powers',1,'03056120188','iux@example.com','Address 1','','Narowal','Punjab','54000','Pakistan','Comments',8),('John','Peck',1,'03098781271','john@example.com','32 Street','','','','','','',9),('Iux','Example',1,'asasas','irfan_lifeu@hotmail.com','adsda','','sdadsa','Alabama','54000','','',10),('Irfan','Ullah',1,'03056120188','larryfrancisco@comcast.net','','','','','','','',11),('Ahsan','Ali',1,'','','','','','','','','',12),('Bhola','Sweets',1,'','','','','','','','','',15);
/*!40000 ALTER TABLE `ospos_people` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_permissions`
--

DROP TABLE IF EXISTS `ospos_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_permissions` (
  `permission_id` varchar(255) NOT NULL,
  `module_id` varchar(255) NOT NULL,
  `location_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`permission_id`),
  KEY `module_id` (`module_id`),
  KEY `ospos_permissions_ibfk_2` (`location_id`),
  CONSTRAINT `ospos_permissions_ibfk_1` FOREIGN KEY (`module_id`) REFERENCES `ospos_modules` (`module_id`) ON DELETE CASCADE,
  CONSTRAINT `ospos_permissions_ibfk_2` FOREIGN KEY (`location_id`) REFERENCES `ospos_stock_locations` (`location_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_permissions`
--

LOCK TABLES `ospos_permissions` WRITE;
/*!40000 ALTER TABLE `ospos_permissions` DISABLE KEYS */;
INSERT INTO `ospos_permissions` VALUES ('config','config',NULL),('customers','customers',NULL),('debcreds','debcreds',NULL),('employees','employees',NULL),('expenses','expenses',NULL),('expenses_categories','expenses_categories',NULL),('giftcards','giftcards',NULL),('home','home',NULL),('items','items',NULL),('items_stock','items',1),('item_kits','item_kits',NULL),('messages','messages',NULL),('office','office',NULL),('receivings','receivings',NULL),('receivings_stock','receivings',1),('reports','reports',NULL),('reports_categories','reports',NULL),('reports_customers','reports',NULL),('reports_discounts','reports',NULL),('reports_employees','reports',NULL),('reports_expenses_categories','reports',NULL),('reports_inventory','reports',NULL),('reports_items','reports',NULL),('reports_payments','reports',NULL),('reports_receivings','reports',NULL),('reports_sales','reports',NULL),('reports_suppliers','reports',NULL),('reports_taxes','reports',NULL),('sales','sales',NULL),('sales_delete','sales',NULL),('sales_stock','sales',1),('suppliers','suppliers',NULL),('taxes','taxes',NULL);
/*!40000 ALTER TABLE `ospos_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_receivings`
--

DROP TABLE IF EXISTS `ospos_receivings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_receivings` (
  `receiving_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `supplier_id` int(10) DEFAULT NULL,
  `employee_id` int(10) NOT NULL DEFAULT '0',
  `comment` text,
  `receiving_id` int(10) NOT NULL AUTO_INCREMENT,
  `payment_type` varchar(20) DEFAULT NULL,
  `reference` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`receiving_id`),
  KEY `supplier_id` (`supplier_id`),
  KEY `employee_id` (`employee_id`),
  KEY `reference` (`reference`),
  CONSTRAINT `ospos_receivings_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `ospos_employees` (`person_id`),
  CONSTRAINT `ospos_receivings_ibfk_2` FOREIGN KEY (`supplier_id`) REFERENCES `ospos_suppliers` (`person_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_receivings`
--

LOCK TABLES `ospos_receivings` WRITE;
/*!40000 ALTER TABLE `ospos_receivings` DISABLE KEYS */;
INSERT INTO `ospos_receivings` VALUES ('2018-08-08 07:44:35',5,1,'',1,'Cash',NULL),('2018-08-08 07:59:10',NULL,1,'',2,'Cash',NULL),('2019-01-01 07:19:28',NULL,7,'',3,'Cash','irfan');
/*!40000 ALTER TABLE `ospos_receivings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_receivings_items`
--

DROP TABLE IF EXISTS `ospos_receivings_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_receivings_items` (
  `receiving_id` int(10) NOT NULL DEFAULT '0',
  `item_id` int(10) NOT NULL DEFAULT '0',
  `description` varchar(30) DEFAULT NULL,
  `serialnumber` varchar(30) DEFAULT NULL,
  `line` int(3) NOT NULL,
  `quantity_purchased` decimal(15,3) NOT NULL DEFAULT '0.000',
  `item_cost_price` decimal(15,2) NOT NULL,
  `item_unit_price` decimal(15,2) NOT NULL,
  `discount_percent` decimal(15,2) NOT NULL DEFAULT '0.00',
  `item_location` int(11) NOT NULL,
  `receiving_quantity` decimal(15,3) NOT NULL DEFAULT '1.000',
  PRIMARY KEY (`receiving_id`,`item_id`,`line`),
  KEY `item_id` (`item_id`),
  CONSTRAINT `ospos_receivings_items_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `ospos_items` (`item_id`),
  CONSTRAINT `ospos_receivings_items_ibfk_2` FOREIGN KEY (`receiving_id`) REFERENCES `ospos_receivings` (`receiving_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_receivings_items`
--

LOCK TABLES `ospos_receivings_items` WRITE;
/*!40000 ALTER TABLE `ospos_receivings_items` DISABLE KEYS */;
INSERT INTO `ospos_receivings_items` VALUES (1,4,'Description Sales',NULL,1,29.000,32.00,32.00,0.00,1,1.000),(1,5,'',NULL,2,32.000,70.00,70.00,0.00,1,1.000),(2,4,'Description Sales',NULL,1,-10.000,32.00,32.00,0.00,1,1.000),(3,25,'',NULL,1,20.000,88.00,88.00,0.00,1,24.000);
/*!40000 ALTER TABLE `ospos_receivings_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_sales`
--

DROP TABLE IF EXISTS `ospos_sales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_sales` (
  `sale_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `customer_id` int(10) DEFAULT NULL,
  `employee_id` int(10) NOT NULL DEFAULT '0',
  `comment` text,
  `invoice_number` varchar(32) DEFAULT NULL,
  `quote_number` varchar(32) DEFAULT NULL,
  `sale_id` int(10) NOT NULL AUTO_INCREMENT,
  `sale_status` tinyint(2) NOT NULL DEFAULT '0',
  `dinner_table_id` int(11) DEFAULT NULL,
  `work_order_number` varchar(32) DEFAULT NULL,
  `sale_type` tinyint(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sale_id`),
  UNIQUE KEY `invoice_number` (`invoice_number`),
  KEY `customer_id` (`customer_id`),
  KEY `employee_id` (`employee_id`),
  KEY `sale_time` (`sale_time`),
  KEY `dinner_table_id` (`dinner_table_id`),
  CONSTRAINT `ospos_sales_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `ospos_employees` (`person_id`),
  CONSTRAINT `ospos_sales_ibfk_2` FOREIGN KEY (`customer_id`) REFERENCES `ospos_customers` (`person_id`),
  CONSTRAINT `ospos_sales_ibfk_3` FOREIGN KEY (`dinner_table_id`) REFERENCES `ospos_dinner_tables` (`dinner_table_id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_sales`
--

LOCK TABLES `ospos_sales` WRITE;
/*!40000 ALTER TABLE `ospos_sales` DISABLE KEYS */;
INSERT INTO `ospos_sales` VALUES ('2018-07-20 08:36:06',6,1,'asas','0',NULL,1,0,NULL,NULL,1),('2018-08-08 06:40:32',NULL,1,'',NULL,NULL,2,0,NULL,NULL,0),('2018-08-08 07:49:24',NULL,1,'',NULL,NULL,3,1,NULL,NULL,1),('2018-08-08 07:54:28',2,1,'','1',NULL,4,0,NULL,NULL,1),('2018-08-08 07:56:00',6,1,'','2',NULL,5,0,NULL,NULL,1),('2018-08-08 08:06:59',2,7,'',NULL,NULL,6,1,NULL,NULL,0),('2018-08-08 08:08:28',6,7,'','3',NULL,7,0,NULL,NULL,1),('2018-08-08 08:12:34',2,1,'','4',NULL,8,0,NULL,NULL,1),('2018-08-08 08:20:03',6,7,'','5',NULL,9,0,NULL,NULL,1),('2018-08-09 09:20:54',10,1,'',NULL,NULL,10,0,NULL,NULL,0),('2018-08-09 09:21:47',2,1,'',NULL,NULL,11,0,NULL,NULL,0),('2018-08-09 14:18:46',NULL,1,'',NULL,NULL,12,0,NULL,NULL,0),('2018-08-10 15:01:46',NULL,1,'',NULL,NULL,15,0,NULL,NULL,0),('2018-12-18 11:06:45',NULL,1,'',NULL,NULL,16,0,NULL,NULL,0),('2018-12-19 13:01:37',NULL,1,'',NULL,NULL,17,0,NULL,NULL,0),('2018-12-20 06:25:27',NULL,7,'',NULL,NULL,18,0,NULL,NULL,0),('2018-12-20 09:50:07',NULL,7,'',NULL,NULL,19,0,NULL,NULL,0),('2018-12-20 11:17:15',NULL,7,'',NULL,NULL,20,0,NULL,NULL,0),('2018-12-20 11:18:48',NULL,7,'',NULL,NULL,21,0,NULL,NULL,0),('2018-12-21 10:14:10',2,7,'',NULL,NULL,22,0,NULL,NULL,0),('2018-12-21 10:49:20',2,7,'',NULL,NULL,23,0,NULL,NULL,0),('2018-12-21 10:51:46',2,7,'',NULL,NULL,24,0,NULL,NULL,0),('2018-12-21 13:55:47',NULL,7,'',NULL,NULL,25,0,NULL,NULL,0),('2018-12-23 06:39:14',NULL,7,'',NULL,NULL,26,1,NULL,NULL,0),('2019-01-01 06:58:49',NULL,7,'',NULL,NULL,27,0,NULL,NULL,0),('2019-01-01 07:06:52',NULL,7,'',NULL,NULL,28,0,NULL,NULL,0),('2019-01-01 09:36:44',NULL,7,'',NULL,NULL,29,0,NULL,NULL,0),('2019-01-01 09:38:45',NULL,7,'',NULL,NULL,30,0,NULL,NULL,0),('2019-01-01 12:46:12',NULL,7,'','6',NULL,31,0,NULL,NULL,1),('2019-01-01 12:47:33',NULL,7,'','7',NULL,32,0,NULL,NULL,1),('2019-01-01 12:49:33',NULL,7,'','8',NULL,33,0,NULL,NULL,1),('2019-01-01 12:53:35',2,7,'','9',NULL,34,0,NULL,NULL,1),('2019-01-01 12:54:52',2,7,'','10',NULL,35,0,NULL,NULL,1),('2019-01-01 12:58:12',NULL,7,'',' 000000000',NULL,36,0,NULL,NULL,1),('2019-01-02 06:18:14',NULL,7,'','12',NULL,37,0,NULL,NULL,1),('2019-01-02 13:48:09',2,7,'','13',NULL,38,0,NULL,NULL,1),('2019-01-02 13:48:58',NULL,7,'',NULL,NULL,39,0,NULL,NULL,0),('2019-01-02 13:49:36',NULL,7,'','14',NULL,40,0,NULL,NULL,1);
/*!40000 ALTER TABLE `ospos_sales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_sales_items`
--

DROP TABLE IF EXISTS `ospos_sales_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_sales_items` (
  `sale_id` int(10) NOT NULL DEFAULT '0',
  `item_id` int(10) NOT NULL DEFAULT '0',
  `description` varchar(255) DEFAULT NULL,
  `serialnumber` varchar(30) DEFAULT NULL,
  `line` int(3) NOT NULL DEFAULT '0',
  `quantity_purchased` decimal(15,3) NOT NULL DEFAULT '0.000',
  `item_cost_price` decimal(15,2) NOT NULL,
  `item_unit_price` decimal(15,2) NOT NULL,
  `discount_percent` decimal(15,2) NOT NULL DEFAULT '0.00',
  `item_location` int(11) NOT NULL,
  `print_option` tinyint(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sale_id`,`item_id`,`line`),
  KEY `sale_id` (`sale_id`),
  KEY `item_id` (`item_id`),
  KEY `item_location` (`item_location`),
  CONSTRAINT `ospos_sales_items_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `ospos_items` (`item_id`),
  CONSTRAINT `ospos_sales_items_ibfk_2` FOREIGN KEY (`sale_id`) REFERENCES `ospos_sales` (`sale_id`),
  CONSTRAINT `ospos_sales_items_ibfk_3` FOREIGN KEY (`item_location`) REFERENCES `ospos_stock_locations` (`location_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_sales_items`
--

LOCK TABLES `ospos_sales_items` WRITE;
/*!40000 ALTER TABLE `ospos_sales_items` DISABLE KEYS */;
INSERT INTO `ospos_sales_items` VALUES (1,3,'','',1,1.000,12.00,121.00,0.00,1,0),(2,1,'','',1,1.000,10.00,11.00,0.00,1,0),(3,4,'Description Sales','',1,4.000,32.00,40.00,0.00,1,0),(4,4,'Description Sales','',1,4.000,32.00,40.00,0.00,1,0),(5,5,'','',1,1.000,70.00,80.00,0.00,1,0),(6,4,'Description Sales','',1,10.000,32.00,40.00,0.00,1,0),(7,4,'Description Sales','',1,1.000,32.00,40.00,0.00,1,0),(8,4,'Description Sales','',1,4.000,32.00,40.00,3.00,1,0),(9,4,'Description Sales','',1,1.000,32.00,40.00,0.00,1,0),(10,6,'','',1,1.000,32.00,44.00,12.00,1,0),(11,4,'Description Sales','',1,1.000,32.00,40.00,0.00,1,0),(12,4,'Description Sales','',1,21.000,32.00,40.00,0.00,1,0),(15,7,'','',1,1.000,15000.00,20000.00,0.00,1,0),(16,5,'','',1,1.000,70.00,80.00,0.00,1,0),(17,13,'','',1,1.000,21.00,12.00,0.00,1,0),(18,16,'','',1,1.000,0.00,0.00,0.00,1,0),(19,16,'','',1,1.000,44.00,32.00,2.00,1,0),(20,16,'','',1,10.000,44.00,32.00,0.00,1,0),(21,16,'','',1,4.000,44.00,32.00,0.00,1,0),(22,16,'','',1,1.000,44.00,32.00,2.00,1,0),(23,16,'','',1,1.000,44.00,32.00,7.00,1,0),(24,12,'Sampel User','',1,1.000,50.00,60.00,10.00,1,0),(24,14,'','',3,1.000,90.00,100.00,5.00,1,0),(24,16,'','',2,1.000,44.00,32.00,8.00,1,0),(25,16,'','',1,1.000,44.00,32.00,2.00,1,0),(26,14,'','',1,1.000,90.00,100.00,2.00,1,0),(27,25,'','',1,20.000,88.00,90.00,1.00,1,0),(28,25,'','',1,15.000,88.00,90.00,0.00,1,0),(29,25,'','',1,2.000,88.00,90.00,0.00,1,0),(30,16,'','',1,1.000,44.00,32.00,0.00,1,0),(31,16,'','',1,1.000,44.00,32.00,0.00,1,0),(32,25,'','',1,1.000,88.00,90.00,0.00,1,0),(33,25,'','',1,1.000,88.00,90.00,0.00,1,0),(34,25,'','',1,1.000,88.00,90.00,5.00,1,0),(35,25,'','',1,1.000,88.00,90.00,5.00,1,0),(36,25,'','',1,1.000,88.00,90.00,0.00,1,0),(37,14,'','',1,1.000,90.00,100.00,0.00,1,0),(38,14,'','',1,1.000,90.00,100.00,5.00,1,0),(39,25,'','',1,1.000,88.00,90.00,0.00,1,0),(40,25,'','',1,1.000,88.00,90.00,0.00,1,0);
/*!40000 ALTER TABLE `ospos_sales_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_sales_items_taxes`
--

DROP TABLE IF EXISTS `ospos_sales_items_taxes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_sales_items_taxes` (
  `sale_id` int(10) NOT NULL,
  `item_id` int(10) NOT NULL,
  `line` int(3) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `percent` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `tax_type` tinyint(2) NOT NULL DEFAULT '0',
  `rounding_code` tinyint(2) NOT NULL DEFAULT '0',
  `cascade_tax` tinyint(2) NOT NULL DEFAULT '0',
  `cascade_sequence` tinyint(2) NOT NULL DEFAULT '0',
  `item_tax_amount` decimal(15,4) NOT NULL DEFAULT '0.0000',
  PRIMARY KEY (`sale_id`,`item_id`,`line`,`name`,`percent`),
  KEY `sale_id` (`sale_id`),
  KEY `item_id` (`item_id`),
  CONSTRAINT `ospos_sales_items_taxes_ibfk_1` FOREIGN KEY (`sale_id`) REFERENCES `ospos_sales_items` (`sale_id`),
  CONSTRAINT `ospos_sales_items_taxes_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `ospos_items` (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_sales_items_taxes`
--

LOCK TABLES `ospos_sales_items_taxes` WRITE;
/*!40000 ALTER TABLE `ospos_sales_items_taxes` DISABLE KEYS */;
INSERT INTO `ospos_sales_items_taxes` VALUES (3,4,1,'Incom',10.0000,1,1,0,0,16.0000),(3,4,1,'Sales',5.0000,1,1,0,0,8.0000),(4,4,1,'Incom',10.0000,1,1,0,0,16.0000),(4,4,1,'Sales',5.0000,1,1,0,0,8.0000),(5,5,1,'Incom',11.0000,1,1,0,0,8.8000),(5,5,1,'sales',5.0000,1,1,0,0,4.0000),(6,4,1,'Incom',10.0000,1,1,0,0,40.0000),(6,4,1,'Sales',5.0000,1,1,0,0,20.0000),(7,4,1,'Incom',10.0000,1,1,0,0,4.0000),(7,4,1,'Sales',5.0000,1,1,0,0,2.0000),(8,4,1,'Incom',10.0000,1,1,0,0,15.5200),(8,4,1,'Sales',5.0000,1,1,0,0,7.7600),(9,4,1,'Incom',10.0000,1,1,0,0,4.0000),(9,4,1,'Sales',5.0000,1,1,0,0,2.0000),(11,4,1,'Incom',10.0000,1,1,0,0,4.0000),(11,4,1,'Sales',5.0000,1,1,0,0,2.0000),(12,4,1,'Incom',10.0000,1,1,0,0,84.0000),(12,4,1,'Sales',5.0000,1,1,0,0,42.0000),(16,5,1,'Incom',11.0000,1,1,0,0,8.8000),(16,5,1,'sales',5.0000,1,1,0,0,4.0000);
/*!40000 ALTER TABLE `ospos_sales_items_taxes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_sales_payments`
--

DROP TABLE IF EXISTS `ospos_sales_payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_sales_payments` (
  `sale_id` int(10) NOT NULL,
  `payment_type` varchar(40) NOT NULL,
  `payment_amount` decimal(15,2) NOT NULL,
  PRIMARY KEY (`sale_id`,`payment_type`),
  KEY `sale_id` (`sale_id`),
  CONSTRAINT `ospos_sales_payments_ibfk_1` FOREIGN KEY (`sale_id`) REFERENCES `ospos_sales` (`sale_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_sales_payments`
--

LOCK TABLES `ospos_sales_payments` WRITE;
/*!40000 ALTER TABLE `ospos_sales_payments` DISABLE KEYS */;
INSERT INTO `ospos_sales_payments` VALUES (1,'Cash',121.00),(2,'Cash',11.00),(3,'Cash',200.00),(4,'Cash',184.00),(5,'Cash',1000.00),(6,'Cash',46.00),(8,'Cash',700.00),(9,'Cash',46.00),(10,'Cash',38.72),(11,'Cash',50.00),(12,'Cash',966.00),(15,'Cash',20000.00),(16,'Cash',92.80),(17,'Cash',100.00),(19,'Cash',31.36),(20,'Cash',400.00),(21,'Cash',128.00),(22,'Cash',50.00),(23,'Cash',50.00),(24,'Cash',200.00),(25,'Cash',100.00),(27,'Cash',5000.00),(28,'Cash',2000.00),(29,'Cash',180.00),(30,'Cash',32.00),(31,'Cash',1200.00),(32,'Cash',100.00),(33,'Cash',100.00),(34,'Cash',100.00),(35,'Cash',100.00),(36,'Cash',100.00),(37,'Cash',100.00),(38,'Cash',100.00),(39,'Cash',90.00),(40,'Cash',90.00);
/*!40000 ALTER TABLE `ospos_sales_payments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_sales_reward_points`
--

DROP TABLE IF EXISTS `ospos_sales_reward_points`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_sales_reward_points` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sale_id` int(11) NOT NULL,
  `earned` float NOT NULL,
  `used` float NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sale_id` (`sale_id`),
  CONSTRAINT `ospos_sales_reward_points_ibfk_1` FOREIGN KEY (`sale_id`) REFERENCES `ospos_sales` (`sale_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_sales_reward_points`
--

LOCK TABLES `ospos_sales_reward_points` WRITE;
/*!40000 ALTER TABLE `ospos_sales_reward_points` DISABLE KEYS */;
/*!40000 ALTER TABLE `ospos_sales_reward_points` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_sales_taxes`
--

DROP TABLE IF EXISTS `ospos_sales_taxes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_sales_taxes` (
  `sale_id` int(10) NOT NULL,
  `tax_type` smallint(2) NOT NULL,
  `tax_group` varchar(32) NOT NULL,
  `sale_tax_basis` decimal(15,4) NOT NULL,
  `sale_tax_amount` decimal(15,4) NOT NULL,
  `print_sequence` tinyint(2) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `tax_rate` decimal(15,4) NOT NULL,
  `sales_tax_code` varchar(32) NOT NULL DEFAULT '',
  `rounding_code` tinyint(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sale_id`,`tax_type`,`tax_group`),
  KEY `print_sequence` (`sale_id`,`print_sequence`,`tax_type`,`tax_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_sales_taxes`
--

LOCK TABLES `ospos_sales_taxes` WRITE;
/*!40000 ALTER TABLE `ospos_sales_taxes` DISABLE KEYS */;
INSERT INTO `ospos_sales_taxes` VALUES (3,1,'10% Incom',160.0000,16.0000,0,'Incom',10.0000,'',1),(3,1,'5% Sales',160.0000,8.0000,1,'Sales',5.0000,'',1),(4,1,'10% Incom',160.0000,16.0000,0,'Incom',10.0000,'',1),(4,1,'5% Sales',160.0000,8.0000,1,'Sales',5.0000,'',1),(5,1,'11% Incom',80.0000,8.8000,0,'Incom',11.0000,'',1),(5,1,'5% sales',80.0000,4.0000,1,'sales',5.0000,'',1),(6,1,'10% Incom',400.0000,40.0000,0,'Incom',10.0000,'',1),(6,1,'5% Sales',400.0000,20.0000,1,'Sales',5.0000,'',1),(7,1,'10% Incom',40.0000,4.0000,0,'Incom',10.0000,'',1),(7,1,'5% Sales',40.0000,2.0000,1,'Sales',5.0000,'',1),(8,1,'10% Incom',155.2000,15.5200,0,'Incom',10.0000,'',1),(8,1,'5% Sales',155.2000,7.7600,1,'Sales',5.0000,'',1),(9,1,'10% Incom',40.0000,4.0000,0,'Incom',10.0000,'',1),(9,1,'5% Sales',40.0000,2.0000,1,'Sales',5.0000,'',1),(11,1,'10% Incom',40.0000,4.0000,0,'Incom',10.0000,'',1),(11,1,'5% Sales',40.0000,2.0000,1,'Sales',5.0000,'',1),(12,1,'10% Incom',840.0000,84.0000,0,'Incom',10.0000,'',1),(12,1,'5% Sales',840.0000,42.0000,1,'Sales',5.0000,'',1),(16,1,'11% Incom',80.0000,8.8000,0,'Incom',11.0000,'',1),(16,1,'5% sales',80.0000,4.0000,1,'sales',5.0000,'',1);
/*!40000 ALTER TABLE `ospos_sales_taxes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_sessions`
--

DROP TABLE IF EXISTS `ospos_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_sessions`
--

LOCK TABLES `ospos_sessions` WRITE;
/*!40000 ALTER TABLE `ospos_sessions` DISABLE KEYS */;
INSERT INTO `ospos_sessions` VALUES ('1cq3kndtv3un0k3bme5hn3t94bfcqr5c','::1',1546672487,_binary '__ci_last_regenerate|i:1546669451;person_id|s:1:\"1\";menu_group|s:4:\"home\";'),('ovhu9r4456l5j7cjcpifn1vvpakv22le','::1',1546673050,_binary '__ci_last_regenerate|i:1546672484;person_id|s:1:\"1\";menu_group|s:4:\"home\";'),('99fuj9t37rk4t2uiubo5v2vftnnr6kut','::1',1546673406,_binary '__ci_last_regenerate|i:1546673047;person_id|s:1:\"1\";menu_group|s:4:\"home\";'),('t4jdcv02s4cr06g015apc4u71bo59oo1','::1',1546673434,_binary '__ci_last_regenerate|i:1546673404;person_id|s:1:\"1\";menu_group|s:4:\"home\";'),('88scd9o2k95iktm6kfgf4sd54tc5ffta','::1',1546674389,_binary '__ci_last_regenerate|i:1546674239;person_id|s:1:\"1\";menu_group|s:4:\"home\";'),('5242qdionfnkf670hh2p02k1jg5k30ie','::1',1546675976,_binary '__ci_last_regenerate|i:1546674557;person_id|s:1:\"1\";menu_group|s:4:\"home\";'),('us3d134d38o2ifafpb7dmv9hqaaul77l','::1',1546684918,_binary '__ci_last_regenerate|i:1546677356;person_id|s:1:\"1\";menu_group|s:4:\"home\";'),('2230n8tev87gq2ef59g04te62inpg96r','::1',1546847133,_binary '__ci_last_regenerate|i:1546847103;person_id|s:1:\"1\";menu_group|s:4:\"home\";'),('6k9vu3fqo8b5inkl8tuidgu4m69hc3rr','::1',1546856077,_binary '__ci_last_regenerate|i:1546856077;'),('lqgp99eqhjkeci42gk19j9b5h0monu3m','::1',1546856077,_binary '__ci_last_regenerate|i:1546856077;'),('pnnu2mr1na3nerp76llsq94evsnv7cb8','::1',1546858865,_binary '__ci_last_regenerate|i:1546858865;person_id|s:1:\"1\";menu_group|s:4:\"home\";item_location|s:1:\"1\";'),('6crgbhhv53haj1cq8tpref9tbt291i3a','::1',1546858912,_binary '__ci_last_regenerate|i:1546858865;person_id|s:1:\"1\";menu_group|s:4:\"home\";item_location|s:1:\"1\";'),('9ku8u3n1rh7lf12d2dv1vto4m2t4n6no','::1',1546877229,_binary '__ci_last_regenerate|i:1546877059;person_id|s:1:\"1\";menu_group|s:4:\"home\";item_location|s:1:\"1\";'),('qna0aehuga614a3jqei4pcp5vvp4sks0','::1',1546924826,_binary '__ci_last_regenerate|i:1546923591;person_id|s:1:\"1\";menu_group|s:4:\"home\";'),('0jutvtje1t7snk6ou8nd1rt4glijg59e','::1',1546926352,_binary '__ci_last_regenerate|i:1546926304;person_id|s:1:\"1\";menu_group|s:4:\"home\";item_location|s:1:\"1\";');
/*!40000 ALTER TABLE `ospos_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_stock_locations`
--

DROP TABLE IF EXISTS `ospos_stock_locations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_stock_locations` (
  `location_id` int(11) NOT NULL AUTO_INCREMENT,
  `location_name` varchar(255) DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`location_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_stock_locations`
--

LOCK TABLES `ospos_stock_locations` WRITE;
/*!40000 ALTER TABLE `ospos_stock_locations` DISABLE KEYS */;
INSERT INTO `ospos_stock_locations` VALUES (1,'stock',0);
/*!40000 ALTER TABLE `ospos_stock_locations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_supplier_deb_cre`
--

DROP TABLE IF EXISTS `ospos_supplier_deb_cre`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_supplier_deb_cre` (
  `dc_id` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `bill_no` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `paid` float NOT NULL,
  `description` text NOT NULL,
  `date_paid` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`dc_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_supplier_deb_cre`
--

LOCK TABLES `ospos_supplier_deb_cre` WRITE;
/*!40000 ALTER TABLE `ospos_supplier_deb_cre` DISABLE KEYS */;
INSERT INTO `ospos_supplier_deb_cre` VALUES (5,5,32,2100,'43','2019-01-05 10:41:55','2018-12-27 19:38:13',1200,'','2019-01-05 15:41:55'),(7,5,401,43000,'121212','2019-01-08 05:20:26','2018-12-28 12:42:11',43000,'','2019-01-08 10:20:26'),(10,5,20,43000,'','2019-01-01 07:42:17','2019-01-01 12:40:34',15000,'','1970-01-01 06:00:00'),(11,5,20,28000,'','2019-01-01 07:51:40','2019-01-01 12:41:28',20000,'2564 3000\r\n10 date 5000','2017-01-02 14:26:00'),(12,5,20,8000,'','2019-01-01 08:00:55','2019-01-01 12:44:42',5000,'','2017-02-01 22:31:00'),(13,5,10,20000,'','2019-01-01 08:02:00','2019-01-01 13:02:00',5000,'','1970-01-01 06:00:00'),(15,5,210,1200,'434344','2019-01-05 08:50:17','2019-01-05 13:38:54',1100,'','2019-01-05 13:50:35'),(16,15,12,13000,'1212','2019-01-08 05:00:42','2019-01-08 10:00:42',12200,'','2019-01-08 10:01:08');
/*!40000 ALTER TABLE `ospos_supplier_deb_cre` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_suppliers`
--

DROP TABLE IF EXISTS `ospos_suppliers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_suppliers` (
  `person_id` int(10) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `agency_name` varchar(255) NOT NULL,
  `account_number` varchar(255) DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `account_number` (`account_number`),
  KEY `person_id` (`person_id`),
  CONSTRAINT `ospos_suppliers_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `ospos_people` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_suppliers`
--

LOCK TABLES `ospos_suppliers` WRITE;
/*!40000 ALTER TABLE `ospos_suppliers` DISABLE KEYS */;
INSERT INTO `ospos_suppliers` VALUES (5,'John Sons','JonhSons','345355454543',0),(15,'Bhola Sweets','Bhola Mithai House',NULL,0);
/*!40000 ALTER TABLE `ospos_suppliers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_tax_categories`
--

DROP TABLE IF EXISTS `ospos_tax_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_tax_categories` (
  `tax_category_id` int(10) NOT NULL AUTO_INCREMENT,
  `tax_category` varchar(32) NOT NULL,
  `tax_group_sequence` tinyint(2) NOT NULL,
  PRIMARY KEY (`tax_category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_tax_categories`
--

LOCK TABLES `ospos_tax_categories` WRITE;
/*!40000 ALTER TABLE `ospos_tax_categories` DISABLE KEYS */;
INSERT INTO `ospos_tax_categories` VALUES (1,'Standard',10),(2,'Service',12),(3,'Alcohol',11);
/*!40000 ALTER TABLE `ospos_tax_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_tax_code_rates`
--

DROP TABLE IF EXISTS `ospos_tax_code_rates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_tax_code_rates` (
  `rate_tax_code` varchar(32) NOT NULL,
  `rate_tax_category_id` int(10) NOT NULL,
  `tax_rate` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `rounding_code` tinyint(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`rate_tax_code`,`rate_tax_category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_tax_code_rates`
--

LOCK TABLES `ospos_tax_code_rates` WRITE;
/*!40000 ALTER TABLE `ospos_tax_code_rates` DISABLE KEYS */;
/*!40000 ALTER TABLE `ospos_tax_code_rates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ospos_tax_codes`
--

DROP TABLE IF EXISTS `ospos_tax_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ospos_tax_codes` (
  `tax_code` varchar(32) NOT NULL,
  `tax_code_name` varchar(255) NOT NULL DEFAULT '',
  `tax_code_type` tinyint(2) NOT NULL DEFAULT '0',
  `city` varchar(255) NOT NULL DEFAULT '',
  `state` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`tax_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ospos_tax_codes`
--

LOCK TABLES `ospos_tax_codes` WRITE;
/*!40000 ALTER TABLE `ospos_tax_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `ospos_tax_codes` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-08 11:18:32
