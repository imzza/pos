<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once("Report.php");

class Inventory_summary extends Report
{
	public function getDataColumns(){
		return array(array('item_name' => $this->lang->line('reports_item_name')),
					array('item_number' => $this->lang->line('reports_item_number')),
					array('quantity' => $this->lang->line('reports_quantity')),
					array('reorder_level' => $this->lang->line('reports_reorder_level')),
					array('location_name' => $this->lang->line('reports_stock_location')),
					array('cost_price' => $this->lang->line('reports_cost_price'), 'sorter' => 'number_sorter'),
					array('unit_price' => $this->lang->line('reports_unit_price'), 'sorter' => 'number_sorter'),
					array('subtotal' => $this->lang->line('reports_sub_total_value'), 'sorter' => 'number_sorter'));
	}

	public function getData(array $inputs)
	{
		$this->db->select('items.item_id,items.name, items.item_number, item_quantities.quantity, items.reorder_level, stock_locations.location_name, items.cost_price,items.assign_employees_qty,items.assign_employees, items.unit_price, (items.cost_price * item_quantities.quantity) AS sub_total_value');
		$this->db->from('items AS items');
		$this->db->join('item_quantities AS item_quantities', 'items.item_id = item_quantities.item_id');
		$this->db->join('stock_locations AS stock_locations', 'item_quantities.location_id = stock_locations.location_id');
		$this->db->where('items.deleted', 0);
		$this->db->where('items.stock_type', 0);
		$this->db->where('stock_locations.deleted', 0);

		//filter items based on employee.
		if ($inputs['employee'] != 'null') {
			$this->db->where('items.employee_id',$inputs['employee']);
			$this->db->or_where('FIND_IN_SET('.$inputs['employee'].', items.assign_employees)');
		}


		// should be corresponding to values Inventory_summary::getItemCountDropdownArray() returns...
		if($inputs['item_count'] == 'zero_and_less')
		{
			$this->db->where('item_quantities.quantity <= 0');
		}
		elseif($inputs['item_count'] == 'more_than_zero')
		{
			$this->db->where('item_quantities.quantity > 0');
		}

		if($inputs['location_id'] != 'all')
		{
			$this->db->where('stock_locations.location_id', $inputs['location_id']);
		}

		

        $this->db->order_by('items.name');

		return $this->db->get()->result_array();
	}

	/**
	 * calculates the total value of the given inventory summary by summing all sub_total_values (see Inventory_summary::getData())
	 *
	 * @param array $inputs expects the reports-data-array which Inventory_summary::getData() returns
	 * @return array
	 */
	public function getSummaryData(array $inputs, $emp='null')
	{
		$return = array('total_inventory_value' => 0, 'total_quantity' => 0, 'total_retail' => 0);
		if ($emp != 'null') {
			foreach($inputs as $input){
				$quant = $this->Item_quantity->get_user_qty_in_stock($input['item_id'], $emp);
				$return['total_inventory_value'] += $input['cost_price'] * $quant;
				$return['total_quantity'] += $quant;
				$return['total_retail'] +=  $input['unit_price'] * $quant;
			}
		}else{
			foreach($inputs as $input){
				$return['total_inventory_value'] += $input['sub_total_value'];
				$return['total_quantity'] += $input['quantity'];
				$return['total_retail'] += $input['unit_price'] * $input['quantity'];
			}
		}
		return $return;
	}

	public function get_user_summary_data($employee = 'null') {
		$this->db->select('items.item_id,items.name, items.item_number, item_quantities.quantity, items.reorder_level, stock_locations.location_name, items.cost_price,items.assign_employees_qty,items.assign_employees, items.unit_price, (items.cost_price * item_quantities.quantity) AS sub_total_value');
		$this->db->from('items AS items');
		$this->db->join('item_quantities AS item_quantities', 'items.item_id = item_quantities.item_id');
		$this->db->join('stock_locations AS stock_locations', 'item_quantities.location_id = stock_locations.location_id');
		$this->db->where('items.deleted', 0);
		$this->db->where('items.stock_type', 0);
		$this->db->where('stock_locations.deleted', 0);

		if ($employee != 'null') {
			$this->db->where('items.employee_id',$employee);
			$this->db->or_where('FIND_IN_SET('.$employee.', items.assign_employees)');
		}


        $this->db->order_by('items.name');

		 $inputs =  $this->db->get()->result_array();

		 return $this->getSummaryData($inputs, $employee);
	}


	/**
	 * returns the array for the dropdown-element item-count in the form for the inventory summary-report
	 *
	 * @return array
	 */
	public function getItemCountDropdownArray(){
		return array('all' => $this->lang->line('reports_all'),
					'zero_and_less' => $this->lang->line('reports_zero_and_less'),
					'more_than_zero' => $this->lang->line('reports_more_than_zero'));
	}
}
?>
