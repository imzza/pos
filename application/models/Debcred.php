<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Expense class
 */ 

class Debcred extends CI_Model
{
	/*
	Determines if a given Expense_id is an Expense
	*/
	public function exists($dc_id)
	{
		$this->db->from('supplier_deb_cre');
		$this->db->where('dc_id', $dc_id);

		return ($this->db->get()->num_rows() == 1);
	}

	/*
	Gets rows
	*/
	public function get_found_rows($search, $filters)
	{
		return $this->search($search, $filters, 0, 0, 'dc_id', 'asc', TRUE);
	}

	/*
	Searches expenses
	*/
	public function search($search, $filters, $rows = 0, $limit_from = 0, $sort = 'dc_id', $order = 'asc', $count_only = FALSE)
	{
		// get_found_rows case
		if($count_only == TRUE)
		{
			$this->db->select('COUNT(DISTINCT supplier_deb_cre.dc_id) as count');
		}
		else
		{
			$this->db->select('
				supplier_deb_cre.dc_id AS dc_id,
				supplier_deb_cre.date_created AS date_created,
				supplier_deb_cre.date_paid AS date_paid,
				supplier_deb_cre.supplier_id AS supplier_id,
				supplier_deb_cre.qty AS quantity,
				supplier_deb_cre.price AS price,
				supplier_deb_cre.bill_no AS bill_no,
				supplier_deb_cre.paid AS paid,
				supplier_deb_cre.status AS status,
				suppliers.company_name AS supplier_name,
			');
		}


		$this->db->from('supplier_deb_cre AS supplier_deb_cre');
		$this->db->join('suppliers AS suppliers', 'suppliers.person_id = supplier_deb_cre.supplier_id', 'LEFT');

		$this->db->where('supplier_deb_cre.status', 'main');

		$this->db->group_start();
			$this->db->like('suppliers.company_name', $search);
			$this->db->or_like('supplier_deb_cre.bill_no', $search);
		$this->db->group_end();


		if(empty($this->config->item('date_or_time_format'))) {
			$this->db->where('DATE_FORMAT(supplier_deb_cre.date_created, "%Y-%m-%d") BETWEEN ' . $this->db->escape($filters['start_date']) . ' AND ' . $this->db->escape($filters['end_date']));
		} else {
			$this->db->where('supplier_deb_cre.date_created BETWEEN ' . $this->db->escape(rawurldecode($filters['start_date'])) . ' AND ' . $this->db->escape(rawurldecode($filters['end_date'])));
		}

		if ($filters['supplier'] !='null') {
			$this->db->where('supplier_id', $filters['supplier']);
		}

		if($count_only == TRUE)
		{
			return $this->db->get()->row()->count;
		}

		$this->db->order_by($sort, $order);

		if($rows > 0)
		{
			$this->db->limit($rows, $limit_from);
		}

		return $this->db->get();
	}

	/*
	Gets information about a particular expense
	*/
	public function get_info($dc_id) {
		$this->db->select('
			supplier_deb_cre.dc_id AS dc_id,
			supplier_deb_cre.qty AS qty,
			supplier_deb_cre.price AS price,
			supplier_deb_cre.supplier_id AS supplier_id,
			supplier_deb_cre.paid AS paid,
			supplier_deb_cre.bill_no AS bill_no,
			supplier_deb_cre.date_paid AS date_paid,
			supplier_deb_cre.description AS description,
			suppliers.company_name AS supplier_name,
		');

		
		$this->db->from('supplier_deb_cre AS supplier_deb_cre');
		$this->db->join('suppliers AS suppliers', 'suppliers.person_id = supplier_deb_cre.supplier_id', 'LEFT');
	
		$this->db->where('dc_id', $dc_id);

		$this->db->where('status', 'main');

		$query = $this->db->get();
		if($query->num_rows() == 1)
		{
			return $query->row();
		} else {
			//Get empty base parent object
			$debcred = new stdClass();

			//Get all the fields from expenses table
			foreach($this->db->list_fields('supplier_deb_cre') as $field)
			{
				$debcred->$field = '';
			}

			return $debcred;
		}
	}

	public function get_info_sub($dc_id) {
		$this->db->select('
			supplier_deb_cre.dc_id AS dc_id,
			supplier_deb_cre.qty AS qty,
			supplier_deb_cre.price AS price,
			supplier_deb_cre.supplier_id AS supplier_id,
			supplier_deb_cre.paid AS paid,
			supplier_deb_cre.bill_no AS bill_no,
			supplier_deb_cre.date_paid AS date_paid,
			supplier_deb_cre.description AS description,
			suppliers.company_name AS supplier_name,
		');

		
		$this->db->from('supplier_deb_cre AS supplier_deb_cre');
		$this->db->join('suppliers AS suppliers', 'suppliers.person_id = supplier_deb_cre.supplier_id', 'LEFT');
	
		$this->db->where('dc_id', $dc_id);

		$this->db->where('status', 'sub');

		$query = $this->db->get();
		if($query->num_rows() == 1)
		{
			return $query->row();
		} else {
			//Get empty base parent object
			$debcred = new stdClass();

			//Get all the fields from expenses table
			foreach($this->db->list_fields('supplier_deb_cre') as $field)
			{
				$debcred->$field = '';
			}

			return $debcred;
		}
	}




	/*
	Inserts or updates an expense
	*/
	public function save(&$debcred_data, $dc_id = FALSE)
	{
		if(!$dc_id || !$this->exists($dc_id))
		{
			if($this->db->insert('supplier_deb_cre', $debcred_data))
			{
				$debcred_data['dc_id'] = $this->db->insert_id();

				return TRUE;
			}

			return FALSE;
		}

		$this->db->where('dc_id', $dc_id);

		return $this->db->update('supplier_deb_cre', $debcred_data);
	}

	/*
	Deletes a list of expense_category
	*/
	public function delete_list($dc_ids)
	{
		$success = FALSE;
		//Run these queries as a transaction, we want to make sure we do all or nothing
		$this->db->trans_start();
			$this->db->where_in('dc_id', $dc_ids);
			$success = $this->db->delete('supplier_deb_cre');
		$this->db->trans_complete();
		return $success;
	}

	/*
	Gets the payment summary for the expenses (expenses/manage) view
	*/
	public function get_payments_summary($search, $filters)
	{
		// get payment summary
		$this->db->select('payment_type, COUNT(amount) AS count, SUM(amount) AS amount');
		$this->db->from('expenses');
		$this->db->where('deleted', $filters['is_deleted']);

		if(empty($this->config->item('date_or_time_format')))
		{
			$this->db->where('DATE_FORMAT(date, "%Y-%m-%d") BETWEEN ' . $this->db->escape($filters['start_date']) . ' AND ' . $this->db->escape($filters['end_date']));
		}
		else
		{
			$this->db->where('date BETWEEN ' . $this->db->escape(rawurldecode($filters['start_date'])) . ' AND ' . $this->db->escape(rawurldecode($filters['end_date'])));
		}

		if($filters['only_cash'] != FALSE)
		{
			$this->db->like('payment_type', $this->lang->line('expenses_cash'));
		}

		if($filters['only_due'] != FALSE)
		{
			$this->db->like('payment_type', $this->lang->line('expenses_due'));
		}

		if($filters['only_check'] != FALSE)
		{
			$this->db->like('payment_type', $this->lang->line('expenses_check'));
		}

		if($filters['only_credit'] != FALSE)
		{
			$this->db->like('payment_type', $this->lang->line('expenses_credit'));
		}

		if($filters['only_debit'] != FALSE)
		{
			$this->db->like('payment_type', $this->lang->line('expenses_debit'));
		}

		$this->db->group_by('payment_type');

		$payments = $this->db->get()->result_array();

		return $payments;
	}

	/*
	Gets the payment options to show in the expense forms
	*/
	public function get_payment_options()
	{
		return get_payment_options();
	}

	/*
	Gets the expense payment
	*/
	public function get_expense_payment($expense_id)
	{
		$this->db->from('expenses');
		$this->db->where('expense_id', $expense_id);

		return $this->db->get();
	}


	public function get_bill_payments($bill_no=-1, $supplier_id=-1) {
		$this->db->where(['bill_no' => $bill_no, 'supplier_id' => $supplier_id, 'status' => 'sub']);
		$this->db->order_by('dc_id', 'desc');
		$res = $this->db->get('supplier_deb_cre');

		if ($res->num_rows() > 0) {
			return $res->result();
		}else{
			return false;
		}
	}





}
?>
