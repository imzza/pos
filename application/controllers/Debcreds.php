<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once("Secure_Controller.php");

class Debcreds extends Secure_Controller {

	public function __construct()
	{
		parent::__construct('debcreds');
		//Do your magic here
	}

	public function index()
	{
		$data['table_headers'] = $this->xss_clean(get_debcred_manage_table_headers());
		// filters that will be loaded in the multiselect dropdown
		$suppliers = array('null' => 'Select supplier');
		foreach($this->Supplier->get_all()->result_array() as $row)
		{
			$suppliers[$this->xss_clean($row['person_id'])] = $this->xss_clean($row['company_name']);
		}
		$data['suppliers'] = $suppliers;
		$data['filters'] = array('only_cash' => $this->lang->line('expenses_cash_filter'));
		$this->load->view('debcred/manage', $data);
	}

	public function search()
	{	
		// echo json_encode($_GET);exit;
		$search   = $this->input->get('search');
		$limit    = $this->input->get('limit');
		$offset   = $this->input->get('offset');
		$sort     = $this->input->get('sort');
		$order    = $this->input->get('order');
		$filters  = array(
					 'start_date' => $this->input->get('start_date'),
					 'end_date' => $this->input->get('end_date')
					);

		// check if any filter is set in the multiselect dropdown

		$filters['supplier'] = $this->input->get('supplier');
		$debcreds = $this->Debcred->search($search, $filters, $limit, $offset, $sort, $order);
		$last = $this->db->last_query();
		$total_rows = $this->Debcred->get_found_rows($search, $filters);
		
		$data_rows = array();
		foreach($debcreds->result() as $debcred)
		{
			$data_rows[] = $this->xss_clean(get_debcred_data_row($debcred));
		}

		if($total_rows > 0)
		{
			$data_rows[] = $this->xss_clean(get_debcreds_data_last_row($debcreds));
		}


		echo json_encode(array('total' => $total_rows, 'rows' => $data_rows, 'last' => $last));
	}

	public function view($dc_id = -1)
	{
		$data = array();

		$suppliers = array('' => 'None');
		foreach($this->Supplier->get_all()->result_array() as $row)
		{
			$suppliers[$this->xss_clean($row['person_id'])] = $this->xss_clean($row['company_name']);
		}
		
		$data['suppliers'] = $suppliers;
		$data['debcred'] = $this->Debcred->get_info($dc_id); 
		$dc_id = $data['debcred']->dc_id;
		
		$this->load->view("debcred/form", $data);
	}

	public function pay($dc_id = -1)
	{
		$data = array();

		$suppliers = array('' => 'None');
		foreach($this->Supplier->get_all()->result_array() as $row)
		{
			$suppliers[$this->xss_clean($row['person_id'])] = $this->xss_clean($row['company_name']);
		}
		
		$data['suppliers'] = $suppliers;
		$data['debcred'] = $this->Debcred->get_info($dc_id); 
		$dc_id = $data['debcred']->dc_id;
		
		$this->load->view("debcred/pay", $data);
	}

	public function get_row($row_id)
	{
		$expense_info = $this->Expense->get_info($row_id);
		$data_row = $this->xss_clean(get_expenses_data_row($expense_info));

		echo json_encode($data_row);
	}

	public function save($dc_id = -1)
	{
		// echo json_encode($_POST);exit;
		// $newdate =date('Y-m-d H:i:s');

		// $date_formatter = date_create_from_format($this->config->item('dateformat') . ' ' . $this->config->item('timeformat'), $newdate);

		$debcred_data = array(
			'date_paid' => date('Y-m-d H:i:s', strtotime($this->input->post('date_paid'))),
			'date_created' => date('Y-m-d H:i:s'),
			'supplier_id' => $this->input->post('supplier_id') == '' ? NULL : $this->input->post('supplier_id'),
			'bill_no' => $this->input->post('bill_no') == '' ? NULL : $this->input->post('bill_no'),
			'price' => parse_decimals($this->input->post('price')),
			'paid' => parse_decimals($this->input->post('paid')),
			'qty' => $this->input->post('quantity'),
			'status' => 'main',
			'description' => $this->input->post('description')
		);


		if($this->Debcred->save($debcred_data, $dc_id))
		{
			$debcred_data = $this->xss_clean($debcred_data);
			if ($dc_id == -1) {
				if ((float) $this->input->post('paid') !=0) {
						$new_d = array(
						'date_paid' => date('Y-m-d H:i:s'), //$date_formatter->format
						'date_created' => date('Y-m-d H:i:s'),
						'supplier_id' => $this->input->post('supplier_id') == '' ? NULL : $this->input->post('supplier_id'),
						'bill_no' => $this->input->post('bill_no') == '' ? NULL : $this->input->post('bill_no'),
						'paid' =>  parse_decimals($this->input->post('paid')),
						'status' => 'sub',
						'description' => $this->input->post('description')
					);
					$this->Debcred->save($new_d);
				}
			}
			

			//New expense_id
			if($dc_id == -1){
				echo json_encode(array('success' => TRUE, 'message' => $this->lang->line('debcreds_successful_adding'), 'id' => $debcred_data['dc_id']));
			} else {
				echo json_encode(array('success' => TRUE, 'message' => $this->lang->line('debcreds_successful_updating'), 'id' => $dc_id));
			}
		} else {
			echo json_encode(array('success' => FALSE, 'message' => $this->lang->line('debcreds_error_adding_updating'), 'id' => -1));
		}
	}

	public function save_pay($dc_id = -1)
	{ 
		// echo json_encode(['post' => $_POST, 'id' => $dc_id]);exit;
		

		$d_info = $this->Debcred->get_info($dc_id);

		$pay = $this->input->post('pay_now') == '' ? 0 : $this->input->post('pay_now');

		$paid = ($d_info->paid + $pay);

		// echo json_encode(['paid' => $paid,'id' => $dc_id, 'info' => $d_info]);exit;
		

		$debcred_data = array(
			'date_paid' => date('Y-m-d H:i:s'), //$date_formatter->format
			'paid' => parse_decimals($paid),
			'description' => $this->input->post('description')
		);

		if($this->Debcred->save($debcred_data, $dc_id))
		{
			$debcred_data = $this->xss_clean($debcred_data);
			//add ne wpayment
			$new_d = array(
				'date_paid' => date('Y-m-d H:i:s'), //$date_formatter->format
				'date_created' => date('Y-m-d H:i:s'),
				'supplier_id' => $this->input->post('hsuplier') == '' ? NULL : $this->input->post('hsuplier'),
				'bill_no' => $this->input->post('hbill') == '' ? NULL : $this->input->post('hbill'),
				'paid' => parse_decimals($pay),
				'status' => 'sub',
				'description' => $this->input->post('description')
			);
			$this->Debcred->save($new_d);

			//New expense_id
			
				echo json_encode(array('success' => TRUE, 'message' => $this->lang->line('debcreds_successful_updating'), 'id' => $dc_id));
		
		}
		else//failure
		{
			echo json_encode(array('success' => FALSE, 'message' => $this->lang->line('debcreds_error_adding_updating'), 'id' => -1));
		}
	}

	public function save_bill() {

		$amt = (int) $this->input->post('amount');

		$bill_id = (int) $this->input->post('id');
		$dc_id = (int) $this->input->post('dc_id');
		$bill = $this->Debcred->get_info_sub($bill_id);
		$dc = $this->Debcred->get_info($dc_id);


		$new_paid = ($bill->paid - $amt);
		$data_main = array(
			'paid' => ($dc->paid - $new_paid),
			'date_updated' => date('Y-m-d H:i:s')
		);

		$data_sub = array(
			'paid' => $amt,
			'date_updated' => date('Y-m-d H:i:s')
		);

		$this->db->trans_start();
		$this->Debcred->save($data_main, $dc_id);
		$this->Debcred->save($data_sub, $bill_id);
		$this->db->trans_complete();

		// $this->Debcred->delete_list($bill_id);
		if ($this->db->trans_status() === FALSE) {
			echo json_encode(array('status' => 'failed', 'message' => 'Unable to update.'));
		}else{
			echo json_encode(array('status' => 'success', 'message' => 'updated successfully.'));
		}
	}

	public function del_bill() {
		$bill_id = (int) $this->input->post('id');
		$dc_id = (int) $this->input->post('dc_id');
		$bill = $this->Debcred->get_info($bill_id);
		$dc = $this->Debcred->get_info($dc_id);

		$data = array(
			'paid' => ($dc->paid - $bill->paid),
			'date_updated' => date('Y-m-d H:i:s')
		);

		$this->db->trans_start();
		$this->Debcred->save($data, $dc_id);
		$this->Debcred->delete_list($bill_id);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE) {
			echo json_encode(array('status' => 'failed', 'message' => 'Unable to delete.'));
		}else{
			echo json_encode(array('status' => 'success', 'message' => 'Deleted successfully.'));
		}

	}

	public function check_bill() {
		// echo json_encode($_POST);
		// exit;
		if ($this->input->post('dc_id') !== "") {
			$bill_no = $this->Debcred->get_info($this->input->post('dc_id'))->bill_no;
			if ($bill_no == $this->input->post('bill_no')) {
				echo 'true';
			} else {
				$this->db->where('bill_no', $this->input->post('bill_no')); //
				$this->db->where('status', 'main');
				$res = $this->db->get('supplier_deb_cre');
				echo ($res->num_rows() == 0) ? 'true' : 'false';
			}
		}else{
			$this->db->where('bill_no', $this->input->post('bill_no')); //
			$this->db->where('status', 'main');
			$res = $this->db->get('supplier_deb_cre');
			echo ($res->num_rows() == 0) ? 'true' : 'false';
		}
	}

	public function ajax_check_amount()
	{
		$value = $this->input->post();
		$parsed_value = parse_decimals(array_pop($value));
		echo json_encode(array('success' => $parsed_value !== FALSE, 'amount' => to_currency_no_money($parsed_value)));
	}

	public function delete()
	{
		$expenses_to_delete = $this->input->post('ids');

		if($this->Debcred->delete_list($expenses_to_delete))
		{
			echo json_encode(array('success' => TRUE, 'message' => $this->lang->line('debcreds_successful_deleted') . ' ' . count($expenses_to_delete) . ' ' . $this->lang->line('debcreds_one_or_multiple'), 'ids' => $expenses_to_delete));
		}
		else
		{
			echo json_encode(array('success' => FALSE, 'message' => $this->lang->line('debcreds_cannot_be_deleted'), 'ids' => $expenses_to_delete));
		}
	}

}

/* End of file Debcred.php */
/* Location: ./application/controllers/Debcred.php */