<div id="required_fields_message"><?php echo $this->lang->line('common_fields_required_message'); ?></div>

<ul id="error_message_box" class="error_message_box"></ul>

<?php echo form_open('items/save/'.$item_info->item_id, array('id'=>'item_form', 'enctype'=>'multipart/form-data', 'class'=>'form-horizontal')); ?>
	<fieldset id="item_basic_info">
		<div class="form-group form-group-sm">
			<?php echo form_label($this->lang->line('items_item_number'), 'item_number', array('class'=>'control-label col-xs-3')); ?>
			<div class='col-xs-8'>
				<div class="input-group">
					<span class="input-group-addon input-sm"><span class="glyphicon glyphicon-barcode"></span></span>
					<?php echo form_input(array(
							'name'=>'item_number',
							'id'=>'item_number', //item_number
							'class'=>'form-control input-sm',
							'value'=>$item_info->item_number)
							);?>
				</div>
			</div>
		</div>

		<div class="form-group form-group-sm">
			<?php echo form_label($this->lang->line('items_name'), 'name', array('class'=>'required control-label col-xs-3')); ?>
			<div class='col-xs-8'>
				<?php echo form_input(array(
						'name'=>'name',
						'id'=>'name',
						'class'=>'form-control input-sm',
						'value'=>$item_info->name)
						);?>
			</div>
		</div>

		<div class="form-group form-group-sm">
			<?php echo form_label($this->lang->line('items_category'), 'category', array('class'=>'control-label col-xs-3')); ?>
			<div class='col-xs-8'>
				<?php echo form_dropdown('category', $categories, $item_info->category, array('class'=>'form-control')); ?>
			</div>
		</div>


		<!-- new category goes here -->

		<!-- AddDynamic Category -->
		<?= form_hidden('add_new_category', 'no'); ?>
		<div class="form-group form-group-sm">
			<?php echo form_label('Add new category.', 'add_new_category', array('class'=>'control-label col-xs-3')); ?>
			<div class='col-xs-1'>
				<?php echo form_checkbox(array(
						'name'=>'add_new_category',
						'id'=>'add_new_category',
						'data-toggle'=>'collapse',
						'data-target'=>'#hidecte',
						'value'=>'yes'));?>
			</div>
		</div> 
		<div class="form-group collapse in form-group-sm" id="hidecte">
			<?php echo form_label('New Category', 'new_category', array('class'=>'control-label col-xs-3')); ?>
			<div class='col-xs-8'>
				<div class="input-group">
					<span class="input-group-addon input-sm"><span class="glyphicon glyphicon-tag"></span></span>
					<?php echo form_input(array(
							'name'=>'new_category',
							'id'=>'new_category',
							'class'=>'form-control input-sm'));?>
				</div>
			</div>
		</div>
		<!-- End Dynamic Category -->

		<?php if ($item_kits_enabled == '1'): ?>
		<div class="form-group form-group-sm">
			<?php echo form_label($this->lang->line('items_stock_type'), 'stock_type', !empty($basic_version) ? array('class'=>'required control-label col-xs-3') : array('class'=>'control-label col-xs-3')); ?>
			<div class="col-xs-8">
				<label class="radio-inline">
					<?php echo form_radio(array(
							'name'=>'stock_type',
							'type'=>'radio',
							'id'=>'stock_type',
							'value'=>0,
							'checked'=>$item_info->stock_type == HAS_STOCK)
					); ?> <?php echo $this->lang->line('items_stock'); ?>
				</label>
				<label class="radio-inline">
					<?php echo form_radio(array(
							'name'=>'stock_type',
							'type'=>'radio',
							'id'=>'stock_type',
							'value'=>1,
							'checked'=>$item_info->stock_type == HAS_NO_STOCK)
					); ?> <?php echo $this->lang->line('items_nonstock'); ?>
				</label>
			</div>
		</div>

		<div class="form-group form-group-sm">
			<?php echo form_label($this->lang->line('items_type'), 'item_type', !empty($basic_version) ? array('class'=>'required control-label col-xs-3') : array('class'=>'control-label col-xs-3')); ?>
			<div class="col-xs-8">
				<label class="radio-inline">
					<?php echo form_radio(array(
							'name'=>'item_type',
							'type'=>'radio',
							'id'=>'item_type',
							'value'=>0,
							'checked'=>$item_info->item_type == ITEM)
					); ?> <?php echo $this->lang->line('items_standard'); ?>
				</label>
				<label class="radio-inline">
					<?php echo form_radio(array(
							'name'=>'item_type',
							'type'=>'radio',
							'id'=>'item_type',
							'value'=>1,
							'checked'=>$item_info->item_type == ITEM_KIT)
					); ?> <?php echo $this->lang->line('items_kit'); ?>
				</label>
				<?php
				if($this->config->item('derive_sale_quantity') == '1') {
				?>
					<label class="radio-inline">
						<?php echo form_radio(array(
								'name' => 'item_type',
								'type' => 'radio',
								'id' => 'item_type',
								'value' => 2,
								'checked' => $item_info->item_type == ITEM_AMOUNT_ENTRY)
						); ?><?php echo $this->lang->line('items_amount_entry'); ?>
					</label>
				<?php
				}
				?>

			</div>
		</div>
		<?php endif; ?>

		<div class="form-group form-group-sm">
			<?php echo form_label($this->lang->line('items_supplier'), 'supplier', array('class'=>'control-label col-xs-3')); ?>
			<div class='col-xs-8'>
				<?php echo form_dropdown('supplier_id', $suppliers, $selected_supplier, array('class'=>'form-control')); ?>
			</div>
		</div>

		<!-- AddDynamic Supplier -->
		<?= form_hidden('add_new_supplier', 'no'); ?>
		<div class="form-group form-group-sm">
			<?php echo form_label('Add new Supplier.', 'add_new_supplier', array('class'=>'control-label col-xs-3')); ?>
			<div class='col-xs-1'>
				<?php echo form_checkbox(array(
						'name'=>'add_new_supplier',
						'id'=>'add_new_supplier',
						'data-toggle'=>'collapse',
						'data-target'=>'#hidesup',
						'value'=>'yes'));?>
			</div>
		</div>
		<div class="form-group collapse in form-group-sm" id="hidesup">
			<?php echo form_label('New Supplier', 'new_supplier', array('class'=>'control-label col-xs-3')); ?>
			<div class='col-xs-8'>
				<div class="input-group">
					<span class="input-group-addon input-sm"><span class="glyphicon glyphicon-tag"></span></span>
					<?php echo form_input(array(
							'name'=>'new_supplier',
							'id'=>'new_supplier',
							'class'=>'form-control input-sm'));?>
				</div>
			</div>
		</div>
		<!-- End Dynamic Category -->



		<div class="form-group form-group-sm">
			<?php echo form_label($this->lang->line('items_cost_price'), 'cost_price', array('class'=>'required control-label col-xs-3')); ?>
			<div class="col-xs-4">
				<div class="input-group input-group-sm">
					<?php if (!currency_side()): ?>
						<span class="input-group-addon input-sm"><b><?php echo $this->config->item('currency_symbol'); ?></b></span>
					<?php endif; ?>
					<?php echo form_input(array(
							'name'=>'cost_price',
							'id'=>'cost_price',
							'class'=>'form-control input-sm',
							'value'=>to_currency_no_money($item_info->cost_price))
							);?>
					<?php if (currency_side()): ?>
						<span class="input-group-addon input-sm"><b><?php echo $this->config->item('currency_symbol'); ?></b></span>
					<?php endif; ?>
				</div>
			</div>
		</div>

		<div class="form-group form-group-sm">
			<?php echo form_label($this->lang->line('items_unit_price'), 'unit_price', array('class'=>'required control-label col-xs-3')); ?>
			<div class='col-xs-4'>
				<div class="input-group input-group-sm">
					<?php if (!currency_side()): ?>
						<span class="input-group-addon input-sm"><b><?php echo $this->config->item('currency_symbol'); ?></b></span>
					<?php endif; ?>
					<?php echo form_input(array(
							'name'=>'unit_price',
							'id'=>'unit_price',
							'class'=>'form-control input-sm',
							'value'=>to_currency_no_money($item_info->unit_price))
							);?>
					<?php if (currency_side()): ?>
						<span class="input-group-addon input-sm"><b><?php echo $this->config->item('currency_symbol'); ?></b></span>
					<?php endif; ?>
				</div>
			</div>
			
		</div>
		<!-- this should be removed -->
			<!-- Tax inputs Removed -->
		<!-- End this should be removed -->

		<?php if($customer_sales_tax_enabled) { ?>
			<div class="form-group form-group-sm">
				<?php echo form_label($this->lang->line('taxes_tax_category'), 'tax_category', array('class'=>'control-label col-xs-3')); ?>
				<div class='col-xs-8'>
					<?php echo form_dropdown('tax_category_id', $tax_categories, $selected_tax_category, array('class'=>'form-control')); ?>
				</div>
			</div>
		<?php } ?>

		<?php foreach($stock_locations as $key=>$location_detail) { ?>
			<div class="form-group form-group-sm">
				<?php echo form_label($this->lang->line('items_quantity').' '.$location_detail['location_name'], 'quantity_' . $key, array('class'=>'required control-label col-xs-3')); ?>
				<div class='col-xs-4'>
					<?php echo form_input(array(
							'name'=>'quantity_' . $key,
							'id'=>'quantity_' . $key,
							'class'=>'required quantity form-control',
							'value'=>isset($item_info->item_id) ? to_quantity_decimals($location_detail['quantity']) : to_quantity_decimals(0))
							);?>
				</div>

			</div>
		<?php } ?>
		
		<!-- Removed Receivin Qty -->
		
		<div class="form-group form-group-sm">
			<?php echo form_label($this->lang->line('items_reorder_level'), 'reorder_level', array('class'=>'required control-label col-xs-3')); ?>
			<div class='col-xs-4'>
				<?php echo form_input(array(
						'name'=>'reorder_level',
						'id'=>'reorder_level',
						'class'=>'form-control input-sm',
						'value'=>isset($item_info->item_id) ? to_quantity_decimals($item_info->reorder_level) : to_quantity_decimals(12))
						);?>
			</div>
		</div>

		
		<?php $emps = $this->Employee->get_emp_not_admin()->result(); ?>
		<?php $empls = explode(',', $item_info->assign_employees); ?>
		<?php $empls_qty = explode(',', $item_info->assign_employees_qty); ?>

		<?php foreach ($emps as $e): ?>
			<?php if (in_array($e->person_id, $empls)): ?>
				<?php foreach ($empls as $key => $es): ?>
						<?php if ($e->person_id == $es): ?>
							<?php $val = $empls_qty[$key]; ?>
						<?php endif ?>
				<?php endforeach ?>
			<?php else: ?>
				<?php $val = 0; ?>
			<?php endif ?>
			<div class="form-group form-group-sm">
				<?php echo form_label($e->username, 'emps_val', array('class'=>'required control-label col-xs-3')); ?>
				<input type="hidden" name=emps[] value="<?= $e->person_id; ?>">
				<div class='col-xs-4'>
					<?php echo form_input(array(
							'name'=>'emps_val[]',
							'class'=>'form-control input-sm inputsm',
							'value'=>isset($item_info->item_id) ? to_quantity_decimals($val) : to_quantity_decimals(0))
							);?>
				</div>
			</div>
		<?php endforeach; ?>

		<div class="form-group form-group-sm">
			<?php echo form_label($this->lang->line('items_description'), 'description', array('class'=>'control-label col-xs-3')); ?>
			<div class='col-xs-8'>
				<?php echo form_textarea(array(
						'name'=>'description',
						'id'=>'description',
						'rows' => 7,
						'class'=>'form-control input-sm',
						'value'=>$item_info->description)
						);?>
			</div>
		</div>

		<div class="form-group form-group-sm">
			<?php echo form_label($this->lang->line('items_image'), 'items_image', array('class'=>'control-label col-xs-3')); ?>
			<div class='col-xs-8'>
				<div class="fileinput <?php echo $logo_exists ? 'fileinput-exists' : 'fileinput-new'; ?>" data-provides="fileinput">
					<div class="fileinput-new thumbnail" style="width: 100px; height: 100px;"></div>
					<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 100px; max-height: 100px;">
						<img data-src="holder.js/100%x100%" alt="<?php echo $this->lang->line('items_image'); ?>"
							 src="<?php echo $image_path; ?>"
							 style="max-height: 100%; max-width: 100%;">
					</div>
					<div>
						<span class="btn btn-default btn-sm btn-file">
							<span class="fileinput-new"><?php echo $this->lang->line("items_select_image"); ?></span>
							<span class="fileinput-exists"><?php echo $this->lang->line("items_change_image"); ?></span>
							<input type="file" name="item_image" accept="image/*">
						</span>
						<a href="#" class="btn btn-default btn-sm fileinput-exists" data-dismiss="fileinput"><?php echo $this->lang->line("items_remove_image"); ?></a>
					</div>
				</div>
			</div>
		</div>

		<div class="form-group form-group-sm">
			<?php echo form_label($this->lang->line('items_allow_alt_description'), 'allow_alt_description', array('class'=>'control-label col-xs-3')); ?>
			<div class='col-xs-1'>
				<?php echo form_checkbox(array(
						'name'=>'allow_alt_description',
						'id'=>'allow_alt_description',
						'value'=>1,
						'checked'=>($item_info->allow_alt_description) ? 1 : 0)
						);?>
			</div>
		</div>

		<div class="form-group form-group-sm">
			<?php echo form_label($this->lang->line('items_is_serialized'), 'is_serialized', array('class'=>'control-label col-xs-3')); ?>
			<div class='col-xs-1'>
				<?php echo form_checkbox(array(
						'name'=>'is_serialized',
						'id'=>'is_serialized',
						'value'=>1,
						'checked'=>($item_info->is_serialized) ? 1 : 0)
						);?>
			</div>
		</div>

		<div class="form-group form-group-sm">
			<?php echo form_label($this->lang->line('items_is_deleted'), 'is_deleted', array('class'=>'control-label col-xs-3')); ?>
			<div class='col-xs-1'>
				<?php echo form_checkbox(array(
						'name'=>'is_deleted',
						'id'=>'is_deleted',
						'value'=>1,
						'checked'=>($item_info->deleted) ? 1 : 0)
						);?>
			</div>
		</div>

		<?php
		for ($i = 1; $i <= 10; ++$i)
		{
		?>
			<?php
			if($this->config->item('custom'.$i.'_name') != NULL)
			{
				$item_arr = (array)$item_info;
			?>
				<div class="form-group form-group-sm">
					<?php echo form_label($this->config->item('custom'.$i.'_name'), 'custom'.$i, array('class'=>'control-label col-xs-3')); ?>
					<div class='col-xs-8'>
						<?php echo form_input(array(
								'name'=>'custom'.$i,
								'id'=>'custom'.$i,
								'class'=>'form-control input-sm',
								'value'=>$item_arr['custom'.$i])
								);?>
					</div>
				</div>
		<?php
			}
		}
		?>
	</fieldset>
<?php echo form_close(); ?>

<script type="text/javascript">
//validation and submit handling
$(document).ready(function()
{
	$('body').on('input', '.inputsm', function(e){
		e.preventDefault();
		var lth = $(this);
		 var total = $('input[id^=quantity_]').val();
		 var indv = $('.inputsm');
		 var sums = 0;
		 $.each(indv, function(i,el){
		 	sums+=parseInt($(this).val());
		 });
		 if (sums > total) {
		 	lth.val(lth.val().slice(0,-1));
		 	alert('All quantity must be less than total quantity.');
		 	return false;
		 }
	});

	$("#new").click(function() {
		stay_open = true;
		$("#item_form").submit();
	});

	$('.form-group.collapse').collapse();

	$("#submit").click(function() {
		stay_open = false;
	});

	var no_op = function(event, data, formatted){};



	<?php for ($i = 1; $i <= 10; ++$i)
	{
	?>
		$("#custom" + <?php echo $i; ?>).autocomplete({
			source:function (request, response) {
				$.ajax({
					type: 'POST',
					url: "<?php echo site_url('items/suggest_custom');?>",
					dataType: 'json',
					data: $.extend(request, $extend(csrf_form_base(), {field_no: <?php echo $i; ?>})),
					success: function(data) {
						response($.map(data, function(item) {
							return {
								value: item.label
							};
						}))
					}
				});
			},
			delay: 10,
			appendTo: '.modal-content'});
	<?php
	}
	?>

	$("a.fileinput-exists").click(function() {
		$.ajax({
			type: 'GET',
			url: "<?php echo site_url("$controller_name/remove_logo/$item_info->item_id"); ?>",
			dataType: 'json'
		})
	});

	// $('#item_number').on('keyup', function(){
	// 	$.post("<?php //echo base_url($controller_name.'/check_item_number'); ?>", $.extend(csrf_form_base(), { "item_id": "<?php //echo $item_info->item_id; ?>", "item_number": function() { return $("#item_number").val(); } }), function(data){
	// 		console.log(data);
	// 	});
	// });
	
	// var check = $('#add_new_category').not(':checked');
	// console.log(check);
	$('#item_form').validate($.extend({
		submitHandler: function(form, event) {
			$(form).ajaxSubmit({
				success: function(response) {
					var stay_open = dialog_support.clicked_id() != 'submit';
					if (stay_open){
						// set action of item_form to url without item id, so a new one can be created
						$("#item_form").attr("action", "<?php echo site_url("items/save/")?>");
						// use a whitelist of fields to minimize unintended side effects
						$(':text, :password, :file, #description, #item_form').not('.quantity, #reorder_level, #tax_name_1,' +
							'#tax_percent_name_1, #reference_number, #name, #cost_price, #unit_price, #taxed_cost_price, #taxed_unit_price,input[name=emps[]],input[name=emps_val[]],#new_category,#new_supplier').val('');
						// de-select any checkboxes, radios and drop-down menus
						$(':input', '#item_form').not('#item_category_id').removeAttr('checked').removeAttr('selected');
					}
					else
					{
						dialog_support.hide();
					}
					table_support.handle_submit('<?php echo site_url('items'); ?>', response, stay_open);
				},
				dataType: 'json'
			});
		},

		rules:
		{
			name: "required",
			category: {
				required: '#add_new_category:not(:checked)'
			},
			new_category:{
				required: '#add_new_category:checked'
			},
			supplier_id:{
				required: '#add_new_supplier:not(:checked)'
			},
			new_supplier:{
				required: '#add_new_supplier:checked'
			},
			item_number:
			{
				required: false,
				remote:
				{
					url: "<?php echo site_url($controller_name . '/check_item_number')?>",
					type: 'post',
					data: $.extend(csrf_form_base(), {
						"item_id": "<?php echo $item_info->item_id; ?>",
						"item_number": function() {
							return $("#item_number").val();
						}
					})
				}
			},
			cost_price:
			{
				required: true,
				min: 1,
				remote: "<?php echo site_url($controller_name . '/check_numeric')?>"
			},
			unit_price:
			{
				required: true,
				min: 1,
				remote: "<?php echo site_url($controller_name . '/check_numeric')?>"
			},
			<?php
			foreach($stock_locations as $key=>$location_detail)
			{
			?>
				<?php echo 'quantity_' . $key ?>:
				{
					required: true,
					remote: "<?php echo site_url($controller_name . '/check_numeric')?>"
				},
			<?php
			}
			?>
			receiving_quantity:
			{
				required: true,
				remote: "<?php echo site_url($controller_name . '/check_numeric')?>"
			},
			reorder_level:
			{
				required: true,
				remote: "<?php echo site_url($controller_name . '/check_numeric')?>"
			},
			tax_percent:
			{
				required: true,
				remote: "<?php echo site_url($controller_name . '/check_numeric')?>"
			}
		},
		messages:
		{
			name: "<?php echo $this->lang->line('items_name_required'); ?>",
			item_number: "<?php echo $this->lang->line('items_item_number_duplicate'); ?>",

			category: "<?php echo $this->lang->line('items_category_required'); ?>",
			new_category: 'Please enter category name',
			supplier_id: "Please select a supplier.",
			new_supplier: "Please enter a supplier.",
			cost_price:
			{
				required: "<?php echo $this->lang->line('items_cost_price_required'); ?>",
				min: 'Enter a valid price.',
				number: "<?php echo $this->lang->line('items_cost_price_number'); ?>"
			},
			unit_price:
			{
				required: "<?php echo $this->lang->line('items_unit_price_required'); ?>",
				min: 'Enter a valid price.',
				number: "<?php echo $this->lang->line('items_unit_price_number'); ?>"
			},
			<?php
			foreach($stock_locations as $key=>$location_detail)
			{
			?>
				<?php echo 'quantity_' . $key ?>:
				{
					required: "<?php echo $this->lang->line('items_quantity_required'); ?>",
					number: "<?php echo $this->lang->line('items_quantity_number'); ?>"
				},
			<?php
			}
			?>
			receiving_quantity:
			{
				required: "<?php echo $this->lang->line('items_quantity_required'); ?>",
				number: "<?php echo $this->lang->line('items_quantity_number'); ?>"
			},
			reorder_level:
			{
				required: "<?php echo $this->lang->line('items_reorder_level_required'); ?>",
				number: "<?php echo $this->lang->line('items_reorder_level_number'); ?>"
			},
			tax_percent:
			{
				required: "<?php echo $this->lang->line('items_tax_percent_required'); ?>",
				number: "<?php echo $this->lang->line('items_tax_percent_number'); ?>"
			}
		}
	}, form_support.error));
});
</script>