<div id="required_fields_message"><?php echo $this->lang->line('common_fields_required_message'); ?></div>

<ul id="error_message_box" class="error_message_box"></ul>

<?php echo form_open('items/save_inventory/'.$item_info->item_id, array('id'=>'item_form', 'class'=>'form-horizontal')); ?>
	<fieldset id="inv_item_basic_info">
		<div class="form-group form-group-sm">
			<?php echo form_label($this->lang->line('items_item_number'), 'name', array('class'=>'control-label col-xs-3')); ?>
			<div class="col-xs-8">
				<div class="input-group">
					<span class="input-group-addon input-sm"><span class="glyphicon glyphicon-barcode"></span></span>
					<?php echo form_input(array(
							'name'=>'item_number',
							'id'=>'item_number',
							'class'=>'form-control input-sm',
							'disabled'=>'',
							'value'=>$item_info->item_number)
							);?>
				</div>
			</div>
		</div>

		<div class="form-group form-group-sm">
			<?php echo form_label($this->lang->line('items_name'), 'name', array('class'=>'control-label col-xs-3')); ?>
			<div class='col-xs-8'>
				<?php echo form_input(array(
						'name'=>'name',
						'id'=>'name',
						'class'=>'form-control input-sm',
						'disabled'=>'',
						'value'=>$item_info->name)
						); ?>
			</div>
		</div>

		<div class="form-group form-group-sm">
			<?php echo form_label($this->lang->line('items_category'), 'category', array('class'=>'control-label col-xs-3')); ?>
			<div class='col-xs-8'>
				<div class="input-group">
					<span class="input-group-addon input-sm"><span class="glyphicon glyphicon-tag"></span></span>
					<?php echo form_input(array(
							'name'=>'category',
							'id'=>'category',
							'class'=>'form-control input-sm',
							'disabled'=>'',
							'value'=>$item_info->category)
						);?>
				</div>
			</div>
		</div>

		<div class="form-group form-group-sm">
			<?php echo form_label($this->lang->line('items_stock_location'), 'stock_location', array('class'=>'control-label col-xs-3')); ?>
			<div class='col-xs-8'>
				<?php echo form_dropdown('stock_location', $stock_locations, current($stock_locations), array('onchange'=>'fill_quantity(this.value)', 'class'=>'form-control'));	?>
			</div>
		</div>

		<div class="form-group form-group-sm">
			<?php echo form_label($this->lang->line('items_current_quantity'), 'quantity', array('class'=>'control-label col-xs-3')); ?>
			<div class='col-xs-4'>
				<?php echo form_input(array(
						'name'=>'quantity',
						'id'=>'quantity',
						'class'=>'form-control input-sm',
						'disabled'=>'',
						'value'=>to_quantity_decimals(current($item_quantities)))
						); ?>
			</div>
			<?php echo form_hidden('current_hidden_qty', to_quantity_decimals(current($item_quantities))); ?>
		</div>

		<div class="form-group form-group-sm">
			<?php echo form_label($this->lang->line('items_add_minus'), 'quantity', array('class'=>'required control-label col-xs-3')); ?>
			<div class='col-xs-4'>
				<?php echo form_input(array(
						'name'=>'newquantity',
						'id'=>'newquantity',
						'class'=>'form-control input-sm')
						); ?>
			</div>
			<span class="nt"></span>
		</div>
		
		
		<div class="form-group form-group-sm">
			<?php echo form_label('Old Purchase Price', 'old_price', array('class'=>'control-label col-xs-3')); ?>
			<div class='col-xs-4'>
				<?php echo form_input(array(
						'name'=>'old_price',
						'id'=>'old_price',
						'disabled' => '',
						'value' => $item_info->cost_price,
						'class'=>'form-control input-sm')
						); ?>
			</div>
			<?php  echo form_hidden('old_hidden_price', $item_info->cost_price); ?>
		</div>
		<div class="form-group form-group-sm">
			<?php echo form_label('New Purchase Price', 'new_price', array('class'=>'control-label col-xs-3')); ?>
			<div class='col-xs-4'>
				<?php echo form_input(array(
						'name'=>'new_price',
						'id'=>'new_price',
						'class'=>'form-control input-sm')
						); ?>
			</div>
			<span class="avg"></span>
		</div>

		<div class="form-group form-group-sm">
			<?php echo form_label('New Selling Price', 'new_price', array('class'=>'control-label col-xs-3')); ?>
			<div class='col-xs-4'>
				<?php echo form_input(array(
						'name'=>'new_sel_price',
						'id'=>'new_sel_price',
						'class'=>'form-control input-sm')
						); ?>
			</div>
			<span class="nl">Current Sale Price: <?= $item_info->unit_price ?></span>
		</div>



		<div class="form-group form-group-sm">
			<?php echo form_label($this->lang->line('items_inventory_comments'), 'description', array('class'=>'control-label col-xs-3')); ?>
			<div class='col-xs-8'>
				<?php echo form_textarea(array(
						'name'=>'trans_comment',
						'id'=>'trans_comment',
						'class'=>'form-control input-sm')
						);?>
			</div>
		</div>
	</fieldset>
<?php echo form_close(); ?>

<script type="text/javascript">
//validation and submit handling
$(document).ready(function()
{	
	//Display Avg Price on edit.
	$('#new_price').on('input', function(){
		var $cont = $('.avg');
		var new_pl_el = $(this);
		// if (!$.isNumeric(new_pl_el.val())) {
		// 	alert('Please enter a number');
		// 	// new_pl_el.val(new_pl_el.val().substr(new_pl_el.val().length-1));
		// 	return false;
		// }
		var new_p = parseFloat(new_pl_el.val());
		var old = parseFloat($('input[name=old_hidden_price]').val());
		
		var avg = (new_p+old)/2;
		if (new_pl_el.val() !=='') {
			$cont.text('Avg Price: '+ avg);
		}else{
			$cont.text('');
		}
	});

	$('#newquantity').on('input', function(){
		var $cot = $('.nt');
		var new_qty = $(this).val();
		var $old_qty = parseInt($('input[name=current_hidden_qty]').val());
		console.log($old_qty);
		// var $est = 0;
		// if (new_qty.startsWith('-') === false) {
		// 	$est = $old_qty + parseInt(new_qty);
		// }else{
			$est = $old_qty + parseInt(new_qty);
		// }

		if (new_qty !=='') {
			$cot.text('Total Quantity: '+ parseInt($est));
			// $cot.text($est);
		}else{
			$cot.text('');
		}


		console.log(parseFloat($est));
	});

	$('#item_form').validate({
		submitHandler:function(form)
		{
			$(form).ajaxSubmit({
			success:function(response)
			{	
				console.log(response);
				// return false;
				dialog_support.hide();
				table_support.handle_submit('<?php echo site_url('items'); ?>', response);
			},
			dataType: 'json'
		});

		},
		errorLabelContainer: "#error_message_box",
 		wrapper: "li",
		rules: 
		{
			newquantity:
			{
				required:true,
				number:true
			}
   		},
		messages: 
		{
			newquantity:
			{
				required: "<?php echo $this->lang->line('items_quantity_required'); ?>",
				number: "<?php echo $this->lang->line('items_quantity_number'); ?>"
			}
		}
	});
});

function fill_quantity(val) 
{   
    var item_quantities = <?php echo json_encode($item_quantities); ?>;
    document.getElementById("quantity").value = parseFloat(item_quantities[val]).toFixed(<?php echo quantity_decimals(); ?>);
}
</script>
