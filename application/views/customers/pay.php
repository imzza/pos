<div id="required_fields_message"><?php echo $this->lang->line('common_fields_required_message'); ?></div>

<ul id="error_message_box" class="error_message_box"></ul>

<?php echo form_open($controller_name . '/save_pay/' . $person_info->person_id, array('id'=>'customer_payment_form', 'class'=>'form-horizontal')); ?>
	<div class="tab-content">
		<div class="tab-pane fade in active" id="customer_basic_info">
			<fieldset>
				<div class="form-group form-group-sm">
					<?php echo form_label($this->lang->line('common_first_name'), 'first_name', array('class'=>'required control-label col-xs-3')); ?>
					<div class='col-xs-8'>
						<?php echo form_input(array(
								'name'=>'first_name',
								'id'=>'first_name',
								'class'=>'form-control input-sm',
								'value'=>$person_info->first_name)
								);?>
					</div>
				</div>

				<div class="form-group form-group-sm">
					<?php echo form_label($this->lang->line('common_last_name'), 'last_name', array('class'=>'required control-label col-xs-3')); ?>
					<div class='col-xs-8'>
						<?php echo form_input(array(
								'name'=>'last_name',
								'id'=>'last_name',
								'class'=>'form-control input-sm',
								'value'=>$person_info->last_name)
								);?>
					</div>
				</div>

				<div class="form-group form-group-sm">
					<?php echo form_label('Balance', 'balance', array('class' => 'control-label col-xs-3')); ?>
					<div class='col-xs-3'>
						<div class="input-group input-group-sm">
							<?php echo form_input(array(
									'name'=>'balance',
									'id'=>'balance',
									'disabled' =>'disabled',
									'class'=>'form-control input-sm',
									'value'=>parse_decimals($person_info->balance)
								)); ?>
							<span class="input-group-addon input-sm"><b>RS</b></span>
						</div>
					</div>
					<?php echo form_hidden('old_balance', parse_decimals($person_info->balance)); ?>
				</div>

				<div class="form-group form-group-sm">
					<?php echo form_label('Pay Now', 'pay_now', array('class' => 'control-label col-xs-3')); ?>
					<div class='col-xs-3'>
						<div class="input-group input-group-sm">
							<?php echo form_input(array(
									'name'=>'pay_now',
									'id'=>'pay_now',
									'class'=>'form-control input-sm',
									'value'=> set_value('pay_now'))
									); ?>
							<span class="input-group-addon input-sm"><b>RS</b></span>
						</div>
					</div>	
				</div>


				<div class="form-group form-group-sm">
					<?php echo form_label($this->lang->line('customers_date'), 'date', array('class'=>'control-label col-xs-3')); ?>
					<div class='col-xs-8'>
						<div class="input-group">
							<span class="input-group-addon input-sm"><span class="glyphicon glyphicon-calendar"></span></span>
							<?php echo form_input(array(
									'name'=>'date',
									'id'=>'datetime',
									'class'=>'form-control input-sm',
									'value'=>date($this->config->item('dateformat') . ' ' . $this->config->item('timeformat'), strtotime($person_info->date)),
								)); ?>
						</div>
					</div>
				</div>
				
			</fieldset>
		</div>
	</div>
<?php echo form_close(); ?>

<table id="items_count_details" class="table table-striped table-hover">
	<thead>
		<tr style="background-color: #999 !important;">
			<th colspan="4">Customer Payments</th>
		</tr>
		<tr>
			<th width="20%">#</th>
			<th width="35%">Date</th>
			<th width="35%">Amount</th>
			<th width="10%">Action</th>
		</tr>
	</thead>
	<tbody id="inventory_result">
			<tr>
				<td>1</td>
				<td>12-12-2018</td>
				<td class="p_amt" data-id="1" onblur=''>1200</td>
				<td class="del_t" data-id="1"><span class="glyphicon glyphicon-trash" style="cursor: pointer;" ></span></td>
			</tr>
	</tbody>
</table>

<script type="text/javascript">
//validation and submit handling
$(document).ready(function()
{
	

	$('#customer_payment_form').validate($.extend({
		submitHandler: function(form)
		{
			$(form).ajaxSubmit({
				success: function(response)
				{
					console.log(response);
					return false;
					dialog_support.hide();
					table_support.handle_submit('<?php echo site_url($controller_name); ?>', response);
				},
				dataType: 'json'
			});
		},

		rules:
		{
			pay_now: {
				required: true,
				number: true,
				min: 1
			}
		},

		messages:
		{
			pay_now: {
				required: 'Amount is required',
				number: 'Please enter a valid number',
				min: 'Please enter a valid amount'
			}
		}
	}, form_support.error));
});
</script>
