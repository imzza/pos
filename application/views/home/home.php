<?php $this->load->view("partial/header"); ?>

<script type="text/javascript">
	dialog_support.init("a.modal-dlg");
</script>
<h3 class="text-center"><?php echo $this->lang->line('common_welcome_message'); ?></h3>
<div id="home_module_list">
	<?php foreach ($allowed_modules as $module): ?>
		<?php if (!in_array($module->module_id, ['giftcards', 'messages'])): ?>
		<div class="module_item" title="<?php echo $this->lang->line('module_'.$module->module_id.'_desc');?>">
			<a href="<?php echo site_url("$module->module_id");?>"><img src="<?php echo base_url().'images/menubar/'.$module->module_id.'.png';?>" border="0" alt="Menubar Image" /></a>
			<a href="<?php echo site_url("$module->module_id");?>"><?php echo $this->lang->line("module_".$module->module_id) ?></a>
		</div>
	<?php endif ?>
	<?php endforeach ?>
</div>
<?php $this->load->view("partial/footer"); ?>

	<?php if ($this->session->userdata('startup') == 1): ?>
		<?php if ($this->Employee->get_logged_in_employee_info()->person_id == 1): ?>
			<script>
				 BootstrapDialog.show({
				 	title: 'Daily Debit Credit',
				    size: BootstrapDialog.SIZE_WIDE,
				    message: $('<div></div>').load("<?php echo base_url('home/report'); ?>"),
				    buttons: [{
				        label: 'Print',
				        cssClass: 'btn-primary',
				        action: function(dia){
				        	dia.close();
				        	window.open("<?php echo base_url('home/report_print'); ?>");
				        	// printDiv('home_login_popup');
				        }
				    },
				    {
				        label: 'Close',
				        action: function(dialogItself){
				            dialogItself.close();
				        }
				    }]
				});
			</script>
		<?php endif ?>
	<?php endif ?>
