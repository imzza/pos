<div id="required_fields_message"><?php echo $this->lang->line('common_fields_required_message'); ?></div>
<ul id="error_message_box" class="error_message_box"></ul>

<?php echo form_open('debcreds/save/'.$debcred->dc_id, array('id'=>'expenses_edit_form', 'class'=>'form-horizontal')); ?>
	<fieldset id="item_basic_info">
		<div class="form-group form-group-sm">
			<?php echo form_label('Suppier', 'supplier', array('class'=>'control-label col-xs-3')); ?>
			<div class='col-xs-6'>
				<?php echo form_dropdown('supplier_id', $suppliers, $debcred->supplier_id, array('class'=>'form-control', 'id'=>'supplier_id'));?>
			</div>
		</div>
		<!-- type either debet or credit -->
		<!-- <div class="form-group form-group-sm">
			<?php //echo form_label('Tytpe', 'type', array('class'=>'control-label col-xs-3')); ?>
			<div class='col-xs-6'>
				<?php //echo form_dropdown('type', ['debet' => 'Debet', 'credet' => 'Credit'], $debcred->type, array('class'=>'form-control', 'id'=>'type'));?>
			</div>
		</div> -->

		<div class="form-group form-group-sm">
			<?php echo form_label('Bill No', 'bill_no', array('class'=>'required control-label col-xs-3')); ?>
			<div class='col-xs-4'>
				<?php echo form_input(array(
						'name'=>'bill_no',
						'id'=>'bill_no',
						'class'=>'required form-control input-sm',
						'value'=>isset($debcred->dc_id) ? $debcred->bill_no : '')
						);?>
			</div>
		</div>

		<div class="form-group form-group-sm">
			<?php echo form_label('Date Paid', 'date_paid', array('class'=>'required control-label col-xs-3')); ?>
			<div class='col-xs-4'>
				<?php echo form_input(array(
						'name'=>'date_paid',
						'id'=>'date_paid',
						'class'=>'required form-control input-sm',
						'value'=>isset($debcred->dc_id)  ? date('d.m.Y', strtotime($debcred->date_paid)) : '')
						);?>
			</div>
		</div>


		<div class="form-group form-group-sm">
			<?php echo form_label('Quantity', 'quantity', array('class'=>'required control-label col-xs-3')); ?>
			<div class='col-xs-4'>
				<?php echo form_input(array(
						'name'=>'quantity',
						'id'=>'quantity',
						'class'=>'required form-control input-sm',
						'value'=>isset($debcred->dc_id) ? to_quantity_decimals($debcred->qty) : to_quantity_decimals(0))
						);?>
			</div>
		</div>

		<div class="form-group form-group-sm">
			<?php echo form_label('Ammount', 'price', array('class'=>'required control-label col-xs-3')); ?>
			<div class="col-xs-4">
				<div class="input-group input-group-sm">
					<?php if (!currency_side()): ?>
						<span class="input-group-addon input-sm"><b><?php echo $this->config->item('currency_symbol'); ?></b></span>
					<?php endif; ?>
					<?php echo form_input(array(
							'name'=>'price',
							'id'=>'price',
							'class'=>'form-control input-sm',
							'value'=>to_currency_no_money($debcred->price))
							);?>
					<?php if (currency_side()): ?>
						<span class="input-group-addon input-sm"><b><?php echo $this->config->item('currency_symbol'); ?></b></span>
					<?php endif; ?>
				</div>
			</div>
		</div>

		<div class="form-group form-group-sm">
			<?php echo form_label('Paid', 'paid', array('class'=>'required control-label col-xs-3')); ?>
			<div class='col-xs-4'>
				<div class="input-group input-group-sm">
					<?php if (!currency_side()): ?>
						<span class="input-group-addon input-sm"><b><?php echo $this->config->item('currency_symbol'); ?></b></span>
					<?php endif; ?>
					<?php echo form_input(array(
							'name'=>'paid',
							'id'=>'paid',
							'class'=>'form-control input-sm',
							'value'=>to_currency_no_money($debcred->paid))
							);?>
					<?php if (currency_side()): ?>
						<span class="input-group-addon input-sm"><b><?php echo $this->config->item('currency_symbol'); ?></b></span>
					<?php endif; ?>
				</div>
			</div>
		</div>

		<div class="form-group form-group-sm">
			<?php echo form_label('Description', 'description', array('class'=>'control-label col-xs-3')); ?>
			<div class='col-xs-8'>
				<?php echo form_textarea(array(
						'name'=>'description',
						'id'=>'description',
						'rows' => 6,
						'class'=>'form-control input-sm',
						'value'=>$debcred->description)
						);?>
			</div>
		</div>

<?php echo form_close(); ?>

<script type='text/javascript'>
//validation and submit handling
$(document).ready(function()
{
	<?php $this->load->view('partial/datepicker_locale'); ?>

	$("#date_paid").datetimepicker({
		format:'dd.mm.yyyy',
		language:'en-US',
		todayBtn: true,
		todayHighlight: true,
		startDate: '-20d',
		// startView:'decade',
		autoclose:true,
		bootcssVer: 3,
		minView:'month'
	});

	var submit_form = function() {
		$(this).ajaxSubmit({
			success: function(response)
			{
				console.log(response);
				dialog_support.hide();
				table_support.handle_submit('<?php echo site_url('debcreds'); ?>', response);
				table_support.refresh();
			},
			dataType: 'json'
		});
	};

	// $('#bill_no').on('keyup', function(){
	// 	var bl = $(this).val();
	// 	$.post("<?php //echo site_url($controller_name . '/check_bill');  ?>", {bill_no: bl, "dc_id": "<?php //echo $debcred->dc_id; ?>"}, function(data){
	// 		console.log(data);
	// 	});
	// });

	$('#expenses_edit_form').validate($.extend(
	{
		submitHandler: function(form)
		{
			submit_form.call(form);
		},
		rules:
		{
			supplier_id: 'required',
			bill_no: {
				required: true,
				remote: {
			        url: "<?php echo site_url($controller_name . '/check_bill')?>",
			        type: "post",
			        data: $.extend(csrf_form_base(),{
			            "dc_id": "<?php echo $debcred->dc_id; ?>",
						"bill_no": function() {
			               return $( "#bill_no" ).val();
			          }
			        })
			     }
			},
			date_paid:
			{
				required: true
			},
			price:
			{
				required: true,
				number: true
			},
			paid:
			{
				required: true,
				number: true
			},
			quantity: {
				required: true,
				number: true
			}
		},
		messages:
		{
			supplier_id: 'Please select a supplier',
			bill_no: {
				required:  'Please enter a bill number',
				remote: 'Bill number already exists.'
			},
			price:
			{
				required: 'Please enter a valid ammount',
				number: 'Please enter a valid number'

			},
			date_paid:
			{
				required: '<?php echo $this->lang->line('debcreds_date_required'); ?>'

			},
			paid:
			{
				required: 'Please enter a valid ammount',
				number: 'Please enter a valid number'
			},
			quantity: {
				required: 'Please enter a valid quantity',
				number: 'Please enter a valid number'
			}
		}
	}, form_support.error));
});
</script>
