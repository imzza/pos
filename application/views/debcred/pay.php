<div id="required_fields_message"><?php echo $this->lang->line('common_fields_required_message'); ?></div>

<ul id="error_message_box" class="error_message_box"></ul>

<?php echo form_open('debcreds/save_pay/'.$debcred->dc_id, array('id'=>'expenses_edit_form', 'class'=>'form-horizontal')); ?>
	<fieldset id="item_basic_info">
		<div class="form-group form-group-sm">
			<?php echo form_label('Suppier', 'supplier', array('class'=>'control-label col-xs-3')); ?>
			<div class='col-xs-6'>
				<?php echo form_dropdown('supplier_id', $suppliers, $debcred->supplier_id, array('class'=>'form-control', 'id'=>'supplier_id', 'disabled' => ''));?>
			</div>
		</div>
		<?php echo form_hidden('hsuplier', $debcred->supplier_id); ?>
		<!-- type either debet or credit -->
		<!-- <div class="form-group form-group-sm">
			<?php //echo form_label('Tytpe', 'type', array('class'=>'control-label col-xs-3')); ?>
			<div class='col-xs-6'>
				<?php //echo form_dropdown('type', ['debet' => 'Debet', 'credet' => 'Credit'], $debcred->type, array('class'=>'form-control', 'id'=>'type'));?>
			</div>
		</div> -->
		<?php echo form_hidden('hbill', $debcred->bill_no); ?>
		<div class="form-group form-group-sm">
			<?php echo form_label('Bill No', 'bill_no', array('class'=>'required control-label col-xs-3')); ?>
			<div class='col-xs-4'>
				<?php echo form_input(array(
						'name'=>'bill_no',
						'id'=>'bill_no',
						'disabled' => '',
						'class'=>'required form-control input-sm',
						'value'=>isset($debcred->dc_id) ? $debcred->bill_no : '')
						);?>
			</div>
		</div>

		<div class="form-group form-group-sm">
			<?php echo form_label('Quantity', 'quantity', array('class'=>'required control-label col-xs-3')); ?>
			<div class='col-xs-4'>
				<?php echo form_input(array(
						'name'=>'quantity',
						'id'=>'quantity',
						'disabled' => '',
						'class'=>'required form-control input-sm',
						'value'=>isset($debcred->dc_id) ? to_quantity_decimals($debcred->qty) : to_quantity_decimals(0))
						);?>
			</div>
		</div>

		<div class="form-group form-group-sm">
			<?php echo form_label('Amount', 'price', array('class'=>'required control-label col-xs-3')); ?>
			<div class="col-xs-4">
				<div class="input-group input-group-sm">
					<?php if (!currency_side()): ?>
						<span class="input-group-addon input-sm"><b><?php echo $this->config->item('currency_symbol'); ?></b></span>
					<?php endif; ?>
					<?php echo form_input(array(
							'name'=>'price',
							'id'=>'price',
							'disabled' => '',
							'class'=>'form-control input-sm',
							'value'=>to_currency_no_money($debcred->price))
							);?>
					<?php if (currency_side()): ?>
						<span class="input-group-addon input-sm"><b><?php echo $this->config->item('currency_symbol'); ?></b></span>
					<?php endif; ?>
				</div>
			</div>
		</div>

		<div class="form-group form-group-sm">
			<?php echo form_label('Paid', 'paid', array('class'=>'required control-label col-xs-3')); ?>
			<div class='col-xs-4'>
				<div class="input-group input-group-sm">
					<?php if (!currency_side()): ?>
						<span class="input-group-addon input-sm"><b><?php echo $this->config->item('currency_symbol'); ?></b></span>
					<?php endif; ?>
					<?php echo form_input(array(
							'name'=>'paid',
							'id'=>'paid',
							'disabled' => '',
							'class'=>'form-control input-sm',
							'value'=>to_currency_no_money($debcred->paid))
							);?>
					<?php if (currency_side()): ?>
						<span class="input-group-addon input-sm"><b><?php echo $this->config->item('currency_symbol'); ?></b></span>
					<?php endif; ?>
				</div>
			</div>
		</div>

		<div class="form-group form-group-sm">
			<?php echo form_label('Pending', 'pending', array('class'=>'required control-label col-xs-3')); ?>
			<div class='col-xs-4'>
				<div class="input-group input-group-sm">
					<?php if (!currency_side()): ?>
						<span class="input-group-addon input-sm"><b><?php echo $this->config->item('currency_symbol'); ?></b></span>
					<?php endif; ?>
					<?php echo form_input(array(
							'name'=>'pending',
							'id'=>'pending',
							'disabled' => '',
							'class'=>'form-control input-sm',
							'value'=>to_currency_no_money($debcred->price - $debcred->paid))
							);?>
					<?php if (currency_side()): ?>
						<span class="input-group-addon input-sm"><b><?php echo $this->config->item('currency_symbol'); ?></b></span>
					<?php endif; ?>
				</div>
			</div>
		</div>

		<div class="form-group form-group-sm">
			<?php echo form_label('Pay Now', 'pay_now', array('class'=>'required control-label col-xs-3')); ?>
			<div class='col-xs-4'>
				<div class="input-group input-group-sm">
					<?php if (!currency_side()): ?>
						<span class="input-group-addon input-sm"><b><?php echo $this->config->item('currency_symbol'); ?></b></span>
					<?php endif; ?>
					<?php echo form_input(array(
							'name'=>'pay_now',
							'id'=>'pay_now',
							'class'=>'form-control input-sm',
							'value'=>'')
							);?>
					<?php if (currency_side()): ?>
						<span class="input-group-addon input-sm"><b><?php echo $this->config->item('currency_symbol'); ?></b></span>
					<?php endif; ?>
				</div>
			</div>
		</div>

		<div class="form-group form-group-sm">
			<?php echo form_label('Description', 'description', array('class'=>'control-label col-xs-3')); ?>
			<div class='col-xs-8'>
				<?php echo form_textarea(array(
						'name'=>'description',
						'id'=>'description',
						'rows' => 6,
						'class'=>'form-control input-sm',
						'value'=>$debcred->description)
						);?>
			</div>
		</div>



<?php echo form_close(); ?>

<table id="items_count_details" class="table table-striped table-hover">
	<thead>
		<tr style="background-color: #999 !important;">
			<th colspan="4">Bill Payments</th>
		</tr>
		<tr>
			<th width="20%">#</th>
			<th width="35%">Date</th>
			<th width="35%">Amount</th>
			<th width="10%">Action</th>
		</tr>
	</thead>
	<tbody id="inventory_result">
		<?php if ($bills =  $this->Debcred->get_bill_payments($debcred->bill_no,$debcred->supplier_id)) : ?>
			<?php foreach ($bills as $b): ?>
				<tr>
					<td><?php echo $b->dc_id ?></td>
					<td><?php echo $b->date_paid ?></td>
					<td class="p_amt" data-id="<?php echo $b->dc_id ?>" onblur='save_rec(event,this, "<?php echo $b->dc_id; ?>");'><?php echo $b->paid ?></td>
					<td class="del_t" data-id="<?php echo $b->dc_id ?>"><span class="glyphicon glyphicon-trash" style="cursor: pointer;" ></span></td>
				</tr>
			<?php endforeach ?>
		<?php endif; ?>
	</tbody>
</table>

<script type='text/javascript'>
//validation and submit handling


function save_rec(e,vl, id){
	var new_val = vl.innerText;
	let ids = vl.getAttribute('data-id');
	if (isNaN(vl.innerText)) {
		alert('Enter a number.');
		return false;
	}

	$.post("<?php echo site_url($controller_name.'/save_bill'); ?>", {amount: new_val, id: ids, dc_id: "<?php echo $debcred->dc_id; ?>"}, function(data) {
		console.log(data);
		data = JSON.parse(data);
		if (data.status == 'success') {
			table_support.refresh();
			$.notify(data.message, { type: 'success' });
		} else {
			$.notify(data.message, { type: 'danger' });
		}
	});
}


$(document).ready(function()
{
	$('#inventory_result tr:first').find('.p_amt').attr('contenteditable', true);
	$('#inventory_result tr:not(:first)').find('.del_t').html('').data('id', '');

	$('.del_t').on('click',function(e){
		var row = $(this).parent();
		let id = $(this).data('id');
		if (id == '') {return false;}
		let amout = $(this).parent().find('.p_amt').text();
		$.post("<?php echo site_url($controller_name.'/del_bill'); ?>", {id: id, dc_id: "<?php echo $debcred->dc_id; ?>"}, function(data, textStatus, xhr) {
			console.log(data);
			data = JSON.parse(data);
			if (data.status == 'success') {
				row.slideUp(300);
				$.notify(data.message, { type: 'success' });
				table_support.refresh();
			}
		});
	});

	<?php $this->load->view('partial/datepicker_locale'); ?>
	$('#datetime').datetimepicker({
		format: "<?php echo dateformat_bootstrap($this->config->item('dateformat')) . ' ' . dateformat_bootstrap($this->config->item('timeformat'));?>",
		startDate: "<?php echo date($this->config->item('dateformat') . ' ' . $this->config->item('timeformat'), mktime(0, 0, 0, 1, 1, 2017));?>",
		<?php
		$t = $this->config->item('timeformat');
		$m = $t[strlen($t)-1];
		if( strpos($this->config->item('timeformat'), 'a') !== false || strpos($this->config->item('timeformat'), 'A') !== false )
		{ 
		?>
			showMeridian: true,
		<?php 
		}
		else
		{
		?>
			showMeridian: false,
		<?php 
		}
		?>
		minuteStep: 1,
		autoclose: true,
		todayBtn: true,
		todayHighlight: true,
		bootcssVer: 3,
		language: '<?php echo current_language_code(); ?>'
	});


	var submit_form = function() {
		$(this).ajaxSubmit({
			success: function(response)
			{
				console.log(response);
				dialog_support.hide();
				table_support.handle_submit('<?php echo site_url('debcreds'); ?>', response);
				table_support.refresh();
			},
			dataType: 'json'
		});
	};

	$('#expenses_edit_form').validate($.extend(
	{
		submitHandler: function(form)
		{
			submit_form.call(form);
		},
		rules:
		{
			pay_now: {
				required: true,
				number: true
			},
			bill_no: 'required',
		},
		messages:
		{
			pay_now: {
				required: 'Pay now is required',
				number: 'please enter a valid ammoint'
			}
		}
	}, form_support.error));
});
</script>
