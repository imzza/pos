-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 14, 2019 at 12:56 PM
-- Server version: 5.7.24-0ubuntu0.18.04.1
-- PHP Version: 5.6.39-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `stepinns_sispos`
--
CREATE DATABASE IF NOT EXISTS `stepinns_sispos` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `stepinns_sispos`;

-- --------------------------------------------------------

--
-- Table structure for table `ospos_app_config`
--

CREATE TABLE `ospos_app_config` (
  `key` varchar(50) NOT NULL,
  `value` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ospos_app_config`
--

INSERT INTO `ospos_app_config` (`key`, `value`) VALUES
('address', 'P2 33 Jeff Hights Main Bullevard Road Gulberg 3 Lhore'),
('allow_duplicate_barcodes', '0'),
('barcode_content', 'id'),
('barcode_first_row', 'category'),
('barcode_font', '0'),
('barcode_font_size', '10'),
('barcode_formats', 'null'),
('barcode_generate_if_empty', '0'),
('barcode_height', '50'),
('barcode_num_in_row', '2'),
('barcode_page_cellspacing', '20'),
('barcode_page_width', '100'),
('barcode_second_row', 'item_code'),
('barcode_third_row', 'unit_price'),
('barcode_type', 'Code39'),
('barcode_width', '250'),
('cash_decimals', '2'),
('cash_rounding_code', '1'),
('company', 'StepInnSolution'),
('company_logo', ''),
('country_codes', 'PKR'),
('currency_decimals', '2'),
('currency_symbol', 'RS'),
('custom10_name', ''),
('custom1_name', ''),
('custom2_name', ''),
('custom3_name', ''),
('custom4_name', ''),
('custom5_name', ''),
('custom6_name', ''),
('custom7_name', ''),
('custom8_name', ''),
('custom9_name', ''),
('customer_reward_enable', '0'),
('customer_sales_tax_support', '0'),
('dateformat', 'd.m.Y'),
('date_or_time_format', 'date_or_time_format'),
('default_origin_tax_code', ''),
('default_register_mode', 'sale'),
('default_sales_discount', '0'),
('default_tax_1_name', ''),
('default_tax_1_rate', ''),
('default_tax_2_name', ''),
('default_tax_2_rate', ''),
('default_tax_category', 'Standard'),
('default_tax_rate', '8'),
('derive_sale_quantity', '0'),
('dinner_table_enable', '0'),
('email', 'info@stepinnsolution.com'),
('email_receipt_check_behaviour', 'last'),
('enforce_privacy', ''),
('fax', ''),
('financial_year', '1'),
('gcaptcha_enable', '0'),
('gcaptcha_secret_key', '6Lc5EoEUAAAAALyNWeSlv32O-_qtmLSwDtA-TAOO'),
('gcaptcha_site_key', '6Lc5EoEUAAAAAK0aq2oarWh8voQlWrAchbSewRSd'),
('giftcard_number', 'series'),
('invoice_default_comments', 'This is a default comment'),
('invoice_email_message', 'Dear {CU}, In attachment the receipt for sale {ISEQ}'),
('invoice_enable', '1'),
('language', 'english'),
('language_code', 'en-US'),
('last_used_invoice_number', '0'),
('last_used_quote_number', '0'),
('last_used_work_order_number', '0'),
('lines_per_page', '25'),
('line_sequence', '0'),
('mailpath', '/usr/sbin/sendmail'),
('msg_msg', ''),
('msg_pwd', ''),
('msg_src', ''),
('msg_uid', ''),
('notify_horizontal_position', 'center'),
('notify_vertical_position', 'bottom'),
('number_locale', 'en_US'),
('payment_options_order', 'cashdebitcredit'),
('phone', '+92 3056120188'),
('print_bottom_margin', ''),
('print_delay_autoreturn', '0'),
('print_footer', '0'),
('print_header', '0'),
('print_left_margin', ''),
('print_receipt_check_behaviour', 'always'),
('print_right_margin', ''),
('print_silently', '0'),
('print_top_margin', ''),
('protocol', 'mail'),
('quantity_decimals', '0'),
('quote_default_comments', 'This is a default quote comment'),
('receipt_font_size', '11'),
('receipt_show_company_name', '1'),
('receipt_show_description', '1'),
('receipt_show_serialnumber', '1'),
('receipt_show_taxes', '0'),
('receipt_show_total_discount', '1'),
('receipt_template', 'receipt_default'),
('receiving_calculate_average_price', '0'),
('recv_invoice_format', '{CO}'),
('return_policy', 'Return Policy SHould be Placed Here.'),
('sales_invoice_format', '{CO}'),
('sales_quote_format', 'Q%y{QSEQ:6}'),
('smtp_crypto', 'ssl'),
('smtp_host', ''),
('smtp_pass', ''),
('smtp_port', '465'),
('smtp_timeout', '5'),
('smtp_user', ''),
('suggestions_first_column', 'name'),
('suggestions_second_column', ''),
('suggestions_third_column', ''),
('tax_decimals', '2'),
('tax_included', '0'),
('theme', 'flatly'),
('thousands_separator', ''),
('timeformat', 'h:i:s A'),
('timezone', 'Asia/Tashkent'),
('website', 'http://stepinnsolution.com/pos/'),
('work_order_enable', '0'),
('work_order_format', 'W%y{WSEQ:6}');

-- --------------------------------------------------------

--
-- Table structure for table `ospos_customers`
--

CREATE TABLE `ospos_customers` (
  `person_id` int(10) NOT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `account_number` varchar(255) DEFAULT NULL,
  `taxable` int(1) NOT NULL DEFAULT '1',
  `sales_tax_code` varchar(32) NOT NULL DEFAULT '1',
  `discount_percent` decimal(15,2) NOT NULL DEFAULT '0.00',
  `package_id` int(11) DEFAULT NULL,
  `points` int(11) DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `employee_id` int(10) NOT NULL,
  `consent` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ospos_customers`
--

INSERT INTO `ospos_customers` (`person_id`, `company_name`, `account_number`, `taxable`, `sales_tax_code`, `discount_percent`, `package_id`, `points`, `deleted`, `date`, `employee_id`, `consent`) VALUES
(2, NULL, NULL, 1, '', '0.00', NULL, NULL, 0, '2018-07-19 13:59:11', 1, 1),
(3, NULL, NULL, 1, '', '0.00', NULL, NULL, 1, '2018-07-20 02:38:34', 1, 1),
(4, 'kjh', 'kjh', 1, '', '0.00', NULL, NULL, 1, '2018-07-20 02:45:44', 1, 1),
(6, NULL, NULL, 1, '', '0.00', NULL, NULL, 0, '2018-07-20 08:35:47', 1, 1),
(10, NULL, NULL, 0, '', '0.00', NULL, NULL, 0, '2018-08-09 09:19:53', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ospos_customers_packages`
--

CREATE TABLE `ospos_customers_packages` (
  `package_id` int(11) NOT NULL,
  `package_name` varchar(255) DEFAULT NULL,
  `points_percent` float NOT NULL DEFAULT '0',
  `deleted` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ospos_customers_packages`
--

INSERT INTO `ospos_customers_packages` (`package_id`, `package_name`, `points_percent`, `deleted`) VALUES
(1, 'Default', 0, 0),
(2, 'Bronze', 10, 0),
(3, 'Silver', 20, 0),
(4, 'Gold', 30, 0),
(5, 'Premium', 50, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ospos_customers_points`
--

CREATE TABLE `ospos_customers_points` (
  `id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `sale_id` int(11) NOT NULL,
  `points_earned` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ospos_dinner_tables`
--

CREATE TABLE `ospos_dinner_tables` (
  `dinner_table_id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ospos_dinner_tables`
--

INSERT INTO `ospos_dinner_tables` (`dinner_table_id`, `name`, `status`, `deleted`) VALUES
(1, 'Delivery', 0, 0),
(2, 'Take Away', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ospos_employees`
--

CREATE TABLE `ospos_employees` (
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `person_id` int(10) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `hash_version` int(1) NOT NULL DEFAULT '2',
  `language` varchar(48) DEFAULT NULL,
  `language_code` varchar(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ospos_employees`
--

INSERT INTO `ospos_employees` (`username`, `password`, `person_id`, `deleted`, `hash_version`, `language`, `language_code`) VALUES
('admin', '$2y$10$xS3Z3.0DoOPUUUY1w2AeCO0tCfKnoDNN0vKahVowiVkqseMijXmyO', 1, 0, 2, '', ''),
('AHSAN', '$2y$10$DyBPV05S./XeuU8EPLdl/eq4L0vYjST2LbX94D.ukTGMgIycZmmUe', 12, 0, 2, '', ''),
('david', '$2y$10$MoJ0FVzkgwoZYUeSGerK5.XvICX.g1OSRwwQXuv72HbBrGeG.ygJ.', 8, 1, 2, '', ''),
('SHAHROZE', '$2y$10$ARiL5FBzSsW3JFFYBH0IkOebvW7ce0xGQIdsTm.gZTOGm00n8p0gu', 7, 0, 2, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `ospos_expenses`
--

CREATE TABLE `ospos_expenses` (
  `expense_id` int(10) NOT NULL,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `amount` decimal(15,2) NOT NULL,
  `payment_type` varchar(40) NOT NULL,
  `expense_category_id` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `employee_id` int(10) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `supplier_name` varchar(255) DEFAULT NULL,
  `supplier_tax_code` varchar(255) DEFAULT NULL,
  `tax_amount` decimal(15,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ospos_expenses`
--

INSERT INTO `ospos_expenses` (`expense_id`, `date`, `amount`, `payment_type`, `expense_category_id`, `description`, `employee_id`, `deleted`, `supplier_name`, `supplier_tax_code`, `tax_amount`) VALUES
(4, '2018-08-08 08:04:16', '10.00', 'Cash', 1, '', 1, 0, 'River John', '12', '1.00'),
(5, '2018-12-18 11:18:22', '13.00', 'Cash', 1, 'Cock Chips Biscuts.', 1, 1, '', '', '13.00'),
(6, '2018-12-24 07:20:57', '100.00', 'Cash', 1, '', 1, 1, 'abc', '', '100.00'),
(7, '2018-12-24 07:58:41', '100.00', 'Cash', 1, '', 1, 0, 'John Sons', '', '0.00'),
(8, '2018-12-28 10:00:18', '1233.00', 'Cash', 2, '', 1, 0, 'Johnsons', '', '1.00'),
(9, '2019-01-05 08:39:52', '1200.00', 'Cash', 2, '', 1, 0, 'John Sons', '', '0.00');

-- --------------------------------------------------------

--
-- Table structure for table `ospos_expense_categories`
--

CREATE TABLE `ospos_expense_categories` (
  `expense_category_id` int(10) NOT NULL,
  `category_name` varchar(255) DEFAULT NULL,
  `category_description` varchar(255) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ospos_expense_categories`
--

INSERT INTO `ospos_expense_categories` (`expense_category_id`, `category_name`, `category_description`, `deleted`) VALUES
(1, 'Utility', 'Unitilies', 0),
(2, 'Supplier', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ospos_giftcards`
--

CREATE TABLE `ospos_giftcards` (
  `record_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `giftcard_id` int(11) NOT NULL,
  `giftcard_number` varchar(255) DEFAULT NULL,
  `value` decimal(15,2) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `person_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ospos_grants`
--

CREATE TABLE `ospos_grants` (
  `permission_id` varchar(255) NOT NULL,
  `person_id` int(10) NOT NULL,
  `menu_group` varchar(32) DEFAULT 'home'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ospos_grants`
--

INSERT INTO `ospos_grants` (`permission_id`, `person_id`, `menu_group`) VALUES
('config', 1, 'office'),
('customers', 1, 'home'),
('customers', 7, 'home'),
('debcreds', 1, 'home'),
('employees', 1, 'office'),
('expenses', 1, 'home'),
('expenses_categories', 1, 'home'),
('home', 1, 'office'),
('items', 1, 'home'),
('items', 7, 'home'),
('items_stock', 1, '--'),
('items_stock', 7, '--'),
('item_kits', 1, 'home'),
('office', 1, 'home'),
('receivings', 7, 'home'),
('receivings_stock', 7, '--'),
('reports', 1, 'home'),
('reports', 7, 'home'),
('reports_categories', 7, '--'),
('reports_customers', 7, '--'),
('reports_discounts', 7, '--'),
('reports_employees', 1, '--'),
('reports_employees', 7, '--'),
('reports_expenses_categories', 7, '--'),
('reports_inventory', 1, '--'),
('reports_inventory', 7, '--'),
('reports_items', 7, '--'),
('reports_payments', 7, '--'),
('reports_sales', 7, '--'),
('sales', 7, 'home'),
('sales', 12, 'home'),
('sales_delete', 7, '--'),
('sales_delete', 12, '--'),
('sales_stock', 7, '--'),
('sales_stock', 12, '--'),
('suppliers', 1, 'home'),
('taxes', 1, 'office');

-- --------------------------------------------------------

--
-- Table structure for table `ospos_inventory`
--

CREATE TABLE `ospos_inventory` (
  `trans_id` int(11) NOT NULL,
  `trans_items` int(11) NOT NULL DEFAULT '0',
  `trans_user` int(11) NOT NULL DEFAULT '0',
  `trans_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `trans_comment` text NOT NULL,
  `trans_location` int(11) NOT NULL,
  `trans_inventory` decimal(15,3) NOT NULL DEFAULT '0.000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ospos_inventory`
--

INSERT INTO `ospos_inventory` (`trans_id`, `trans_items`, `trans_user`, `trans_date`, `trans_comment`, `trans_location`, `trans_inventory`) VALUES
(1, 1, 1, '2018-07-19 13:58:01', 'Manual Edit of Quantity', 1, '20.000'),
(2, 2, 1, '2018-07-20 04:31:52', 'Manual Edit of Quantity', 1, '4.000'),
(3, 1, 1, '2018-07-20 04:32:19', '', 1, '12.000'),
(4, 3, 1, '2018-07-20 08:35:12', 'Manual Edit of Quantity', 1, '0.000'),
(5, 3, 1, '2018-07-20 08:36:06', 'POS 1', 1, '-1.000'),
(6, 1, 1, '2018-08-08 06:40:32', 'POS 2', 1, '-1.000'),
(7, 1, 1, '2018-08-08 10:51:03', 'Deleted', 1, '-31.000'),
(8, 2, 1, '2018-08-08 10:51:03', 'Deleted', 1, '-4.000'),
(9, 4, 1, '2018-08-08 06:59:27', 'Manual Edit of Quantity', 1, '43.000'),
(10, 5, 1, '2018-08-08 07:08:15', 'Manual Edit of Quantity', 1, '50.000'),
(11, 4, 1, '2018-08-08 07:33:45', '', 1, '-20.000'),
(12, 4, 1, '2018-08-08 07:44:35', 'RECV 1', 1, '29.000'),
(13, 5, 1, '2018-08-08 07:44:35', 'RECV 1', 1, '32.000'),
(14, 4, 1, '2018-08-08 07:54:28', 'POS 4', 1, '-4.000'),
(15, 5, 1, '2018-08-08 07:56:00', 'POS 5', 1, '-1.000'),
(16, 4, 1, '2018-08-08 07:59:10', 'RECV 2', 1, '-10.000'),
(17, 4, 7, '2018-08-08 08:08:28', 'POS 7', 1, '-1.000'),
(18, 4, 1, '2018-08-08 08:12:34', 'POS 8', 1, '-4.000'),
(19, 4, 7, '2018-08-08 08:20:03', 'POS 9', 1, '-1.000'),
(20, 6, 1, '2018-08-09 09:19:24', 'Manual Edit of Quantity', 1, '43.000'),
(21, 6, 1, '2018-08-09 09:20:54', 'POS 10', 1, '-1.000'),
(22, 4, 1, '2018-08-09 09:21:47', 'POS 11', 1, '-1.000'),
(23, 4, 1, '2018-08-09 14:18:46', 'POS 12', 1, '-21.000'),
(28, 7, 1, '2018-08-10 14:57:56', 'Manual Edit of Quantity', 1, '50.000'),
(29, 7, 1, '2018-08-10 15:01:46', 'POS 15', 1, '-1.000'),
(30, 8, 1, '2018-12-17 00:05:59', 'Manual Edit of Quantity', 1, '0.000'),
(31, 9, 1, '2018-12-18 11:01:06', 'Manual Edit of Quantity', 1, '2.000'),
(32, 5, 1, '2018-12-18 11:06:45', 'POS 16', 1, '-1.000'),
(33, 9, 1, '2018-12-18 11:10:51', 'Deleted', 1, '-2.000'),
(34, 10, 1, '2018-12-19 09:28:36', 'Manual Edit of Quantity', 1, '12.000'),
(35, 11, 1, '2018-12-19 10:41:58', 'Manual Edit of Quantity', 1, '12.000'),
(36, 12, 1, '2018-12-19 11:01:50', 'Manual Edit of Quantity', 1, '12.000'),
(37, 12, 1, '2018-12-19 11:14:54', 'Manual Edit of Quantity', 1, '11.000'),
(38, 13, 1, '2018-12-19 12:56:49', 'Manual Edit of Quantity', 1, '11.000'),
(39, 13, 1, '2018-12-19 13:01:38', 'POS 17', 1, '-1.000'),
(40, 14, 1, '2018-12-19 14:33:40', 'Manual Edit of Quantity', 1, '12.000'),
(41, 15, 1, '2018-12-20 04:59:26', 'Manual Edit of Quantity', 1, '12.000'),
(42, 16, 1, '2018-12-20 05:03:35', 'Manual Edit of Quantity', 1, '21.000'),
(43, 16, 7, '2018-12-20 06:25:27', 'POS 18', 1, '-1.000'),
(44, 16, 7, '2018-12-20 09:50:07', 'POS 19', 1, '-1.000'),
(45, 16, 7, '2018-12-20 11:17:16', 'POS 20', 1, '-10.000'),
(46, 16, 7, '2018-12-20 11:18:48', 'POS 21', 1, '-4.000'),
(47, 16, 1, '2018-12-21 07:12:03', 'Manual Edit of Quantity', 1, '10.000'),
(48, 16, 7, '2018-12-21 10:14:10', 'POS 22', 1, '-1.000'),
(49, 16, 7, '2018-12-21 10:49:20', 'POS 23', 1, '-1.000'),
(50, 12, 7, '2018-12-21 10:51:46', 'POS 24', 1, '-1.000'),
(51, 16, 7, '2018-12-21 10:51:46', 'POS 24', 1, '-1.000'),
(52, 14, 7, '2018-12-21 10:51:47', 'POS 24', 1, '-1.000'),
(53, 16, 7, '2018-12-21 13:55:49', 'POS 25', 1, '-1.000'),
(54, 17, 1, '2018-12-24 09:06:34', 'Manual Edit of Quantity', 1, '12.000'),
(55, 18, 1, '2018-12-24 10:12:00', 'Manual Edit of Quantity', 1, '21.000'),
(56, 19, 1, '2018-12-24 10:41:37', 'Manual Edit of Quantity', 1, '20.000'),
(57, 19, 1, '2018-12-24 10:55:50', 'Manual Edit of Quantity', 1, '10.000'),
(58, 20, 1, '2018-12-27 10:38:34', 'Manual Edit of Quantity', 1, '0.000'),
(59, 21, 1, '2018-12-27 11:15:55', 'Manual Edit of Quantity', 1, '12.000'),
(60, 22, 1, '2018-12-27 12:49:18', 'Manual Edit of Quantity', 1, '12.000'),
(61, 23, 7, '2019-01-01 05:42:43', 'Manual Edit of Quantity', 1, '20.000'),
(62, 24, 7, '2019-01-01 06:09:16', 'Manual Edit of Quantity', 1, '20.000'),
(63, 25, 7, '2019-01-01 06:18:17', 'Manual Edit of Quantity', 1, '20.000'),
(64, 25, 7, '2019-01-01 06:27:25', '', 1, '40.000'),
(65, 25, 7, '2019-01-01 06:58:49', 'POS 27', 1, '-20.000'),
(66, 25, 7, '2019-01-01 07:06:52', 'POS 28', 1, '-15.000'),
(67, 25, 7, '2019-01-01 07:19:28', 'RECV 3', 1, '480.000'),
(68, 26, 1, '2019-01-01 09:16:07', 'Manual Edit of Quantity', 1, '50.000'),
(69, 26, 1, '2019-01-01 09:18:57', '', 1, '60.000'),
(70, 25, 7, '2019-01-01 09:36:44', 'POS 29', 1, '-2.000'),
(71, 16, 7, '2019-01-01 09:38:45', 'POS 30', 1, '-1.000'),
(72, 16, 7, '2019-01-01 12:46:12', 'POS 31', 1, '-1.000'),
(73, 25, 7, '2019-01-01 12:47:33', 'POS 32', 1, '-1.000'),
(74, 25, 7, '2019-01-01 12:49:33', 'POS 33', 1, '-1.000'),
(75, 25, 7, '2019-01-01 12:53:35', 'POS 34', 1, '-1.000'),
(76, 25, 7, '2019-01-01 12:54:52', 'POS 35', 1, '-1.000'),
(77, 25, 7, '2019-01-01 12:58:12', 'POS 36', 1, '-1.000'),
(78, 14, 7, '2019-01-02 06:18:14', 'POS 37', 1, '-1.000'),
(79, 27, 1, '2019-01-02 07:42:38', 'Manual Edit of Quantity', 1, '0.000'),
(80, 28, 1, '2019-01-02 07:43:50', 'Manual Edit of Quantity', 1, '0.000'),
(81, 29, 1, '2019-01-02 07:44:32', 'Manual Edit of Quantity', 1, '0.000'),
(82, 30, 1, '2019-01-02 07:47:56', 'Manual Edit of Quantity', 1, '0.000'),
(83, 31, 1, '2019-01-02 07:48:36', 'Manual Edit of Quantity', 1, '0.000'),
(84, 32, 1, '2019-01-02 07:48:38', 'Manual Edit of Quantity', 1, '0.000'),
(85, 13, 1, '2019-01-02 08:23:53', '', 1, '-3.000'),
(86, 12, 1, '2019-01-02 08:24:12', '', 1, '-10.000'),
(87, 12, 1, '2019-01-02 08:24:43', '', 1, '100.000'),
(88, 14, 7, '2019-01-02 13:48:09', 'POS 38', 1, '-1.000'),
(89, 25, 7, '2019-01-02 13:48:58', 'POS 39', 1, '-1.000'),
(90, 25, 7, '2019-01-02 13:49:36', 'POS 40', 1, '-1.000'),
(91, 33, 1, '2019-01-03 09:29:50', 'Manual Edit of Quantity', 1, '1500.000'),
(92, 33, 1, '2019-01-03 10:06:43', '', 1, '1000.000'),
(93, 33, 1, '2019-01-03 10:08:01', '', 1, '-2000.000'),
(94, 33, 1, '2019-01-03 10:12:44', '', 1, '100.000'),
(95, 33, 1, '2019-01-03 10:21:41', '', 1, '10.000'),
(96, 20, 1, '2019-01-03 10:24:16', '', 1, '12.000'),
(97, 20, 1, '2019-01-03 10:25:20', 'Deleted', 1, '-12.000'),
(98, 21, 1, '2019-01-03 10:25:20', 'Deleted', 1, '-12.000'),
(99, 22, 1, '2019-01-03 10:25:20', 'Deleted', 1, '-12.000'),
(100, 23, 1, '2019-01-03 10:25:20', 'Deleted', 1, '-20.000'),
(101, 27, 1, '2019-01-03 10:27:00', 'Manual Edit of Quantity', 1, '150.000'),
(102, 26, 1, '2019-01-03 11:11:57', '', 1, '12.000'),
(103, 26, 1, '2019-01-03 11:12:45', '', 1, '1.000'),
(104, 26, 1, '2019-01-03 11:17:38', 'Manual Edit of Quantity', 1, '27.000'),
(105, 33, 1, '2019-01-07 10:55:09', '', 1, '10.000'),
(106, 18, 1, '2019-01-08 05:45:52', '', 1, '211.000'),
(107, 25, 7, '2019-01-08 07:28:06', 'POS 41', 1, '-1.000'),
(108, 14, 7, '2019-01-08 07:31:43', 'POS 42', 1, '-1.000'),
(109, 24, 7, '2019-01-08 09:26:16', 'Deleted', 1, '-20.000'),
(110, 34, 7, '2019-01-08 11:12:30', 'Manual Edit of Quantity', 1, '13.000'),
(111, 16, 7, '2019-01-11 05:31:03', 'POS 43', 1, '-1.000'),
(112, 16, 7, '2019-01-11 07:08:32', 'POS 44', 1, '-1.000'),
(113, 16, 7, '2019-01-11 07:19:19', 'POS 45', 1, '-2.000'),
(114, 12, 1, '2019-01-11 07:34:39', '', 1, '12.000'),
(115, 16, 7, '2019-01-11 08:05:29', 'POS 46', 1, '-1.000'),
(116, 16, 7, '2019-01-11 08:19:55', 'POS 47', 1, '-1.000'),
(117, 12, 1, '2019-01-14 07:07:57', '', 1, '10.000'),
(118, 12, 1, '2019-01-14 07:09:21', '', 1, '10.000'),
(119, 12, 1, '2019-01-14 07:10:57', '', 1, '10.000');

-- --------------------------------------------------------

--
-- Table structure for table `ospos_items`
--

CREATE TABLE `ospos_items` (
  `name` varchar(255) NOT NULL,
  `category` varchar(255) NOT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `item_number` varchar(255) DEFAULT NULL,
  `description` varchar(255) NOT NULL,
  `cost_price` decimal(15,2) NOT NULL,
  `unit_price` decimal(15,2) NOT NULL,
  `avg_price` float NOT NULL DEFAULT '0',
  `reorder_level` decimal(15,3) NOT NULL DEFAULT '0.000',
  `receiving_quantity` decimal(15,3) NOT NULL DEFAULT '1.000',
  `item_id` int(10) NOT NULL,
  `pic_filename` varchar(255) DEFAULT NULL,
  `allow_alt_description` tinyint(1) NOT NULL,
  `is_serialized` tinyint(1) NOT NULL,
  `stock_type` tinyint(2) NOT NULL DEFAULT '0',
  `item_type` tinyint(2) NOT NULL DEFAULT '0',
  `tax_category_id` int(10) NOT NULL DEFAULT '1',
  `deleted` int(1) NOT NULL DEFAULT '0',
  `custom1` varchar(255) DEFAULT NULL,
  `custom2` varchar(255) DEFAULT NULL,
  `custom3` varchar(255) DEFAULT NULL,
  `custom4` varchar(255) DEFAULT NULL,
  `custom5` varchar(255) DEFAULT NULL,
  `custom6` varchar(255) DEFAULT NULL,
  `custom7` varchar(255) DEFAULT NULL,
  `custom8` varchar(255) DEFAULT NULL,
  `custom9` varchar(255) DEFAULT NULL,
  `custom10` varchar(255) DEFAULT NULL,
  `employee_id` int(11) NOT NULL,
  `assign_employees` varchar(255) NOT NULL,
  `assign_employees_qty` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ospos_items`
--

INSERT INTO `ospos_items` (`name`, `category`, `supplier_id`, `item_number`, `description`, `cost_price`, `unit_price`, `avg_price`, `reorder_level`, `receiving_quantity`, `item_id`, `pic_filename`, `allow_alt_description`, `is_serialized`, `stock_type`, `item_type`, `tax_category_id`, `deleted`, `custom1`, `custom2`, `custom3`, `custom4`, `custom5`, `custom6`, `custom7`, `custom8`, `custom9`, `custom10`, `employee_id`, `assign_employees`, `assign_employees_qty`) VALUES
('To be Delete', 'Laptop', 15, NULL, '', '100.00', '60.00', 75, '1.000', '0.000', 12, NULL, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', 1, '7', '4'),
('Chrismiss Special', 'choclate', 5, NULL, '', '0.00', '0.00', 0, '1.000', '1.000', 15, NULL, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', 1, '8', '12'),
('Only ME', 'saryia', 5, NULL, '', '44.00', '32.00', 0, '1.000', '1.000', 16, NULL, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', 1, '7', '-7'),
('Hello', 'Laptop', 15, '8961006040044', '', '32.00', '40.00', 0, '6.000', '0.000', 17, NULL, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', 1, '', ''),
('', 'asas', NULL, NULL, '', '0.00', '0.00', 0, '1.000', '1.000', 20, NULL, 0, 0, 0, 0, 0, 1, '', '', '', '', '', '', '', '', '', '', 1, '', ''),
('sooper', 'Biscuite', 15, '8964000681107', '', '130.00', '150.00', 0, '1.000', '0.000', 26, NULL, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', 1, '', ''),
('tisshue', 'asas', 5, '8961006040044', '', '100.00', '110.00', 0, '1.000', '0.000', 27, NULL, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', 1, '', ''),
('Mr. admin', 'asas', 5, NULL, '', '0.00', '0.00', 0, '1.000', '1.000', 29, NULL, 0, 0, 0, 0, 0, 1, '', '', '', '', '', '', '', '', '', '', 1, '', ''),
('Gulab Jamun', 'Mithai', 15, NULL, '', '1113.00', '1122.00', 0, '12.000', '0.000', 33, NULL, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', 1, '12,7', '100,100'),
('Rio Choclate', 'Biscuite', 16, NULL, '', '1000.00', '1200.00', 0, '12.000', '0.000', 34, NULL, 0, 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', 1, '12,7', '1,1');

-- --------------------------------------------------------

--
-- Table structure for table `ospos_items_taxes`
--

CREATE TABLE `ospos_items_taxes` (
  `item_id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `percent` decimal(15,3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ospos_items_taxes`
--

INSERT INTO `ospos_items_taxes` (`item_id`, `name`, `percent`) VALUES
(2, 'General', '7.000'),
(2, 'Sales', '12.000'),
(4, 'Incom', '10.000'),
(4, 'Sales', '5.000'),
(5, 'Incom', '11.000'),
(5, 'sales', '5.000');

-- --------------------------------------------------------

--
-- Table structure for table `ospos_item_kits`
--

CREATE TABLE `ospos_item_kits` (
  `item_kit_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `item_id` int(10) NOT NULL DEFAULT '0',
  `kit_discount_percent` decimal(15,2) NOT NULL DEFAULT '0.00',
  `price_option` tinyint(2) NOT NULL DEFAULT '0',
  `print_option` tinyint(2) NOT NULL DEFAULT '0',
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ospos_item_kits`
--

INSERT INTO `ospos_item_kits` (`item_kit_id`, `name`, `item_id`, `kit_discount_percent`, `price_option`, `print_option`, `description`) VALUES
(1, 'dummy', 0, '1.00', 2, 0, 'Dum description'),
(2, 'General', 0, '12.00', 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `ospos_item_kit_items`
--

CREATE TABLE `ospos_item_kit_items` (
  `item_kit_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `quantity` decimal(15,3) NOT NULL,
  `kit_sequence` int(3) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ospos_item_quantities`
--

CREATE TABLE `ospos_item_quantities` (
  `item_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `quantity` decimal(15,3) NOT NULL DEFAULT '0.000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ospos_item_quantities`
--

INSERT INTO `ospos_item_quantities` (`item_id`, `location_id`, `quantity`) VALUES
(12, 1, '154.000'),
(13, 1, '7.000'),
(14, 1, '8.000'),
(15, 1, '12.000'),
(16, 1, '3.000'),
(17, 1, '12.000'),
(18, 1, '232.000'),
(19, 1, '30.000'),
(20, 1, '0.000'),
(21, 1, '0.000'),
(22, 1, '0.000'),
(23, 1, '0.000'),
(24, 1, '0.000'),
(25, 1, '495.000'),
(26, 1, '150.000'),
(27, 1, '150.000'),
(28, 1, '0.000'),
(29, 1, '0.000'),
(30, 1, '0.000'),
(31, 1, '0.000'),
(32, 1, '0.000'),
(33, 1, '620.000'),
(34, 1, '13.000');

-- --------------------------------------------------------

--
-- Table structure for table `ospos_migrations`
--

CREATE TABLE `ospos_migrations` (
  `version` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ospos_migrations`
--

INSERT INTO `ospos_migrations` (`version`) VALUES
(20180501100000);

-- --------------------------------------------------------

--
-- Table structure for table `ospos_modules`
--

CREATE TABLE `ospos_modules` (
  `name_lang_key` varchar(255) NOT NULL,
  `desc_lang_key` varchar(255) NOT NULL,
  `sort` int(10) NOT NULL,
  `module_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ospos_modules`
--

INSERT INTO `ospos_modules` (`name_lang_key`, `desc_lang_key`, `sort`, `module_id`) VALUES
('module_config', 'module_config_desc', 110, 'config'),
('module_customers', 'module_customers_desc', 10, 'customers'),
('module_debcreds\r\n', 'module_debcreds_desc\r\n', 700, 'debcreds'),
('module_employees', 'module_employees_desc', 80, 'employees'),
('module_expenses', 'module_expenses_desc', 108, 'expenses'),
('module_expenses_categories', 'module_expenses_categories_desc', 109, 'expenses_categories'),
('module_giftcards', 'module_giftcards_desc', 90, 'giftcards'),
('module_home', 'module_home_desc', 1, 'home'),
('module_items', 'module_items_desc', 20, 'items'),
('module_item_kits', 'module_item_kits_desc', 30, 'item_kits'),
('module_messages', 'module_messages_desc', 98, 'messages'),
('module_office', 'module_office_desc', 999, 'office'),
('module_receivings', 'module_receivings_desc', 60, 'receivings'),
('module_reports', 'module_reports_desc', 50, 'reports'),
('module_sales', 'module_sales_desc', 70, 'sales'),
('module_suppliers', 'module_suppliers_desc', 40, 'suppliers'),
('module_taxes', 'module_taxes_desc', 105, 'taxes');

-- --------------------------------------------------------

--
-- Table structure for table `ospos_people`
--

CREATE TABLE `ospos_people` (
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `gender` int(1) DEFAULT NULL,
  `phone_number` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `address_1` varchar(255) NOT NULL,
  `address_2` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `zip` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `comments` text NOT NULL,
  `person_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ospos_people`
--

INSERT INTO `ospos_people` (`first_name`, `last_name`, `gender`, `phone_number`, `email`, `address_1`, `address_2`, `city`, `state`, `zip`, `country`, `comments`, `person_id`) VALUES
('System', 'Administrator', NULL, '555-555-5555', 'stepinnsolution@gmail.com', 'Address 1', '', '', '', '', '', '', 1),
('Ashiq ', 'Khaki', 1, '323232323', 'ashiq@sis.com', 'abc address', 'address 2', 'lahore', 'punjab', '54000', 'pakistan', '', 2),
('Hafiz', 'Adil', 1, '03343938883', 'hafizadil431@gmail.com', 'Green Town Lahore', 'Green Town Lhr', '', '', '', '', '', 3),
('Jhkijhkjhk', 'Jh', NULL, 'h', 'kj', 'jh', 'kjh', 'kj', 'k', 'jh', 'kjh', 'kj', 4),
('John', 'River', 1, '121-211-1212', 'irfan_lifeu@hotmail.com', '74 2 B 2 Township Lahore', '', 'sdadsa', 'Alabama', '54000', 'Pakisatn', 'Sample Comments', 5),
('As', 'Asd', 1, 'as', 'as', 'sas', '', '', '', '', '', '', 6),
('Irfan', 'Ullah', 1, '03056120188', 'irfan_lifeu@hotmail.com', 'adsda', '', 'sdadsa', 'Alabama', '54000', 'Pakistan', 'Comments', 7),
('David', 'Powers', 1, '03056120188', 'iux@example.com', 'Address 1', '', 'Narowal', 'Punjab', '54000', 'Pakistan', 'Comments', 8),
('John', 'Peck', 1, '03098781271', 'john@example.com', '32 Street', '', '', '', '', '', '', 9),
('Iux', 'Example', 1, 'asasas', 'irfan_lifeu@hotmail.com', 'adsda', '', 'sdadsa', 'Alabama', '54000', '', '', 10),
('Irfan', 'Ullah', 1, '03056120188', 'larryfrancisco@comcast.net', '', '', '', '', '', '', '', 11),
('Ahsan', 'Ali', 1, '', '', '', '', '', '', '', '', '', 12),
('Bhola', 'Sweets', 1, '', '', '', '', '', '', '', '', '', 15),
('Prepeans', '', NULL, '', '', '', '', '', '', '', '', '', 16);

-- --------------------------------------------------------

--
-- Table structure for table `ospos_permissions`
--

CREATE TABLE `ospos_permissions` (
  `permission_id` varchar(255) NOT NULL,
  `module_id` varchar(255) NOT NULL,
  `location_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ospos_permissions`
--

INSERT INTO `ospos_permissions` (`permission_id`, `module_id`, `location_id`) VALUES
('config', 'config', NULL),
('customers', 'customers', NULL),
('debcreds', 'debcreds', NULL),
('employees', 'employees', NULL),
('expenses', 'expenses', NULL),
('expenses_categories', 'expenses_categories', NULL),
('giftcards', 'giftcards', NULL),
('home', 'home', NULL),
('items', 'items', NULL),
('items_stock', 'items', 1),
('item_kits', 'item_kits', NULL),
('messages', 'messages', NULL),
('office', 'office', NULL),
('receivings', 'receivings', NULL),
('receivings_stock', 'receivings', 1),
('reports', 'reports', NULL),
('reports_categories', 'reports', NULL),
('reports_customers', 'reports', NULL),
('reports_discounts', 'reports', NULL),
('reports_employees', 'reports', NULL),
('reports_expenses_categories', 'reports', NULL),
('reports_inventory', 'reports', NULL),
('reports_items', 'reports', NULL),
('reports_payments', 'reports', NULL),
('reports_receivings', 'reports', NULL),
('reports_sales', 'reports', NULL),
('reports_suppliers', 'reports', NULL),
('reports_taxes', 'reports', NULL),
('sales', 'sales', NULL),
('sales_delete', 'sales', NULL),
('sales_stock', 'sales', 1),
('suppliers', 'suppliers', NULL),
('taxes', 'taxes', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ospos_receivings`
--

CREATE TABLE `ospos_receivings` (
  `receiving_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `supplier_id` int(10) DEFAULT NULL,
  `employee_id` int(10) NOT NULL DEFAULT '0',
  `comment` text,
  `receiving_id` int(10) NOT NULL,
  `payment_type` varchar(20) DEFAULT NULL,
  `reference` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ospos_receivings`
--

INSERT INTO `ospos_receivings` (`receiving_time`, `supplier_id`, `employee_id`, `comment`, `receiving_id`, `payment_type`, `reference`) VALUES
('2018-08-08 07:44:35', 5, 1, '', 1, 'Cash', NULL),
('2018-08-08 07:59:10', NULL, 1, '', 2, 'Cash', NULL),
('2019-01-01 07:19:28', NULL, 7, '', 3, 'Cash', 'irfan');

-- --------------------------------------------------------

--
-- Table structure for table `ospos_receivings_items`
--

CREATE TABLE `ospos_receivings_items` (
  `receiving_id` int(10) NOT NULL DEFAULT '0',
  `item_id` int(10) NOT NULL DEFAULT '0',
  `description` varchar(30) DEFAULT NULL,
  `serialnumber` varchar(30) DEFAULT NULL,
  `line` int(3) NOT NULL,
  `quantity_purchased` decimal(15,3) NOT NULL DEFAULT '0.000',
  `item_cost_price` decimal(15,2) NOT NULL,
  `item_unit_price` decimal(15,2) NOT NULL,
  `discount_percent` decimal(15,2) NOT NULL DEFAULT '0.00',
  `item_location` int(11) NOT NULL,
  `receiving_quantity` decimal(15,3) NOT NULL DEFAULT '1.000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ospos_receivings_items`
--

INSERT INTO `ospos_receivings_items` (`receiving_id`, `item_id`, `description`, `serialnumber`, `line`, `quantity_purchased`, `item_cost_price`, `item_unit_price`, `discount_percent`, `item_location`, `receiving_quantity`) VALUES
(1, 4, 'Description Sales', NULL, 1, '29.000', '32.00', '32.00', '0.00', 1, '1.000'),
(1, 5, '', NULL, 2, '32.000', '70.00', '70.00', '0.00', 1, '1.000'),
(2, 4, 'Description Sales', NULL, 1, '-10.000', '32.00', '32.00', '0.00', 1, '1.000'),
(3, 25, '', NULL, 1, '20.000', '88.00', '88.00', '0.00', 1, '24.000');

-- --------------------------------------------------------

--
-- Table structure for table `ospos_sales`
--

CREATE TABLE `ospos_sales` (
  `sale_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `customer_id` int(10) DEFAULT NULL,
  `employee_id` int(10) NOT NULL DEFAULT '0',
  `comment` text,
  `invoice_number` varchar(32) DEFAULT NULL,
  `quote_number` varchar(32) DEFAULT NULL,
  `sale_id` int(10) NOT NULL,
  `sale_status` tinyint(2) NOT NULL DEFAULT '0',
  `dinner_table_id` int(11) DEFAULT NULL,
  `work_order_number` varchar(32) DEFAULT NULL,
  `sale_type` tinyint(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ospos_sales`
--

INSERT INTO `ospos_sales` (`sale_time`, `customer_id`, `employee_id`, `comment`, `invoice_number`, `quote_number`, `sale_id`, `sale_status`, `dinner_table_id`, `work_order_number`, `sale_type`) VALUES
('2018-07-20 08:36:06', 6, 1, 'asas', '0', NULL, 1, 0, NULL, NULL, 1),
('2018-08-08 06:40:32', NULL, 1, '', NULL, NULL, 2, 0, NULL, NULL, 0),
('2018-08-08 07:49:24', NULL, 1, '', NULL, NULL, 3, 1, NULL, NULL, 1),
('2018-08-08 07:54:28', 2, 1, '', '1', NULL, 4, 0, NULL, NULL, 1),
('2018-08-08 07:56:00', 6, 1, '', '2', NULL, 5, 0, NULL, NULL, 1),
('2018-08-08 08:06:59', 2, 7, '', NULL, NULL, 6, 1, NULL, NULL, 0),
('2018-08-08 08:08:28', 6, 7, '', '3', NULL, 7, 0, NULL, NULL, 1),
('2018-08-08 08:12:34', 2, 1, '', '4', NULL, 8, 0, NULL, NULL, 1),
('2018-08-08 08:20:03', 6, 7, '', '5', NULL, 9, 0, NULL, NULL, 1),
('2018-08-09 09:20:54', 10, 1, '', NULL, NULL, 10, 0, NULL, NULL, 0),
('2018-08-09 09:21:47', 2, 1, '', NULL, NULL, 11, 0, NULL, NULL, 0),
('2018-08-09 14:18:46', NULL, 1, '', NULL, NULL, 12, 0, NULL, NULL, 0),
('2018-08-10 15:01:46', NULL, 1, '', NULL, NULL, 15, 0, NULL, NULL, 0),
('2018-12-18 11:06:45', NULL, 1, '', NULL, NULL, 16, 0, NULL, NULL, 0),
('2018-12-19 13:01:37', NULL, 1, '', NULL, NULL, 17, 0, NULL, NULL, 0),
('2018-12-20 06:25:27', NULL, 7, '', NULL, NULL, 18, 0, NULL, NULL, 0),
('2018-12-20 09:50:07', NULL, 7, '', NULL, NULL, 19, 0, NULL, NULL, 0),
('2018-12-20 11:17:15', NULL, 7, '', NULL, NULL, 20, 0, NULL, NULL, 0),
('2018-12-20 11:18:48', NULL, 7, '', NULL, NULL, 21, 0, NULL, NULL, 0),
('2018-12-21 10:14:10', 2, 7, '', NULL, NULL, 22, 0, NULL, NULL, 0),
('2018-12-21 10:49:20', 2, 7, '', NULL, NULL, 23, 0, NULL, NULL, 0),
('2018-12-21 10:51:46', 2, 7, '', NULL, NULL, 24, 0, NULL, NULL, 0),
('2018-12-21 13:55:47', NULL, 7, '', NULL, NULL, 25, 0, NULL, NULL, 0),
('2018-12-23 06:39:14', NULL, 7, '', NULL, NULL, 26, 1, NULL, NULL, 0),
('2019-01-01 06:58:49', NULL, 7, '', NULL, NULL, 27, 0, NULL, NULL, 0),
('2019-01-01 07:06:52', NULL, 7, '', NULL, NULL, 28, 0, NULL, NULL, 0),
('2019-01-01 09:36:44', NULL, 7, '', NULL, NULL, 29, 0, NULL, NULL, 0),
('2019-01-01 09:38:45', NULL, 7, '', NULL, NULL, 30, 0, NULL, NULL, 0),
('2019-01-01 12:46:12', NULL, 7, '', '6', NULL, 31, 0, NULL, NULL, 1),
('2019-01-01 12:47:33', NULL, 7, '', '7', NULL, 32, 0, NULL, NULL, 1),
('2019-01-01 12:49:33', NULL, 7, '', '8', NULL, 33, 0, NULL, NULL, 1),
('2019-01-01 12:53:35', 2, 7, '', '9', NULL, 34, 0, NULL, NULL, 1),
('2019-01-01 12:54:52', 2, 7, '', '10', NULL, 35, 0, NULL, NULL, 1),
('2019-01-01 12:58:12', NULL, 7, '', ' 000000000', NULL, 36, 0, NULL, NULL, 1),
('2019-01-02 06:18:14', NULL, 7, '', '12', NULL, 37, 0, NULL, NULL, 1),
('2019-01-02 13:48:09', 2, 7, '', '13', NULL, 38, 0, NULL, NULL, 1),
('2019-01-02 13:48:58', NULL, 7, '', NULL, NULL, 39, 0, NULL, NULL, 0),
('2019-01-02 13:49:36', NULL, 7, '', '14', NULL, 40, 0, NULL, NULL, 1),
('2019-01-08 07:28:06', 6, 7, '', '15', NULL, 41, 0, NULL, NULL, 1),
('2019-01-08 07:31:43', 2, 7, '', '16', NULL, 42, 0, NULL, NULL, 1),
('2019-01-11 05:31:03', NULL, 7, '', '17', NULL, 43, 0, NULL, NULL, 1),
('2019-01-11 07:08:32', 2, 7, '', NULL, NULL, 44, 0, NULL, NULL, 0),
('2019-01-11 07:19:19', 2, 7, '', NULL, NULL, 45, 0, NULL, NULL, 0),
('2019-01-11 08:05:29', NULL, 7, '', NULL, NULL, 46, 0, NULL, NULL, 0),
('2019-01-11 08:19:55', NULL, 7, '', NULL, NULL, 47, 0, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ospos_sales_items`
--

CREATE TABLE `ospos_sales_items` (
  `sale_id` int(10) NOT NULL DEFAULT '0',
  `item_id` int(10) NOT NULL DEFAULT '0',
  `description` varchar(255) DEFAULT NULL,
  `serialnumber` varchar(30) DEFAULT NULL,
  `line` int(3) NOT NULL DEFAULT '0',
  `quantity_purchased` decimal(15,3) NOT NULL DEFAULT '0.000',
  `item_cost_price` decimal(15,2) NOT NULL,
  `item_unit_price` decimal(15,2) NOT NULL,
  `discount_percent` decimal(15,2) NOT NULL DEFAULT '0.00',
  `item_location` int(11) NOT NULL,
  `print_option` tinyint(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ospos_sales_items`
--

INSERT INTO `ospos_sales_items` (`sale_id`, `item_id`, `description`, `serialnumber`, `line`, `quantity_purchased`, `item_cost_price`, `item_unit_price`, `discount_percent`, `item_location`, `print_option`) VALUES
(1, 3, '', '', 1, '1.000', '12.00', '121.00', '0.00', 1, 0),
(2, 1, '', '', 1, '1.000', '10.00', '11.00', '0.00', 1, 0),
(3, 4, 'Description Sales', '', 1, '4.000', '32.00', '40.00', '0.00', 1, 0),
(4, 4, 'Description Sales', '', 1, '4.000', '32.00', '40.00', '0.00', 1, 0),
(5, 5, '', '', 1, '1.000', '70.00', '80.00', '0.00', 1, 0),
(6, 4, 'Description Sales', '', 1, '10.000', '32.00', '40.00', '0.00', 1, 0),
(7, 4, 'Description Sales', '', 1, '1.000', '32.00', '40.00', '0.00', 1, 0),
(8, 4, 'Description Sales', '', 1, '4.000', '32.00', '40.00', '3.00', 1, 0),
(9, 4, 'Description Sales', '', 1, '1.000', '32.00', '40.00', '0.00', 1, 0),
(10, 6, '', '', 1, '1.000', '32.00', '44.00', '12.00', 1, 0),
(11, 4, 'Description Sales', '', 1, '1.000', '32.00', '40.00', '0.00', 1, 0),
(12, 4, 'Description Sales', '', 1, '21.000', '32.00', '40.00', '0.00', 1, 0),
(15, 7, '', '', 1, '1.000', '15000.00', '20000.00', '0.00', 1, 0),
(16, 5, '', '', 1, '1.000', '70.00', '80.00', '0.00', 1, 0),
(17, 13, '', '', 1, '1.000', '21.00', '12.00', '0.00', 1, 0),
(18, 16, '', '', 1, '1.000', '0.00', '0.00', '0.00', 1, 0),
(19, 16, '', '', 1, '1.000', '44.00', '32.00', '2.00', 1, 0),
(20, 16, '', '', 1, '10.000', '44.00', '32.00', '0.00', 1, 0),
(21, 16, '', '', 1, '4.000', '44.00', '32.00', '0.00', 1, 0),
(22, 16, '', '', 1, '1.000', '44.00', '32.00', '2.00', 1, 0),
(23, 16, '', '', 1, '1.000', '44.00', '32.00', '7.00', 1, 0),
(24, 12, 'Sampel User', '', 1, '1.000', '50.00', '60.00', '10.00', 1, 0),
(24, 14, '', '', 3, '1.000', '90.00', '100.00', '5.00', 1, 0),
(24, 16, '', '', 2, '1.000', '44.00', '32.00', '8.00', 1, 0),
(25, 16, '', '', 1, '1.000', '44.00', '32.00', '2.00', 1, 0),
(26, 14, '', '', 1, '1.000', '90.00', '100.00', '2.00', 1, 0),
(27, 25, '', '', 1, '20.000', '88.00', '90.00', '1.00', 1, 0),
(28, 25, '', '', 1, '15.000', '88.00', '90.00', '0.00', 1, 0),
(29, 25, '', '', 1, '2.000', '88.00', '90.00', '0.00', 1, 0),
(30, 16, '', '', 1, '1.000', '44.00', '32.00', '0.00', 1, 0),
(31, 16, '', '', 1, '1.000', '44.00', '32.00', '0.00', 1, 0),
(32, 25, '', '', 1, '1.000', '88.00', '90.00', '0.00', 1, 0),
(33, 25, '', '', 1, '1.000', '88.00', '90.00', '0.00', 1, 0),
(34, 25, '', '', 1, '1.000', '88.00', '90.00', '5.00', 1, 0),
(35, 25, '', '', 1, '1.000', '88.00', '90.00', '5.00', 1, 0),
(36, 25, '', '', 1, '1.000', '88.00', '90.00', '0.00', 1, 0),
(37, 14, '', '', 1, '1.000', '90.00', '100.00', '0.00', 1, 0),
(38, 14, '', '', 1, '1.000', '90.00', '100.00', '5.00', 1, 0),
(39, 25, '', '', 1, '1.000', '88.00', '90.00', '0.00', 1, 0),
(40, 25, '', '', 1, '1.000', '88.00', '90.00', '0.00', 1, 0),
(41, 25, '', '', 1, '1.000', '88.00', '90.00', '0.00', 1, 0),
(42, 14, '', '', 1, '1.000', '90.00', '100.00', '5.00', 1, 0),
(43, 16, '', '', 1, '1.000', '44.00', '32.00', '0.00', 1, 0),
(44, 16, '', '', 1, '1.000', '44.00', '32.00', '0.00', 1, 0),
(45, 16, '', '', 1, '2.000', '44.00', '32.00', '0.00', 1, 0),
(46, 16, '', '', 1, '1.000', '44.00', '32.00', '0.00', 1, 0),
(47, 16, '', '', 1, '1.000', '44.00', '30.00', '0.00', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ospos_sales_items_taxes`
--

CREATE TABLE `ospos_sales_items_taxes` (
  `sale_id` int(10) NOT NULL,
  `item_id` int(10) NOT NULL,
  `line` int(3) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `percent` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `tax_type` tinyint(2) NOT NULL DEFAULT '0',
  `rounding_code` tinyint(2) NOT NULL DEFAULT '0',
  `cascade_tax` tinyint(2) NOT NULL DEFAULT '0',
  `cascade_sequence` tinyint(2) NOT NULL DEFAULT '0',
  `item_tax_amount` decimal(15,4) NOT NULL DEFAULT '0.0000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ospos_sales_items_taxes`
--

INSERT INTO `ospos_sales_items_taxes` (`sale_id`, `item_id`, `line`, `name`, `percent`, `tax_type`, `rounding_code`, `cascade_tax`, `cascade_sequence`, `item_tax_amount`) VALUES
(3, 4, 1, 'Incom', '10.0000', 1, 1, 0, 0, '16.0000'),
(3, 4, 1, 'Sales', '5.0000', 1, 1, 0, 0, '8.0000'),
(4, 4, 1, 'Incom', '10.0000', 1, 1, 0, 0, '16.0000'),
(4, 4, 1, 'Sales', '5.0000', 1, 1, 0, 0, '8.0000'),
(5, 5, 1, 'Incom', '11.0000', 1, 1, 0, 0, '8.8000'),
(5, 5, 1, 'sales', '5.0000', 1, 1, 0, 0, '4.0000'),
(6, 4, 1, 'Incom', '10.0000', 1, 1, 0, 0, '40.0000'),
(6, 4, 1, 'Sales', '5.0000', 1, 1, 0, 0, '20.0000'),
(7, 4, 1, 'Incom', '10.0000', 1, 1, 0, 0, '4.0000'),
(7, 4, 1, 'Sales', '5.0000', 1, 1, 0, 0, '2.0000'),
(8, 4, 1, 'Incom', '10.0000', 1, 1, 0, 0, '15.5200'),
(8, 4, 1, 'Sales', '5.0000', 1, 1, 0, 0, '7.7600'),
(9, 4, 1, 'Incom', '10.0000', 1, 1, 0, 0, '4.0000'),
(9, 4, 1, 'Sales', '5.0000', 1, 1, 0, 0, '2.0000'),
(11, 4, 1, 'Incom', '10.0000', 1, 1, 0, 0, '4.0000'),
(11, 4, 1, 'Sales', '5.0000', 1, 1, 0, 0, '2.0000'),
(12, 4, 1, 'Incom', '10.0000', 1, 1, 0, 0, '84.0000'),
(12, 4, 1, 'Sales', '5.0000', 1, 1, 0, 0, '42.0000'),
(16, 5, 1, 'Incom', '11.0000', 1, 1, 0, 0, '8.8000'),
(16, 5, 1, 'sales', '5.0000', 1, 1, 0, 0, '4.0000');

-- --------------------------------------------------------

--
-- Table structure for table `ospos_sales_payments`
--

CREATE TABLE `ospos_sales_payments` (
  `sale_id` int(10) NOT NULL,
  `payment_type` varchar(40) NOT NULL,
  `payment_amount` decimal(15,2) NOT NULL,
  `amount_due` float NOT NULL,
  `discount` float NOT NULL,
  `total_payment` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ospos_sales_payments`
--

INSERT INTO `ospos_sales_payments` (`sale_id`, `payment_type`, `payment_amount`, `amount_due`, `discount`, `total_payment`) VALUES
(43, 'Cash', '32.00', 0, 0, 32),
(44, 'Cash', '0.00', 30, 2, 32),
(45, 'Cash', '100.00', -32, 4, 64),
(46, 'Cash', '30.00', 0, 2, 32),
(47, 'Cash', '50000.00', -49970, 0, 30);

-- --------------------------------------------------------

--
-- Table structure for table `ospos_sales_reward_points`
--

CREATE TABLE `ospos_sales_reward_points` (
  `id` int(11) NOT NULL,
  `sale_id` int(11) NOT NULL,
  `earned` float NOT NULL,
  `used` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ospos_sales_taxes`
--

CREATE TABLE `ospos_sales_taxes` (
  `sale_id` int(10) NOT NULL,
  `tax_type` smallint(2) NOT NULL,
  `tax_group` varchar(32) NOT NULL,
  `sale_tax_basis` decimal(15,4) NOT NULL,
  `sale_tax_amount` decimal(15,4) NOT NULL,
  `print_sequence` tinyint(2) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `tax_rate` decimal(15,4) NOT NULL,
  `sales_tax_code` varchar(32) NOT NULL DEFAULT '',
  `rounding_code` tinyint(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ospos_sales_taxes`
--

INSERT INTO `ospos_sales_taxes` (`sale_id`, `tax_type`, `tax_group`, `sale_tax_basis`, `sale_tax_amount`, `print_sequence`, `name`, `tax_rate`, `sales_tax_code`, `rounding_code`) VALUES
(3, 1, '10% Incom', '160.0000', '16.0000', 0, 'Incom', '10.0000', '', 1),
(3, 1, '5% Sales', '160.0000', '8.0000', 1, 'Sales', '5.0000', '', 1),
(4, 1, '10% Incom', '160.0000', '16.0000', 0, 'Incom', '10.0000', '', 1),
(4, 1, '5% Sales', '160.0000', '8.0000', 1, 'Sales', '5.0000', '', 1),
(5, 1, '11% Incom', '80.0000', '8.8000', 0, 'Incom', '11.0000', '', 1),
(5, 1, '5% sales', '80.0000', '4.0000', 1, 'sales', '5.0000', '', 1),
(6, 1, '10% Incom', '400.0000', '40.0000', 0, 'Incom', '10.0000', '', 1),
(6, 1, '5% Sales', '400.0000', '20.0000', 1, 'Sales', '5.0000', '', 1),
(7, 1, '10% Incom', '40.0000', '4.0000', 0, 'Incom', '10.0000', '', 1),
(7, 1, '5% Sales', '40.0000', '2.0000', 1, 'Sales', '5.0000', '', 1),
(8, 1, '10% Incom', '155.2000', '15.5200', 0, 'Incom', '10.0000', '', 1),
(8, 1, '5% Sales', '155.2000', '7.7600', 1, 'Sales', '5.0000', '', 1),
(9, 1, '10% Incom', '40.0000', '4.0000', 0, 'Incom', '10.0000', '', 1),
(9, 1, '5% Sales', '40.0000', '2.0000', 1, 'Sales', '5.0000', '', 1),
(11, 1, '10% Incom', '40.0000', '4.0000', 0, 'Incom', '10.0000', '', 1),
(11, 1, '5% Sales', '40.0000', '2.0000', 1, 'Sales', '5.0000', '', 1),
(12, 1, '10% Incom', '840.0000', '84.0000', 0, 'Incom', '10.0000', '', 1),
(12, 1, '5% Sales', '840.0000', '42.0000', 1, 'Sales', '5.0000', '', 1),
(16, 1, '11% Incom', '80.0000', '8.8000', 0, 'Incom', '11.0000', '', 1),
(16, 1, '5% sales', '80.0000', '4.0000', 1, 'sales', '5.0000', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ospos_sessions`
--

CREATE TABLE `ospos_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ospos_sessions`
--

INSERT INTO `ospos_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('1cq3kndtv3un0k3bme5hn3t94bfcqr5c', '::1', 1546672487, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363636393435313b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b),
('ovhu9r4456l5j7cjcpifn1vvpakv22le', '::1', 1546673050, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363637323438343b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b),
('99fuj9t37rk4t2uiubo5v2vftnnr6kut', '::1', 1546673406, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363637333034373b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b),
('t4jdcv02s4cr06g015apc4u71bo59oo1', '::1', 1546673434, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363637333430343b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b),
('88scd9o2k95iktm6kfgf4sd54tc5ffta', '::1', 1546674389, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363637343233393b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b),
('5242qdionfnkf670hh2p02k1jg5k30ie', '::1', 1546675976, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363637343535373b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b),
('us3d134d38o2ifafpb7dmv9hqaaul77l', '::1', 1546684918, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363637373335363b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b),
('2230n8tev87gq2ef59g04te62inpg96r', '::1', 1546847133, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363834373130333b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b),
('6k9vu3fqo8b5inkl8tuidgu4m69hc3rr', '::1', 1546856077, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363835363037373b),
('lqgp99eqhjkeci42gk19j9b5h0monu3m', '::1', 1546856077, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363835363037373b),
('pnnu2mr1na3nerp76llsq94evsnv7cb8', '::1', 1546858865, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363835383836353b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('6crgbhhv53haj1cq8tpref9tbt291i3a', '::1', 1546858912, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363835383836353b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('9ku8u3n1rh7lf12d2dv1vto4m2t4n6no', '::1', 1546877229, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363837373035393b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('qna0aehuga614a3jqei4pcp5vvp4sks0', '::1', 1546924826, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363932333539313b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b),
('0jutvtje1t7snk6ou8nd1rt4glijg59e', '::1', 1546929310, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363932363330343b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('pm0oejn7vkgv0g7d0pjjr1c7u856u1hj', '127.0.0.1', 1546929012, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363932393031323b),
('q350fc30a1s0hc37900jn0df5vghnkia', '::1', 1546931447, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363933313432363b706572736f6e5f69647c733a313a2237223b6d656e755f67726f75707c733a343a22686f6d65223b73616c655f69647c693a2d313b73616c65735f636172747c613a313a7b693a313b613a32313a7b733a373a226974656d5f6964223b733a323a223235223b733a31333a226974656d5f6c6f636174696f6e223b733a313a2231223b733a31303a2273746f636b5f6e616d65223b733a353a2273746f636b223b733a343a226c696e65223b693a313b733a343a226e616d65223b733a363a22736f6f706572223b733a31313a226974656d5f6e756d626572223b733a31333a2238393634303030363831313037223b733a31313a226465736372697074696f6e223b733a303a22223b733a31323a2273657269616c6e756d626572223b733a303a22223b733a32313a22616c6c6f775f616c745f6465736372697074696f6e223b733a313a2230223b733a31333a2269735f73657269616c697a6564223b733a313a2230223b733a383a227175616e74697479223b693a313b733a383a22646973636f756e74223b733a313a2230223b733a383a22696e5f73746f636b223b733a373a223439362e303030223b733a353a227072696365223b733a353a2239302e3030223b733a31303a22636f73745f7072696365223b733a353a2238382e3030223b733a353a22746f74616c223b733a353a2239302e3030223b733a31363a22646973636f756e7465645f746f74616c223b733a373a2239302e30303030223b733a31323a227072696e745f6f7074696f6e223b693a303b733a31303a2273746f636b5f74797065223b733a313a2230223b733a393a226974656d5f74797065223b733a313a2230223b733a31353a227461785f63617465676f72795f6964223b733a313a2230223b7d7d73616c65735f637573746f6d65727c733a313a2236223b73616c65735f6d6f64657c733a343a2273616c65223b73616c65735f6c6f636174696f6e7c733a313a2231223b73616c65735f7061796d656e74737c613a303a7b7d636173685f6d6f64657c693a303b636173685f726f756e64696e677c693a303b73616c65735f696e766f6963655f6e756d6265727c4e3b),
('rjoa5sjgeipmv9ih0n1gakq8h7l6b4sa', '::1', 1546931811, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363933313831303b706572736f6e5f69647c733a313a2237223b6d656e755f67726f75707c733a343a22686f6d65223b73616c655f69647c693a2d313b73616c65735f636172747c613a313a7b693a313b613a32313a7b733a373a226974656d5f6964223b733a323a223235223b733a31333a226974656d5f6c6f636174696f6e223b733a313a2231223b733a31303a2273746f636b5f6e616d65223b733a353a2273746f636b223b733a343a226c696e65223b693a313b733a343a226e616d65223b733a363a22736f6f706572223b733a31313a226974656d5f6e756d626572223b733a31333a2238393634303030363831313037223b733a31313a226465736372697074696f6e223b733a303a22223b733a31323a2273657269616c6e756d626572223b733a303a22223b733a32313a22616c6c6f775f616c745f6465736372697074696f6e223b733a313a2230223b733a31333a2269735f73657269616c697a6564223b733a313a2230223b733a383a227175616e74697479223b693a313b733a383a22646973636f756e74223b733a313a2230223b733a383a22696e5f73746f636b223b733a373a223439362e303030223b733a353a227072696365223b733a353a2239302e3030223b733a31303a22636f73745f7072696365223b733a353a2238382e3030223b733a353a22746f74616c223b733a353a2239302e3030223b733a31363a22646973636f756e7465645f746f74616c223b733a373a2239302e30303030223b733a31323a227072696e745f6f7074696f6e223b693a303b733a31303a2273746f636b5f74797065223b733a313a2230223b733a393a226974656d5f74797065223b733a313a2230223b733a31353a227461785f63617465676f72795f6964223b733a313a2230223b7d7d73616c65735f637573746f6d65727c733a313a2236223b73616c65735f6d6f64657c733a343a2273616c65223b73616c65735f6c6f636174696f6e7c733a313a2231223b73616c65735f7061796d656e74737c613a313a7b733a343a2243617368223b613a323a7b733a31323a227061796d656e745f74797065223b733a343a2243617368223b733a31343a227061796d656e745f616d6f756e74223b643a38303b7d7d636173685f6d6f64657c693a303b636173685f726f756e64696e677c693a303b73616c65735f696e766f6963655f6e756d6265727c4e3b),
('b8joji02bnr4jb368n5vuogh0pbs4n62', '::1', 1546932486, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363933323234373b706572736f6e5f69647c733a313a2237223b6d656e755f67726f75707c733a343a22686f6d65223b73616c655f69647c693a2d313b73616c65735f6d6f64657c733a31323a2273616c655f696e766f696365223b73616c65735f6c6f636174696f6e7c733a313a2231223b73616c65735f696e766f6963655f6e756d6265725f656e61626c65647c623a303b),
('up0jqv8sstcm46oilohhtcj9eari9dbi', '::1', 1546934207, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363933323636323b706572736f6e5f69647c733a313a2237223b6d656e755f67726f75707c733a343a22686f6d65223b73616c655f69647c693a2d313b73616c65735f6d6f64657c733a31323a2273616c655f696e766f696365223b73616c65735f6c6f636174696f6e7c733a313a2231223b73616c65735f696e766f6963655f6e756d6265725f656e61626c65647c623a303b73616c65735f636172747c613a303a7b7d73616c65735f637573746f6d65727c693a2d313b73616c65735f7061796d656e74737c613a303a7b7d636173685f6d6f64657c693a303b636173685f726f756e64696e677c693a303b73616c65735f696e766f6963655f6e756d6265727c4e3b),
('lspkrrjtpddsj8auk4u11l2dppmms8h4', '::1', 1546939462, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363933343432383b706572736f6e5f69647c733a313a2237223b6d656e755f67726f75707c733a343a22686f6d65223b73616c655f69647c693a2d313b73616c65735f6d6f64657c733a31323a2273616c655f696e766f696365223b73616c65735f6c6f636174696f6e7c733a313a2231223b73616c65735f696e766f6963655f6e756d6265725f656e61626c65647c623a303b73616c65735f636172747c613a313a7b693a313b613a32313a7b733a373a226974656d5f6964223b733a323a223235223b733a31333a226974656d5f6c6f636174696f6e223b733a313a2231223b733a31303a2273746f636b5f6e616d65223b733a353a2273746f636b223b733a343a226c696e65223b693a313b733a343a226e616d65223b733a363a22736f6f706572223b733a31313a226974656d5f6e756d626572223b733a31333a2238393634303030363831313037223b733a31313a226465736372697074696f6e223b733a303a22223b733a31323a2273657269616c6e756d626572223b733a303a22223b733a32313a22616c6c6f775f616c745f6465736372697074696f6e223b733a313a2230223b733a31333a2269735f73657269616c697a6564223b733a313a2230223b733a383a227175616e74697479223b693a313b733a383a22646973636f756e74223b733a313a2230223b733a383a22696e5f73746f636b223b733a373a223439352e303030223b733a353a227072696365223b733a353a2239302e3030223b733a31303a22636f73745f7072696365223b733a353a2238382e3030223b733a353a22746f74616c223b733a353a2239302e3030223b733a31363a22646973636f756e7465645f746f74616c223b733a373a2239302e30303030223b733a31323a227072696e745f6f7074696f6e223b693a303b733a31303a2273746f636b5f74797065223b733a313a2230223b733a393a226974656d5f74797065223b733a313a2230223b733a31353a227461785f63617465676f72795f6964223b733a313a2230223b7d7d73616c65735f637573746f6d65727c693a2d313b73616c65735f7061796d656e74737c613a303a7b7d636173685f6d6f64657c693a303b636173685f726f756e64696e677c693a303b73616c65735f696e766f6963655f6e756d6265727c4e3b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('43ileke89r7blior410h1i4hc7accutc', '::1', 1546940202, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363933393535383b706572736f6e5f69647c733a313a2237223b6d656e755f67726f75707c733a343a22686f6d65223b73616c655f69647c693a2d313b73616c65735f6d6f64657c733a31323a2273616c655f696e766f696365223b73616c65735f6c6f636174696f6e7c733a313a2231223b73616c65735f696e766f6963655f6e756d6265725f656e61626c65647c623a303b73616c65735f636172747c613a313a7b693a313b613a32313a7b733a373a226974656d5f6964223b733a323a223235223b733a31333a226974656d5f6c6f636174696f6e223b733a313a2231223b733a31303a2273746f636b5f6e616d65223b733a353a2273746f636b223b733a343a226c696e65223b693a313b733a343a226e616d65223b733a363a22736f6f706572223b733a31313a226974656d5f6e756d626572223b733a31333a2238393634303030363831313037223b733a31313a226465736372697074696f6e223b733a303a22223b733a31323a2273657269616c6e756d626572223b733a303a22223b733a32313a22616c6c6f775f616c745f6465736372697074696f6e223b733a313a2230223b733a31333a2269735f73657269616c697a6564223b733a313a2230223b733a383a227175616e74697479223b693a313b733a383a22646973636f756e74223b733a313a2230223b733a383a22696e5f73746f636b223b733a373a223439352e303030223b733a353a227072696365223b733a353a2239302e3030223b733a31303a22636f73745f7072696365223b733a353a2238382e3030223b733a353a22746f74616c223b733a353a2239302e3030223b733a31363a22646973636f756e7465645f746f74616c223b733a373a2239302e30303030223b733a31323a227072696e745f6f7074696f6e223b693a303b733a31303a2273746f636b5f74797065223b733a313a2230223b733a393a226974656d5f74797065223b733a313a2230223b733a31353a227461785f63617465676f72795f6964223b733a313a2230223b7d7d73616c65735f637573746f6d65727c693a2d313b73616c65735f7061796d656e74737c613a303a7b7d636173685f6d6f64657c693a303b636173685f726f756e64696e677c693a303b73616c65735f696e766f6963655f6e756d6265727c4e3b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('p91g6olshdm24kk52ujeuq51524r51id', '::1', 1546940482, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363934303230343b706572736f6e5f69647c733a313a2237223b6d656e755f67726f75707c733a343a22686f6d65223b73616c655f69647c693a2d313b73616c65735f6d6f64657c733a31323a2273616c655f696e766f696365223b73616c65735f6c6f636174696f6e7c733a313a2231223b73616c65735f696e766f6963655f6e756d6265725f656e61626c65647c623a303b73616c65735f636172747c613a313a7b693a313b613a32313a7b733a373a226974656d5f6964223b733a323a223235223b733a31333a226974656d5f6c6f636174696f6e223b733a313a2231223b733a31303a2273746f636b5f6e616d65223b733a353a2273746f636b223b733a343a226c696e65223b693a313b733a343a226e616d65223b733a363a22736f6f706572223b733a31313a226974656d5f6e756d626572223b733a31333a2238393634303030363831313037223b733a31313a226465736372697074696f6e223b733a303a22223b733a31323a2273657269616c6e756d626572223b733a303a22223b733a32313a22616c6c6f775f616c745f6465736372697074696f6e223b733a313a2230223b733a31333a2269735f73657269616c697a6564223b733a313a2230223b733a383a227175616e74697479223b693a313b733a383a22646973636f756e74223b733a313a2230223b733a383a22696e5f73746f636b223b733a373a223439352e303030223b733a353a227072696365223b733a353a2239302e3030223b733a31303a22636f73745f7072696365223b733a353a2238382e3030223b733a353a22746f74616c223b733a353a2239302e3030223b733a31363a22646973636f756e7465645f746f74616c223b733a373a2239302e30303030223b733a31323a227072696e745f6f7074696f6e223b693a303b733a31303a2273746f636b5f74797065223b733a313a2230223b733a393a226974656d5f74797065223b733a313a2230223b733a31353a227461785f63617465676f72795f6964223b733a313a2230223b7d7d73616c65735f637573746f6d65727c693a2d313b73616c65735f7061796d656e74737c613a303a7b7d636173685f6d6f64657c693a303b636173685f726f756e64696e677c693a303b73616c65735f696e766f6963655f6e756d6265727c4e3b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('30m7qartmjqgk2umc2o613bhql27ugp5', '::1', 1546940921, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363934303538343b706572736f6e5f69647c733a313a2237223b6d656e755f67726f75707c733a343a22686f6d65223b73616c655f69647c693a2d313b73616c65735f6d6f64657c733a31323a2273616c655f696e766f696365223b73616c65735f6c6f636174696f6e7c733a313a2231223b73616c65735f696e766f6963655f6e756d6265725f656e61626c65647c623a303b73616c65735f636172747c613a313a7b693a313b613a32313a7b733a373a226974656d5f6964223b733a323a223235223b733a31333a226974656d5f6c6f636174696f6e223b733a313a2231223b733a31303a2273746f636b5f6e616d65223b733a353a2273746f636b223b733a343a226c696e65223b693a313b733a343a226e616d65223b733a363a22736f6f706572223b733a31313a226974656d5f6e756d626572223b733a31333a2238393634303030363831313037223b733a31313a226465736372697074696f6e223b733a303a22223b733a31323a2273657269616c6e756d626572223b733a303a22223b733a32313a22616c6c6f775f616c745f6465736372697074696f6e223b733a313a2230223b733a31333a2269735f73657269616c697a6564223b733a313a2230223b733a383a227175616e74697479223b693a313b733a383a22646973636f756e74223b733a313a2230223b733a383a22696e5f73746f636b223b733a373a223439352e303030223b733a353a227072696365223b733a353a2239302e3030223b733a31303a22636f73745f7072696365223b733a353a2238382e3030223b733a353a22746f74616c223b733a353a2239302e3030223b733a31363a22646973636f756e7465645f746f74616c223b733a373a2239302e30303030223b733a31323a227072696e745f6f7074696f6e223b693a303b733a31303a2273746f636b5f74797065223b733a313a2230223b733a393a226974656d5f74797065223b733a313a2230223b733a31353a227461785f63617465676f72795f6964223b733a313a2230223b7d7d73616c65735f637573746f6d65727c693a2d313b73616c65735f7061796d656e74737c613a303a7b7d636173685f6d6f64657c693a303b636173685f726f756e64696e677c693a303b73616c65735f696e766f6963655f6e756d6265727c4e3b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('2n9nvlsi72k4kbip0020sp9m1mr3v9p3', '::1', 1546941258, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363934313133363b706572736f6e5f69647c733a313a2237223b6d656e755f67726f75707c733a343a22686f6d65223b73616c655f69647c693a2d313b73616c65735f6d6f64657c733a31323a2273616c655f696e766f696365223b73616c65735f6c6f636174696f6e7c733a313a2231223b73616c65735f696e766f6963655f6e756d6265725f656e61626c65647c623a303b73616c65735f636172747c613a313a7b693a313b613a32313a7b733a373a226974656d5f6964223b733a323a223235223b733a31333a226974656d5f6c6f636174696f6e223b733a313a2231223b733a31303a2273746f636b5f6e616d65223b733a353a2273746f636b223b733a343a226c696e65223b693a313b733a343a226e616d65223b733a363a22736f6f706572223b733a31313a226974656d5f6e756d626572223b733a31333a2238393634303030363831313037223b733a31313a226465736372697074696f6e223b733a303a22223b733a31323a2273657269616c6e756d626572223b733a303a22223b733a32313a22616c6c6f775f616c745f6465736372697074696f6e223b733a313a2230223b733a31333a2269735f73657269616c697a6564223b733a313a2230223b733a383a227175616e74697479223b693a313b733a383a22646973636f756e74223b733a313a2230223b733a383a22696e5f73746f636b223b733a373a223439352e303030223b733a353a227072696365223b733a353a2239302e3030223b733a31303a22636f73745f7072696365223b733a353a2238382e3030223b733a353a22746f74616c223b733a353a2239302e3030223b733a31363a22646973636f756e7465645f746f74616c223b733a373a2239302e30303030223b733a31323a227072696e745f6f7074696f6e223b693a303b733a31303a2273746f636b5f74797065223b733a313a2230223b733a393a226974656d5f74797065223b733a313a2230223b733a31353a227461785f63617465676f72795f6964223b733a313a2230223b7d7d73616c65735f637573746f6d65727c693a2d313b73616c65735f7061796d656e74737c613a303a7b7d636173685f6d6f64657c693a303b636173685f726f756e64696e677c693a303b73616c65735f696e766f6963655f6e756d6265727c4e3b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('3un5p0o052ih7mnhr3kfllds38fm77ic', '::1', 1546941558, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363934313531393b706572736f6e5f69647c733a313a2237223b6d656e755f67726f75707c733a343a22686f6d65223b73616c655f69647c693a2d313b73616c65735f6d6f64657c733a31323a2273616c655f696e766f696365223b73616c65735f6c6f636174696f6e7c733a313a2231223b73616c65735f696e766f6963655f6e756d6265725f656e61626c65647c623a303b73616c65735f636172747c613a313a7b693a313b613a32313a7b733a373a226974656d5f6964223b733a323a223235223b733a31333a226974656d5f6c6f636174696f6e223b733a313a2231223b733a31303a2273746f636b5f6e616d65223b733a353a2273746f636b223b733a343a226c696e65223b693a313b733a343a226e616d65223b733a363a22736f6f706572223b733a31313a226974656d5f6e756d626572223b733a31333a2238393634303030363831313037223b733a31313a226465736372697074696f6e223b733a303a22223b733a31323a2273657269616c6e756d626572223b733a303a22223b733a32313a22616c6c6f775f616c745f6465736372697074696f6e223b733a313a2230223b733a31333a2269735f73657269616c697a6564223b733a313a2230223b733a383a227175616e74697479223b693a313b733a383a22646973636f756e74223b733a313a2230223b733a383a22696e5f73746f636b223b733a373a223439352e303030223b733a353a227072696365223b733a353a2239302e3030223b733a31303a22636f73745f7072696365223b733a353a2238382e3030223b733a353a22746f74616c223b733a353a2239302e3030223b733a31363a22646973636f756e7465645f746f74616c223b733a373a2239302e30303030223b733a31323a227072696e745f6f7074696f6e223b693a303b733a31303a2273746f636b5f74797065223b733a313a2230223b733a393a226974656d5f74797065223b733a313a2230223b733a31353a227461785f63617465676f72795f6964223b733a313a2230223b7d7d73616c65735f637573746f6d65727c693a2d313b73616c65735f7061796d656e74737c613a303a7b7d636173685f6d6f64657c693a303b636173685f726f756e64696e677c693a303b73616c65735f696e766f6963655f6e756d6265727c4e3b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('4kmjkcl71ek6cb2nas3qfqbclm37th9o', '::1', 1546942461, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363934323038373b706572736f6e5f69647c733a313a2237223b6d656e755f67726f75707c733a343a22686f6d65223b73616c655f69647c693a2d313b73616c65735f6d6f64657c733a31323a2273616c655f696e766f696365223b73616c65735f6c6f636174696f6e7c733a313a2231223b73616c65735f696e766f6963655f6e756d6265725f656e61626c65647c623a303b73616c65735f636172747c613a313a7b693a313b613a32313a7b733a373a226974656d5f6964223b733a323a223235223b733a31333a226974656d5f6c6f636174696f6e223b733a313a2231223b733a31303a2273746f636b5f6e616d65223b733a353a2273746f636b223b733a343a226c696e65223b693a313b733a343a226e616d65223b733a363a22736f6f706572223b733a31313a226974656d5f6e756d626572223b733a31333a2238393634303030363831313037223b733a31313a226465736372697074696f6e223b733a303a22223b733a31323a2273657269616c6e756d626572223b733a303a22223b733a32313a22616c6c6f775f616c745f6465736372697074696f6e223b733a313a2230223b733a31333a2269735f73657269616c697a6564223b733a313a2230223b733a383a227175616e74697479223b693a313b733a383a22646973636f756e74223b733a313a2230223b733a383a22696e5f73746f636b223b733a373a223439352e303030223b733a353a227072696365223b733a353a2239302e3030223b733a31303a22636f73745f7072696365223b733a353a2238382e3030223b733a353a22746f74616c223b733a353a2239302e3030223b733a31363a22646973636f756e7465645f746f74616c223b733a373a2239302e30303030223b733a31323a227072696e745f6f7074696f6e223b693a303b733a31303a2273746f636b5f74797065223b733a313a2230223b733a393a226974656d5f74797065223b733a313a2230223b733a31353a227461785f63617465676f72795f6964223b733a313a2230223b7d7d73616c65735f637573746f6d65727c693a2d313b73616c65735f7061796d656e74737c613a303a7b7d636173685f6d6f64657c693a303b636173685f726f756e64696e677c693a303b73616c65735f696e766f6963655f6e756d6265727c4e3b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('rgu43jppotjtuohjjfvk9otdoi21ot0e', '::1', 1546945950, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363934353636383b706572736f6e5f69647c733a313a2237223b6d656e755f67726f75707c733a343a22686f6d65223b73616c655f69647c693a2d313b73616c65735f6d6f64657c733a31323a2273616c655f696e766f696365223b73616c65735f6c6f636174696f6e7c733a313a2231223b73616c65735f696e766f6963655f6e756d6265725f656e61626c65647c623a303b73616c65735f636172747c613a313a7b693a313b613a32313a7b733a373a226974656d5f6964223b733a323a223235223b733a31333a226974656d5f6c6f636174696f6e223b733a313a2231223b733a31303a2273746f636b5f6e616d65223b733a353a2273746f636b223b733a343a226c696e65223b693a313b733a343a226e616d65223b733a363a22736f6f706572223b733a31313a226974656d5f6e756d626572223b733a31333a2238393634303030363831313037223b733a31313a226465736372697074696f6e223b733a303a22223b733a31323a2273657269616c6e756d626572223b733a303a22223b733a32313a22616c6c6f775f616c745f6465736372697074696f6e223b733a313a2230223b733a31333a2269735f73657269616c697a6564223b733a313a2230223b733a383a227175616e74697479223b693a313b733a383a22646973636f756e74223b733a313a2230223b733a383a22696e5f73746f636b223b733a373a223439352e303030223b733a353a227072696365223b733a353a2239302e3030223b733a31303a22636f73745f7072696365223b733a353a2238382e3030223b733a353a22746f74616c223b733a353a2239302e3030223b733a31363a22646973636f756e7465645f746f74616c223b733a373a2239302e30303030223b733a31323a227072696e745f6f7074696f6e223b693a303b733a31303a2273746f636b5f74797065223b733a313a2230223b733a393a226974656d5f74797065223b733a313a2230223b733a31353a227461785f63617465676f72795f6964223b733a313a2230223b7d7d73616c65735f637573746f6d65727c693a2d313b73616c65735f7061796d656e74737c613a303a7b7d636173685f6d6f64657c693a303b636173685f726f756e64696e677c693a303b73616c65735f696e766f6963655f6e756d6265727c4e3b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('ncjc8c72oa385au2o39ek6go9s7cgrdk', '::1', 1546947689, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534363934373638353b706572736f6e5f69647c733a313a2237223b6d656e755f67726f75707c733a343a22686f6d65223b73616c655f69647c693a2d313b73616c65735f6d6f64657c733a31323a2273616c655f696e766f696365223b73616c65735f6c6f636174696f6e7c733a313a2231223b73616c65735f696e766f6963655f6e756d6265725f656e61626c65647c623a303b73616c65735f636172747c613a313a7b693a313b613a32313a7b733a373a226974656d5f6964223b733a323a223235223b733a31333a226974656d5f6c6f636174696f6e223b733a313a2231223b733a31303a2273746f636b5f6e616d65223b733a353a2273746f636b223b733a343a226c696e65223b693a313b733a343a226e616d65223b733a363a22736f6f706572223b733a31313a226974656d5f6e756d626572223b733a31333a2238393634303030363831313037223b733a31313a226465736372697074696f6e223b733a303a22223b733a31323a2273657269616c6e756d626572223b733a303a22223b733a32313a22616c6c6f775f616c745f6465736372697074696f6e223b733a313a2230223b733a31333a2269735f73657269616c697a6564223b733a313a2230223b733a383a227175616e74697479223b693a313b733a383a22646973636f756e74223b733a313a2230223b733a383a22696e5f73746f636b223b733a373a223439352e303030223b733a353a227072696365223b733a353a2239302e3030223b733a31303a22636f73745f7072696365223b733a353a2238382e3030223b733a353a22746f74616c223b733a353a2239302e3030223b733a31363a22646973636f756e7465645f746f74616c223b733a373a2239302e30303030223b733a31323a227072696e745f6f7074696f6e223b693a303b733a31303a2273746f636b5f74797065223b733a313a2230223b733a393a226974656d5f74797065223b733a313a2230223b733a31353a227461785f63617465676f72795f6964223b733a313a2230223b7d7d73616c65735f637573746f6d65727c693a2d313b73616c65735f7061796d656e74737c613a303a7b7d636173685f6d6f64657c693a303b636173685f726f756e64696e677c693a303b73616c65735f696e766f6963655f6e756d6265727c4e3b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('4bhi62oteqjspvdm8cq3t95e81of0p4r', '::1', 1547013266, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373031333031363b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b),
('s32ccmq5dqmtkfmfhdg2lv3d9lmnks9s', '::1', 1547015558, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373031353534343b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('m0r81r76c77s291rdsnj1j235k7jqvg7', '::1', 1547016118, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373031363130343b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b),
('rcgcnrf6pheg8o4oqegns2a68rdpihbs', '::1', 1547016649, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373031363633383b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b),
('0qocgjdqtsgod0hkc9egkkcl9auak1qa', '::1', 1547017505, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373031373439383b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b),
('vp0bb78latgtb1482kcu6vt1jcfvfpqc', '::1', 1547019168, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373031373935383b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b),
('mddevjt536r0g58bv3uil97g6n1jkns4', '::1', 1547019448, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373031393334313b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b),
('tkj8e7q1i1f7cb6pkq5c2se658bl044f', '::1', 1547019986, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373031393936373b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('dnkqk2h5k0fn0hdrqjlth2p1j27pr11h', '::1', 1547020721, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373032303630323b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('k0mqt92cmqilv8k8k3jgbrjo16v1k7oo', '::1', 1547021865, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373032313635323b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('0ipaiu9dbh61uuoi6gtlu8gdkdv5ojct', '::1', 1547024220, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373032313936383b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('10k62f890ck52231ieb4pjoat57c33oe', '::1', 1547024991, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373032343438363b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('uuibvmp87jj77umectb7ktsa72hdcrl9', '::1', 1547025404, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373032353032373b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('3o044sgsdno5ur0ntlifo5gr8si3i81u', '::1', 1547025437, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373032353431303b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('4d414cgk87vbtv5or5kjuhlj1vsei7tp', '::1', 1547026469, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373032363339383b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('jfun029251ovmal07l7uubihgo31osb9', '::1', 1547027477, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373032373237323b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('e2k67ce22v3p1rg6uek3gdknr6s0j22o', '::1', 1547027695, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373032373638383b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('2s0780vt8cj6i4sjbvsu6ta3cqb066sg', '::1', 1547028178, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373032383137313b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('t5h2mqdb5js1bdk653csi5t6laah011b', '::1', 1547028678, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373032383532313b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('07c8o0qd8encgtli8qq3n4oeh3lp1au4', '::1', 1547029330, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373032393130353b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('as8jj64h2ac6v7drpms422cc3666psdd', '::1', 1547029684, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373032393433363b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('9b3356kmki7h7viuiho990c82697epkm', '::1', 1547030210, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373032393838333b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('2m403vboo7ck9hkf286cr7c6uho0fu8b', '::1', 1547032122, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373033313439303b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('enq9v7dpabf9gonpii1fhhs08tjvh588', '::1', 1547032350, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373033323132373b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('0lfkbmf963jnrovgnmrdqhvt0cclgogj', '::1', 1547033272, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373033333034323b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('b19hvabpbqogq5ppu6fu0kngut76ueub', '::1', 1547033927, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373033333634333b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('si31t9b4d2fmbtp76vi5lsjhvo4034jt', '::1', 1547034251, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373033333936333b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('n90irsnbmkl8lhe5uo72kve7vcfnr5cj', '::1', 1547034708, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373033343330373b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('li5lbkcoe5i89slom7e6vgs390rfv3ve', '::1', 1547035957, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373033353836323b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('ipuahe2upefkckf5490u53g44f3jdipl', '::1', 1547036567, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373033363235333b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('fh4vr5rd1887u3pq8297nqel55pg1106', '::1', 1547037052, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373033363734333b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('pjb90np8rlpes3t9iv5cgqcdmmufgfu5', '::1', 1547037973, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373033373631393b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('2arcrv5v0vq232s6jqh991619cp78gjv', '::1', 1547038053, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373033383034303b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('i6ifr6l8b5bna4bap2vrn6rularqrn8a', '::1', 1547038736, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373033383338313b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('h3j6naiapnhq9jftqnm678ecsogpsq16', '::1', 1547038917, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373033383736363b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('89bigveh5q0iat57hesannlr1q1b5v2q', '::1', 1547039220, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373033393132323b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('hci5d141k07dei82rng38p6i439866j0', '::1', 1547039715, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373033393536323b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('hnl635e3hihujaqa4uqj7a3622tvco30', '::1', 1547040611, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373034303434373b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('orc4fklebbeo4v45i5fkkl79mvudg37h', '::1', 1547040982, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373034303739383b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('uebt1gjrchcl8tudgvs2e188eubbstq3', '::1', 1547041687, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373034313135383b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('sqvdaie1bfo4gkftpv0lbv9q14tb4vo6', '::1', 1547041951, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373034313734363b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('eb8ps2li3mgcejitnoci8m13kc6j7uqq', '::1', 1547042882, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373034323039303b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('55pgkrcmdei2cm3kr8ph6spchrh1munp', '::1', 1547043426, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373034323933383b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('htm127bjtlq1dsah1sqgasj0bdnju6h1', '::1', 1547043778, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373034333737383b),
('0rabm7qephma3k2353ogdm13o62ck03o', '::1', 1547103035, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373039383431363b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b),
('j3q83hricd2j02g960o95ockj5ftjsmj', '::1', 1547107515, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373130333735333b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b),
('e10ic68kptr02qg1tspm52oo8a2044sa', '::1', 1547107948, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373130373634343b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b),
('rvp7rv1ja2qnr9jmj8s6ejts1gig1u72', '::1', 1547108259, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373130373939323b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b),
('9u0e0v72sadq05q98cecmr96s572nd5o', '::1', 1547110412, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373131303139303b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b),
('m9si001ku24e1s9li0hiinb6fqvag1o1', '::1', 1547111170, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373131303834363b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b),
('5b6bbo6arknkmdr7pq49rjo1g5mscc71', '::1', 1547111754, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373131313434303b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('ll0rokmo2nk6ha87vt68hc6dkca9nfdg', '::1', 1547112926, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373131323737343b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('ak14uvqs8jn6qkf8r4jf3q6slpio7nh9', '::1', 1547113705, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373131333336373b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('g62e3u80cfu275rfoieas1h50ga4rtg2', '::1', 1547113846, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373131333833323b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a363a226f6666696365223b),
('722s6asumnjfind4lqnn22av5d1b71t7', '::1', 1547114297, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373131343032393b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('jukvq40usf4rmp2nts4mlv66c6qk2jqs', '::1', 1547116680, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373131363634363b706572736f6e5f69647c733a313a2237223b6d656e755f67726f75707c733a343a22686f6d65223b73616c655f69647c693a2d313b73616c65735f636172747c613a303a7b7d73616c65735f637573746f6d65727c693a2d313b73616c65735f6d6f64657c733a343a2273616c65223b73616c65735f6c6f636174696f6e7c733a313a2231223b73616c65735f7061796d656e74737c613a303a7b7d636173685f6d6f64657c693a303b636173685f726f756e64696e677c693a303b73616c65735f696e766f6963655f6e756d6265727c4e3b),
('s9kvrg3b3si0ld10nud10gaemr2u4t54', '::1', 1547184600, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373138343539383b),
('moopu8jc54uo6mlug2pkgfiu96gcr4ua', '::1', 1547184686, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373138343539383b706572736f6e5f69647c733a313a2237223b6d656e755f67726f75707c733a343a22686f6d65223b73616c655f69647c693a2d313b73616c65735f6d6f64657c733a343a2273616c65223b73616c65735f6c6f636174696f6e7c733a313a2231223b73616c65735f696e766f6963655f6e756d6265725f656e61626c65647c623a303b73616c65735f636172747c613a303a7b7d73616c65735f637573746f6d65727c693a2d313b73616c65735f7061796d656e74737c613a303a7b7d636173685f6d6f64657c693a303b636173685f726f756e64696e677c693a303b73616c65735f696e766f6963655f6e756d6265727c4e3b),
('2s7314coesqv7bcfnrpsc4fuqj4md6r3', '::1', 1547186374, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373138363333383b706572736f6e5f69647c733a313a2237223b6d656e755f67726f75707c733a343a22686f6d65223b73616c655f69647c693a2d313b73616c65735f6d6f64657c733a343a2273616c65223b73616c65735f6c6f636174696f6e7c733a313a2231223b73616c65735f696e766f6963655f6e756d6265725f656e61626c65647c623a303b73616c65735f636172747c613a303a7b7d73616c65735f637573746f6d65727c693a2d313b73616c65735f7061796d656e74737c613a303a7b7d636173685f6d6f64657c693a303b636173685f726f756e64696e677c693a303b73616c65735f696e766f6963655f6e756d6265727c4e3b),
('mg9jfk6kb55jj5on6dll4dvlbh4rarc5', '::1', 1547189770, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373138393539313b706572736f6e5f69647c733a313a2237223b6d656e755f67726f75707c733a343a22686f6d65223b73616c655f69647c693a2d313b73616c65735f6d6f64657c733a343a2273616c65223b73616c65735f6c6f636174696f6e7c733a313a2231223b73616c65735f696e766f6963655f6e756d6265725f656e61626c65647c623a303b73616c65735f636172747c613a303a7b7d73616c65735f637573746f6d65727c693a2d313b73616c65735f7061796d656e74737c613a303a7b7d636173685f6d6f64657c693a303b636173685f726f756e64696e677c693a303b73616c65735f696e766f6963655f6e756d6265727c4e3b),
('ntm86epthevklqfaso8f2d7v2v0lct4k', '::1', 1547190186, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373138393934343b706572736f6e5f69647c733a313a2237223b6d656e755f67726f75707c733a343a22686f6d65223b73616c655f69647c693a2d313b73616c65735f6d6f64657c733a343a2273616c65223b73616c65735f6c6f636174696f6e7c733a313a2231223b73616c65735f696e766f6963655f6e756d6265725f656e61626c65647c623a303b73616c65735f636172747c613a303a7b7d73616c65735f637573746f6d65727c693a2d313b73616c65735f7061796d656e74737c613a303a7b7d636173685f6d6f64657c693a303b636173685f726f756e64696e677c693a303b73616c65735f696e766f6963655f6e756d6265727c4e3b),
('2m7rqmct7dr1afhrrv93vevfu59jpgl5', '::1', 1547190513, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373139303234363b706572736f6e5f69647c733a313a2237223b6d656e755f67726f75707c733a343a22686f6d65223b73616c655f69647c693a2d313b73616c65735f6d6f64657c733a343a2273616c65223b73616c65735f6c6f636174696f6e7c733a313a2231223b73616c65735f696e766f6963655f6e756d6265725f656e61626c65647c623a303b726563765f636172747c613a313a7b693a313b613a31353a7b733a373a226974656d5f6964223b733a323a223136223b733a31333a226974656d5f6c6f636174696f6e223b733a313a2231223b733a31303a2273746f636b5f6e616d65223b733a353a2273746f636b223b733a343a226c696e65223b693a313b733a343a226e616d65223b733a373a224f6e6c79204d45223b733a31313a226465736372697074696f6e223b733a303a22223b733a31323a2273657269616c6e756d626572223b733a303a22223b733a32313a22616c6c6f775f616c745f6465736372697074696f6e223b733a313a2230223b733a31333a2269735f73657269616c697a6564223b733a313a2230223b733a383a227175616e74697479223b693a313b733a383a22646973636f756e74223b693a303b733a383a22696e5f73746f636b223b733a353a22382e303030223b733a353a227072696365223b733a353a2234342e3030223b733a31383a22726563656976696e675f7175616e74697479223b733a353a22312e303030223b733a353a22746f74616c223b733a373a2234342e30303030223b7d7d726563765f6d6f64657c733a373a2272656365697665223b726563765f737570706c6965727c693a2d313b726563765f73746f636b5f736f757263657c733a313a2231223b),
('qd1n6obe2qk882lhuunt2g2jlr8hr5pc', '::1', 1547190781, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373139303631333b706572736f6e5f69647c733a313a2237223b6d656e755f67726f75707c733a343a22686f6d65223b73616c655f69647c693a2d313b73616c65735f6d6f64657c733a343a2273616c65223b73616c65735f6c6f636174696f6e7c733a313a2231223b73616c65735f696e766f6963655f6e756d6265725f656e61626c65647c623a303b726563765f636172747c613a313a7b693a313b613a31353a7b733a373a226974656d5f6964223b733a323a223136223b733a31333a226974656d5f6c6f636174696f6e223b733a313a2231223b733a31303a2273746f636b5f6e616d65223b733a353a2273746f636b223b733a343a226c696e65223b693a313b733a343a226e616d65223b733a373a224f6e6c79204d45223b733a31313a226465736372697074696f6e223b733a303a22223b733a31323a2273657269616c6e756d626572223b733a303a22223b733a32313a22616c6c6f775f616c745f6465736372697074696f6e223b733a313a2230223b733a31333a2269735f73657269616c697a6564223b733a313a2230223b733a383a227175616e74697479223b693a313b733a383a22646973636f756e74223b693a303b733a383a22696e5f73746f636b223b733a353a22382e303030223b733a353a227072696365223b733a353a2234342e3030223b733a31383a22726563656976696e675f7175616e74697479223b733a353a22312e303030223b733a353a22746f74616c223b733a373a2234342e30303030223b7d7d726563765f6d6f64657c733a373a2272656365697665223b726563765f737570706c6965727c693a2d313b726563765f73746f636b5f736f757263657c733a313a2231223b73616c65735f636172747c613a313a7b693a313b613a32313a7b733a373a226974656d5f6964223b733a323a223136223b733a31333a226974656d5f6c6f636174696f6e223b733a313a2231223b733a31303a2273746f636b5f6e616d65223b733a353a2273746f636b223b733a343a226c696e65223b693a313b733a343a226e616d65223b733a373a224f6e6c79204d45223b733a31313a226974656d5f6e756d626572223b4e3b733a31313a226465736372697074696f6e223b733a303a22223b733a31323a2273657269616c6e756d626572223b733a303a22223b733a32313a22616c6c6f775f616c745f6465736372697074696f6e223b733a313a2230223b733a31333a2269735f73657269616c697a6564223b733a313a2230223b733a383a227175616e74697479223b733a363a22322e30303030223b733a383a22646973636f756e74223b733a343a22302e3030223b733a383a22696e5f73746f636b223b733a353a22372e303030223b733a353a227072696365223b733a353a2233322e3030223b733a31303a22636f73745f7072696365223b733a353a2234342e3030223b733a353a22746f74616c223b733a373a2236342e30303030223b733a31363a22646973636f756e7465645f746f74616c223b733a373a2236342e30303030223b733a31323a227072696e745f6f7074696f6e223b693a303b733a31303a2273746f636b5f74797065223b733a313a2230223b733a393a226974656d5f74797065223b733a313a2230223b733a31353a227461785f63617465676f72795f6964223b733a313a2230223b7d7d73616c65735f637573746f6d65727c733a313a2232223b73616c65735f7061796d656e74737c613a303a7b7d636173685f6d6f64657c693a303b636173685f726f756e64696e677c693a303b73616c65735f696e766f6963655f6e756d6265727c4e3b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('t7gvgnrv548nn9f5c4hoqu6o2lsj6kjh', '::1', 1547191159, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373139313133363b706572736f6e5f69647c733a313a2237223b6d656e755f67726f75707c733a343a22686f6d65223b73616c655f69647c693a2d313b73616c65735f6d6f64657c733a343a2273616c65223b73616c65735f6c6f636174696f6e7c733a313a2231223b73616c65735f696e766f6963655f6e756d6265725f656e61626c65647c623a303b726563765f636172747c613a313a7b693a313b613a31353a7b733a373a226974656d5f6964223b733a323a223136223b733a31333a226974656d5f6c6f636174696f6e223b733a313a2231223b733a31303a2273746f636b5f6e616d65223b733a353a2273746f636b223b733a343a226c696e65223b693a313b733a343a226e616d65223b733a373a224f6e6c79204d45223b733a31313a226465736372697074696f6e223b733a303a22223b733a31323a2273657269616c6e756d626572223b733a303a22223b733a32313a22616c6c6f775f616c745f6465736372697074696f6e223b733a313a2230223b733a31333a2269735f73657269616c697a6564223b733a313a2230223b733a383a227175616e74697479223b693a313b733a383a22646973636f756e74223b693a303b733a383a22696e5f73746f636b223b733a353a22382e303030223b733a353a227072696365223b733a353a2234342e3030223b733a31383a22726563656976696e675f7175616e74697479223b733a353a22312e303030223b733a353a22746f74616c223b733a373a2234342e30303030223b7d7d726563765f6d6f64657c733a373a2272656365697665223b726563765f737570706c6965727c693a2d313b726563765f73746f636b5f736f757263657c733a313a2231223b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('4i8crab5cuedshaatjqo55ki21p8pbu3', '::1', 1547192142, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373139313834313b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('7kuqhe777ma80hu40i9l9rjr3gsp0jt6', '::1', 1547193264, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373139323230343b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('444l12omo2617spkk0itsilnqilve6be', '::1', 1547193930, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373139333834353b706572736f6e5f69647c733a313a2237223b6d656e755f67726f75707c733a343a22686f6d65223b73616c655f69647c693a2d313b73616c65735f6d6f64657c733a343a2273616c65223b73616c65735f6c6f636174696f6e7c733a313a2231223b73616c65735f696e766f6963655f6e756d6265725f656e61626c65647c623a303b),
('6h465jpjf1eo50a3l4h0f34ihcc4ai3g', '::1', 1547194410, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373139343336343b706572736f6e5f69647c733a313a2237223b6d656e755f67726f75707c733a343a22686f6d65223b73616c655f69647c693a2d313b73616c65735f6d6f64657c733a343a2273616c65223b73616c65735f6c6f636174696f6e7c733a313a2231223b73616c65735f696e766f6963655f6e756d6265725f656e61626c65647c623a303b73616c65735f636172747c613a313a7b693a313b613a32313a7b733a373a226974656d5f6964223b733a323a223136223b733a31333a226974656d5f6c6f636174696f6e223b733a313a2231223b733a31303a2273746f636b5f6e616d65223b733a353a2273746f636b223b733a343a226c696e65223b693a313b733a343a226e616d65223b733a373a224f6e6c79204d45223b733a31313a226974656d5f6e756d626572223b4e3b733a31313a226465736372697074696f6e223b733a303a22223b733a31323a2273657269616c6e756d626572223b733a303a22223b733a32313a22616c6c6f775f616c745f6465736372697074696f6e223b733a313a2230223b733a31333a2269735f73657269616c697a6564223b733a313a2230223b733a383a227175616e74697479223b643a313b733a383a22646973636f756e74223b733a313a2230223b733a383a22696e5f73746f636b223b733a353a22342e303030223b733a353a227072696365223b643a33303b733a31303a22636f73745f7072696365223b733a353a2234342e3030223b733a353a22746f74616c223b733a323a223330223b733a31363a22646973636f756e7465645f746f74616c223b733a373a2233302e30303030223b733a31323a227072696e745f6f7074696f6e223b693a303b733a31303a2273746f636b5f74797065223b733a313a2230223b733a393a226974656d5f74797065223b733a313a2230223b733a31353a227461785f63617465676f72795f6964223b733a313a2230223b7d7d73616c65735f637573746f6d65727c693a2d313b73616c65735f7061796d656e74737c613a303a7b7d636173685f6d6f64657c693a303b636173685f726f756e64696e677c693a303b73616c65735f696e766f6963655f6e756d6265727c4e3b),
('em3s1ldl7r2p69pltdo10qcp3o80it0t', '::1', 1547194973, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373139343936353b706572736f6e5f69647c733a313a2237223b6d656e755f67726f75707c733a343a22686f6d65223b73616c655f69647c693a2d313b73616c65735f636172747c613a303a7b7d73616c65735f637573746f6d65727c693a2d313b73616c65735f6d6f64657c733a343a2273616c65223b73616c65735f6c6f636174696f6e7c733a313a2231223b73616c65735f7061796d656e74737c613a303a7b7d636173685f6d6f64657c693a303b636173685f726f756e64696e677c693a303b73616c65735f696e766f6963655f6e756d6265727c4e3b),
('u0sf3fognav5ohmsu5h830us2hgd0jhq', '::1', 1547199269, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373139383932383b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a363a226f6666696365223b),
('ugknfr4romh59t2ko5p73s9f1jb1aouu', '::1', 1547199571, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373139393238313b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a363a226f6666696365223b);
INSERT INTO `ospos_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('jrbgd7fmcpb72h69kmen1746uib5vct9', '::1', 1547200037, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373139393738313b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a363a226f6666696365223b),
('gu29gbars790mqah4gf58nr469inivnl', '::1', 1547200241, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373230303130343b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a363a226f6666696365223b),
('phm7p6ebnm9tf3f9r3v7lreq7kr68oic', '::1', 1547204054, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373230333533303b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a363a226f6666696365223b),
('u2gj25nt971q4uv2e93lm6h6p0c23u1c', '::1', 1547204154, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373230343038373b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a363a226f6666696365223b),
('1rqnh8ddp73e6fd254kstqca3db3c616', '::1', 1547204593, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373230343435333b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a363a226f6666696365223b),
('oil32r5295aui23mohongfh1f8osfd7a', '::1', 1547205182, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373230343935343b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a363a226f6666696365223b),
('oon02ao2iflumao1u9045esdf5hjr0tt', '::1', 1547205524, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373230353335343b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a363a226f6666696365223b),
('g8flijsh6jktbda8hc5htim87eriou73', '::1', 1547206384, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373230363338333b),
('i76h4mj1r1pgigsdr4efn96ea6mcounb', '::1', 1547207787, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373230373738363b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b),
('ak63rd47jt01eiee23drlmmmr86ugc7p', '::1', 1547208453, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373230383230373b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b),
('frg4rt5rrric95vm16ikabhks262ul0m', '::1', 1547209893, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373230393838353b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b),
('3mhenjqh2slapc0psbknjtrjt97amtb7', '::1', 1547211880, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373231313836393b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b),
('d80hvu0o9llom0rah8onhd06su44krn8', '::1', 1547444946, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373434343930383b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b),
('11eg0piltelgmdk572hk2jk0okk39iqb', '::1', 1547449401, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373434393234333b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('alr7l0k4raf74rf8ubogrpugbf2eges9', '::1', 1547449858, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373434393635313b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('8lpu8apn3q1asbmpagum1uhpb350k149', '::1', 1547451483, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373435303133393b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('m4om0eut9ftvhqi444mej25ljmccr715', '::1', 1547451899, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373435313439383b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b6974656d5f6c6f636174696f6e7c733a313a2231223b),
('07b0gaqge8t7s36q23c654ood97ienk6', '::1', 1547452479, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534373435323437363b706572736f6e5f69647c733a313a2231223b6d656e755f67726f75707c733a343a22686f6d65223b6974656d5f6c6f636174696f6e7c733a313a2231223b);

-- --------------------------------------------------------

--
-- Table structure for table `ospos_stock_locations`
--

CREATE TABLE `ospos_stock_locations` (
  `location_id` int(11) NOT NULL,
  `location_name` varchar(255) DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ospos_stock_locations`
--

INSERT INTO `ospos_stock_locations` (`location_id`, `location_name`, `deleted`) VALUES
(1, 'stock', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ospos_suppliers`
--

CREATE TABLE `ospos_suppliers` (
  `person_id` int(10) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `agency_name` varchar(255) NOT NULL,
  `account_number` varchar(255) DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ospos_suppliers`
--

INSERT INTO `ospos_suppliers` (`person_id`, `company_name`, `agency_name`, `account_number`, `deleted`) VALUES
(5, 'John Sons', 'JonhSons', '345355454543', 0),
(15, 'Bhola Sweets', 'Bhola Mithai House', NULL, 0),
(16, 'Prepeans', '', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ospos_supplier_deb_cre`
--

CREATE TABLE `ospos_supplier_deb_cre` (
  `dc_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `bill_no` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `paid` float NOT NULL,
  `description` text NOT NULL,
  `date_paid` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ospos_supplier_deb_cre`
--

INSERT INTO `ospos_supplier_deb_cre` (`dc_id`, `supplier_id`, `qty`, `price`, `bill_no`, `date_created`, `date_updated`, `paid`, `description`, `date_paid`, `status`) VALUES
(1, 15, 12, 15000, '43', '2019-01-14 07:40:27', '2019-01-14 12:40:27', 5000, '', '2019-01-14 12:41:03', 'main'),
(2, 15, 0, 0, '43', '2019-01-14 07:40:27', '2019-01-14 12:40:27', 3000, '', '2019-01-14 12:40:27', 'sub'),
(3, 15, 0, 0, '43', '2019-01-14 07:41:03', '2019-01-14 12:41:03', 2000, '', '2019-01-14 12:41:03', 'sub');

-- --------------------------------------------------------

--
-- Table structure for table `ospos_tax_categories`
--

CREATE TABLE `ospos_tax_categories` (
  `tax_category_id` int(10) NOT NULL,
  `tax_category` varchar(32) NOT NULL,
  `tax_group_sequence` tinyint(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ospos_tax_categories`
--

INSERT INTO `ospos_tax_categories` (`tax_category_id`, `tax_category`, `tax_group_sequence`) VALUES
(1, 'Standard', 10),
(2, 'Service', 12),
(3, 'Alcohol', 11);

-- --------------------------------------------------------

--
-- Table structure for table `ospos_tax_codes`
--

CREATE TABLE `ospos_tax_codes` (
  `tax_code` varchar(32) NOT NULL,
  `tax_code_name` varchar(255) NOT NULL DEFAULT '',
  `tax_code_type` tinyint(2) NOT NULL DEFAULT '0',
  `city` varchar(255) NOT NULL DEFAULT '',
  `state` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ospos_tax_code_rates`
--

CREATE TABLE `ospos_tax_code_rates` (
  `rate_tax_code` varchar(32) NOT NULL,
  `rate_tax_category_id` int(10) NOT NULL,
  `tax_rate` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `rounding_code` tinyint(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ospos_app_config`
--
ALTER TABLE `ospos_app_config`
  ADD PRIMARY KEY (`key`);

--
-- Indexes for table `ospos_customers`
--
ALTER TABLE `ospos_customers`
  ADD UNIQUE KEY `account_number` (`account_number`),
  ADD KEY `person_id` (`person_id`),
  ADD KEY `package_id` (`package_id`);

--
-- Indexes for table `ospos_customers_packages`
--
ALTER TABLE `ospos_customers_packages`
  ADD PRIMARY KEY (`package_id`);

--
-- Indexes for table `ospos_customers_points`
--
ALTER TABLE `ospos_customers_points`
  ADD PRIMARY KEY (`id`),
  ADD KEY `person_id` (`person_id`),
  ADD KEY `package_id` (`package_id`),
  ADD KEY `sale_id` (`sale_id`);

--
-- Indexes for table `ospos_dinner_tables`
--
ALTER TABLE `ospos_dinner_tables`
  ADD PRIMARY KEY (`dinner_table_id`);

--
-- Indexes for table `ospos_employees`
--
ALTER TABLE `ospos_employees`
  ADD UNIQUE KEY `username` (`username`),
  ADD KEY `person_id` (`person_id`);

--
-- Indexes for table `ospos_expenses`
--
ALTER TABLE `ospos_expenses`
  ADD PRIMARY KEY (`expense_id`),
  ADD KEY `expense_category_id` (`expense_category_id`),
  ADD KEY `employee_id` (`employee_id`);

--
-- Indexes for table `ospos_expense_categories`
--
ALTER TABLE `ospos_expense_categories`
  ADD PRIMARY KEY (`expense_category_id`),
  ADD UNIQUE KEY `category_name` (`category_name`);

--
-- Indexes for table `ospos_giftcards`
--
ALTER TABLE `ospos_giftcards`
  ADD PRIMARY KEY (`giftcard_id`),
  ADD UNIQUE KEY `giftcard_number` (`giftcard_number`),
  ADD KEY `person_id` (`person_id`);

--
-- Indexes for table `ospos_grants`
--
ALTER TABLE `ospos_grants`
  ADD PRIMARY KEY (`permission_id`,`person_id`),
  ADD KEY `ospos_grants_ibfk_2` (`person_id`);

--
-- Indexes for table `ospos_inventory`
--
ALTER TABLE `ospos_inventory`
  ADD PRIMARY KEY (`trans_id`),
  ADD KEY `trans_items` (`trans_items`),
  ADD KEY `trans_user` (`trans_user`),
  ADD KEY `trans_location` (`trans_location`);

--
-- Indexes for table `ospos_items`
--
ALTER TABLE `ospos_items`
  ADD PRIMARY KEY (`item_id`),
  ADD KEY `item_number` (`item_number`),
  ADD KEY `supplier_id` (`supplier_id`);

--
-- Indexes for table `ospos_items_taxes`
--
ALTER TABLE `ospos_items_taxes`
  ADD PRIMARY KEY (`item_id`,`name`,`percent`);

--
-- Indexes for table `ospos_item_kits`
--
ALTER TABLE `ospos_item_kits`
  ADD PRIMARY KEY (`item_kit_id`);

--
-- Indexes for table `ospos_item_kit_items`
--
ALTER TABLE `ospos_item_kit_items`
  ADD PRIMARY KEY (`item_kit_id`,`item_id`,`quantity`),
  ADD KEY `ospos_item_kit_items_ibfk_2` (`item_id`);

--
-- Indexes for table `ospos_item_quantities`
--
ALTER TABLE `ospos_item_quantities`
  ADD PRIMARY KEY (`item_id`,`location_id`),
  ADD KEY `item_id` (`item_id`),
  ADD KEY `location_id` (`location_id`);

--
-- Indexes for table `ospos_modules`
--
ALTER TABLE `ospos_modules`
  ADD PRIMARY KEY (`module_id`),
  ADD UNIQUE KEY `desc_lang_key` (`desc_lang_key`),
  ADD UNIQUE KEY `name_lang_key` (`name_lang_key`);

--
-- Indexes for table `ospos_people`
--
ALTER TABLE `ospos_people`
  ADD PRIMARY KEY (`person_id`),
  ADD KEY `email` (`email`);

--
-- Indexes for table `ospos_permissions`
--
ALTER TABLE `ospos_permissions`
  ADD PRIMARY KEY (`permission_id`),
  ADD KEY `module_id` (`module_id`),
  ADD KEY `ospos_permissions_ibfk_2` (`location_id`);

--
-- Indexes for table `ospos_receivings`
--
ALTER TABLE `ospos_receivings`
  ADD PRIMARY KEY (`receiving_id`),
  ADD KEY `supplier_id` (`supplier_id`),
  ADD KEY `employee_id` (`employee_id`),
  ADD KEY `reference` (`reference`);

--
-- Indexes for table `ospos_receivings_items`
--
ALTER TABLE `ospos_receivings_items`
  ADD PRIMARY KEY (`receiving_id`,`item_id`,`line`),
  ADD KEY `item_id` (`item_id`);

--
-- Indexes for table `ospos_sales`
--
ALTER TABLE `ospos_sales`
  ADD PRIMARY KEY (`sale_id`),
  ADD UNIQUE KEY `invoice_number` (`invoice_number`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `employee_id` (`employee_id`),
  ADD KEY `sale_time` (`sale_time`),
  ADD KEY `dinner_table_id` (`dinner_table_id`);

--
-- Indexes for table `ospos_sales_items`
--
ALTER TABLE `ospos_sales_items`
  ADD PRIMARY KEY (`sale_id`,`item_id`,`line`),
  ADD KEY `sale_id` (`sale_id`),
  ADD KEY `item_id` (`item_id`),
  ADD KEY `item_location` (`item_location`);

--
-- Indexes for table `ospos_sales_items_taxes`
--
ALTER TABLE `ospos_sales_items_taxes`
  ADD PRIMARY KEY (`sale_id`,`item_id`,`line`,`name`,`percent`),
  ADD KEY `sale_id` (`sale_id`),
  ADD KEY `item_id` (`item_id`);

--
-- Indexes for table `ospos_sales_reward_points`
--
ALTER TABLE `ospos_sales_reward_points`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sale_id` (`sale_id`);

--
-- Indexes for table `ospos_sales_taxes`
--
ALTER TABLE `ospos_sales_taxes`
  ADD PRIMARY KEY (`sale_id`,`tax_type`,`tax_group`),
  ADD KEY `print_sequence` (`sale_id`,`print_sequence`,`tax_type`,`tax_group`);

--
-- Indexes for table `ospos_sessions`
--
ALTER TABLE `ospos_sessions`
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `ospos_stock_locations`
--
ALTER TABLE `ospos_stock_locations`
  ADD PRIMARY KEY (`location_id`);

--
-- Indexes for table `ospos_suppliers`
--
ALTER TABLE `ospos_suppliers`
  ADD UNIQUE KEY `account_number` (`account_number`),
  ADD KEY `person_id` (`person_id`);

--
-- Indexes for table `ospos_supplier_deb_cre`
--
ALTER TABLE `ospos_supplier_deb_cre`
  ADD PRIMARY KEY (`dc_id`);

--
-- Indexes for table `ospos_tax_categories`
--
ALTER TABLE `ospos_tax_categories`
  ADD PRIMARY KEY (`tax_category_id`);

--
-- Indexes for table `ospos_tax_codes`
--
ALTER TABLE `ospos_tax_codes`
  ADD PRIMARY KEY (`tax_code`);

--
-- Indexes for table `ospos_tax_code_rates`
--
ALTER TABLE `ospos_tax_code_rates`
  ADD PRIMARY KEY (`rate_tax_code`,`rate_tax_category_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ospos_customers_packages`
--
ALTER TABLE `ospos_customers_packages`
  MODIFY `package_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `ospos_customers_points`
--
ALTER TABLE `ospos_customers_points`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ospos_dinner_tables`
--
ALTER TABLE `ospos_dinner_tables`
  MODIFY `dinner_table_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ospos_expenses`
--
ALTER TABLE `ospos_expenses`
  MODIFY `expense_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `ospos_expense_categories`
--
ALTER TABLE `ospos_expense_categories`
  MODIFY `expense_category_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ospos_giftcards`
--
ALTER TABLE `ospos_giftcards`
  MODIFY `giftcard_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ospos_inventory`
--
ALTER TABLE `ospos_inventory`
  MODIFY `trans_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=120;
--
-- AUTO_INCREMENT for table `ospos_items`
--
ALTER TABLE `ospos_items`
  MODIFY `item_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `ospos_item_kits`
--
ALTER TABLE `ospos_item_kits`
  MODIFY `item_kit_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ospos_people`
--
ALTER TABLE `ospos_people`
  MODIFY `person_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `ospos_receivings`
--
ALTER TABLE `ospos_receivings`
  MODIFY `receiving_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ospos_sales`
--
ALTER TABLE `ospos_sales`
  MODIFY `sale_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `ospos_sales_reward_points`
--
ALTER TABLE `ospos_sales_reward_points`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ospos_stock_locations`
--
ALTER TABLE `ospos_stock_locations`
  MODIFY `location_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ospos_supplier_deb_cre`
--
ALTER TABLE `ospos_supplier_deb_cre`
  MODIFY `dc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ospos_tax_categories`
--
ALTER TABLE `ospos_tax_categories`
  MODIFY `tax_category_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `ospos_customers`
--
ALTER TABLE `ospos_customers`
  ADD CONSTRAINT `ospos_customers_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `ospos_people` (`person_id`),
  ADD CONSTRAINT `ospos_customers_ibfk_2` FOREIGN KEY (`package_id`) REFERENCES `ospos_customers_packages` (`package_id`);

--
-- Constraints for table `ospos_customers_points`
--
ALTER TABLE `ospos_customers_points`
  ADD CONSTRAINT `ospos_customers_points_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `ospos_customers` (`person_id`),
  ADD CONSTRAINT `ospos_customers_points_ibfk_2` FOREIGN KEY (`package_id`) REFERENCES `ospos_customers_packages` (`package_id`),
  ADD CONSTRAINT `ospos_customers_points_ibfk_3` FOREIGN KEY (`sale_id`) REFERENCES `ospos_sales` (`sale_id`);

--
-- Constraints for table `ospos_employees`
--
ALTER TABLE `ospos_employees`
  ADD CONSTRAINT `ospos_employees_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `ospos_people` (`person_id`);

--
-- Constraints for table `ospos_expenses`
--
ALTER TABLE `ospos_expenses`
  ADD CONSTRAINT `ospos_expenses_ibfk_1` FOREIGN KEY (`expense_category_id`) REFERENCES `ospos_expense_categories` (`expense_category_id`),
  ADD CONSTRAINT `ospos_expenses_ibfk_2` FOREIGN KEY (`employee_id`) REFERENCES `ospos_employees` (`person_id`);

--
-- Constraints for table `ospos_giftcards`
--
ALTER TABLE `ospos_giftcards`
  ADD CONSTRAINT `ospos_giftcards_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `ospos_people` (`person_id`);

--
-- Constraints for table `ospos_grants`
--
ALTER TABLE `ospos_grants`
  ADD CONSTRAINT `ospos_grants_ibfk_1` FOREIGN KEY (`permission_id`) REFERENCES `ospos_permissions` (`permission_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ospos_grants_ibfk_2` FOREIGN KEY (`person_id`) REFERENCES `ospos_employees` (`person_id`) ON DELETE CASCADE;

--
-- Constraints for table `ospos_inventory`
--
ALTER TABLE `ospos_inventory`
  ADD CONSTRAINT `ospos_inventory_ibfk_1` FOREIGN KEY (`trans_items`) REFERENCES `ospos_items` (`item_id`),
  ADD CONSTRAINT `ospos_inventory_ibfk_2` FOREIGN KEY (`trans_user`) REFERENCES `ospos_employees` (`person_id`),
  ADD CONSTRAINT `ospos_inventory_ibfk_3` FOREIGN KEY (`trans_location`) REFERENCES `ospos_stock_locations` (`location_id`);

--
-- Constraints for table `ospos_items`
--
ALTER TABLE `ospos_items`
  ADD CONSTRAINT `ospos_items_ibfk_1` FOREIGN KEY (`supplier_id`) REFERENCES `ospos_suppliers` (`person_id`);

--
-- Constraints for table `ospos_items_taxes`
--
ALTER TABLE `ospos_items_taxes`
  ADD CONSTRAINT `ospos_items_taxes_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `ospos_items` (`item_id`) ON DELETE CASCADE;

--
-- Constraints for table `ospos_item_kit_items`
--
ALTER TABLE `ospos_item_kit_items`
  ADD CONSTRAINT `ospos_item_kit_items_ibfk_1` FOREIGN KEY (`item_kit_id`) REFERENCES `ospos_item_kits` (`item_kit_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ospos_item_kit_items_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `ospos_items` (`item_id`) ON DELETE CASCADE;

--
-- Constraints for table `ospos_item_quantities`
--
ALTER TABLE `ospos_item_quantities`
  ADD CONSTRAINT `ospos_item_quantities_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `ospos_items` (`item_id`),
  ADD CONSTRAINT `ospos_item_quantities_ibfk_2` FOREIGN KEY (`location_id`) REFERENCES `ospos_stock_locations` (`location_id`);

--
-- Constraints for table `ospos_permissions`
--
ALTER TABLE `ospos_permissions`
  ADD CONSTRAINT `ospos_permissions_ibfk_1` FOREIGN KEY (`module_id`) REFERENCES `ospos_modules` (`module_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ospos_permissions_ibfk_2` FOREIGN KEY (`location_id`) REFERENCES `ospos_stock_locations` (`location_id`) ON DELETE CASCADE;

--
-- Constraints for table `ospos_receivings`
--
ALTER TABLE `ospos_receivings`
  ADD CONSTRAINT `ospos_receivings_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `ospos_employees` (`person_id`),
  ADD CONSTRAINT `ospos_receivings_ibfk_2` FOREIGN KEY (`supplier_id`) REFERENCES `ospos_suppliers` (`person_id`);

--
-- Constraints for table `ospos_receivings_items`
--
ALTER TABLE `ospos_receivings_items`
  ADD CONSTRAINT `ospos_receivings_items_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `ospos_items` (`item_id`),
  ADD CONSTRAINT `ospos_receivings_items_ibfk_2` FOREIGN KEY (`receiving_id`) REFERENCES `ospos_receivings` (`receiving_id`);

--
-- Constraints for table `ospos_sales`
--
ALTER TABLE `ospos_sales`
  ADD CONSTRAINT `ospos_sales_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `ospos_employees` (`person_id`),
  ADD CONSTRAINT `ospos_sales_ibfk_2` FOREIGN KEY (`customer_id`) REFERENCES `ospos_customers` (`person_id`),
  ADD CONSTRAINT `ospos_sales_ibfk_3` FOREIGN KEY (`dinner_table_id`) REFERENCES `ospos_dinner_tables` (`dinner_table_id`);

--
-- Constraints for table `ospos_sales_items`
--
ALTER TABLE `ospos_sales_items`
  ADD CONSTRAINT `ospos_sales_items_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `ospos_items` (`item_id`),
  ADD CONSTRAINT `ospos_sales_items_ibfk_2` FOREIGN KEY (`sale_id`) REFERENCES `ospos_sales` (`sale_id`),
  ADD CONSTRAINT `ospos_sales_items_ibfk_3` FOREIGN KEY (`item_location`) REFERENCES `ospos_stock_locations` (`location_id`);

--
-- Constraints for table `ospos_sales_items_taxes`
--
ALTER TABLE `ospos_sales_items_taxes`
  ADD CONSTRAINT `ospos_sales_items_taxes_ibfk_1` FOREIGN KEY (`sale_id`) REFERENCES `ospos_sales_items` (`sale_id`),
  ADD CONSTRAINT `ospos_sales_items_taxes_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `ospos_items` (`item_id`);

--
-- Constraints for table `ospos_sales_reward_points`
--
ALTER TABLE `ospos_sales_reward_points`
  ADD CONSTRAINT `ospos_sales_reward_points_ibfk_1` FOREIGN KEY (`sale_id`) REFERENCES `ospos_sales` (`sale_id`);

--
-- Constraints for table `ospos_suppliers`
--
ALTER TABLE `ospos_suppliers`
  ADD CONSTRAINT `ospos_suppliers_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `ospos_people` (`person_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
